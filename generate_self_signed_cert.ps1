# Source: https://stackoverflow.com/a/62060315
# Generate self-signed certificate to be used by IdentityServer.
# When using localhost - API cannot see the IdentityServer from within the docker-compose'd network.
# You have to run this script as Administrator (open Powershell by right click -> Run as Administrator).
#https://github.com/mjarosie/IdentityServerDockerHttpsDemo

$ErrorActionPreference = "Stop"

$rootCN = "IdentityServerDockerDemoRootCert"

$identityCNs = "identity","localhost"
$webApiCNs = "webapi","localhost"

$pass = "dev@123"

$rootCertPathPfx = ".data/certs"
$identityServerCertPath = ".data/identity/certs" 
$webApiCertPath = ".data/webapi/certs"

if ((Test-Path ".data") -eq "True") {
   Get-ChildItem -Path .data\* -Include key-*.xml -Recurse | Remove-Item
}

if ((Test-Path $rootCertPathPfx) ) {
   Write-Output "directory exist $rootCertPathPfx"
}
else
{
   New-Item $rootCertPathPfx -ItemType Directory  -Force
   Write-Output "directory created $rootCertPathPfx"
}
if ((Test-Path $identityServerCertPath)) {
   Write-Output "directory exist $identityServerCertPath"
}
else
{
   New-Item $identityServerCertPath -ItemType Directory  -Force
   Write-Output "directory created $identityServerCertPath"
}
if ((Test-Path $webApiCertPath) ) {
   Write-Output "directory exist $webApiCertPath"
}
else
{
   New-Item $webApiCertPath -ItemType Directory  -Force
   Write-Output "directory created $webApiCertPath"
}



$alreadyExistingCerts = Get-ChildItem -Path Cert:\LocalMachine\My -Recurse   | Where-Object {$_.Issuer -eq "CN=$rootCN"} 
$alreadyExistingRootCerts = Get-ChildItem -Path Cert:\LocalMachine\Root -Recurse   | Where-Object {$_.Issuer -eq "CN=$rootCN"} 
$alreadyExistingCACerts = Get-ChildItem -Path Cert:\LocalMachine\CA -Recurse   | Where-Object {$_.Issuer -eq "CN=$rootCN"} 

$certsCount = $alreadyExistingCerts.Count
$certsRootCount = $alreadyExistingRootCerts.Count
$certsCACount = $alreadyExistingCACerts.Count


if ($alreadyExistingCerts.Count -ge 1) {
   Write-Output "Delete exist certificates ($certsCount) CN=$rootCN from  LocalMachine\My"
   $alreadyExistingCerts |  Remove-Item
}

if ($alreadyExistingRootCerts.Count -ge 1) {
   Write-Output "Delete exist certificates ($certsRootCount) CN=$rootCN from  LocalMachine\Root"
   $alreadyExistingRootCerts |  Remove-Item
}

if ($alreadyExistingCACerts.Count -ge 1) {
   Write-Output "Delete exist certificates ($certsCACount) CN=$rootCN from  LocalMachine\CA"
   $alreadyExistingCACerts |  Remove-Item
}



$testRootCA = New-SelfSignedCertificate -Subject $rootCN -KeyUsageProperty Sign -KeyUsage CertSign -CertStoreLocation Cert:\LocalMachine\My
$identityServerCert = New-SelfSignedCertificate -DnsName $identityCNs -Signer $testRootCA -CertStoreLocation Cert:\LocalMachine\My
$webApiCert = New-SelfSignedCertificate -DnsName $webApiCNs -Signer $testRootCA -CertStoreLocation Cert:\LocalMachine\My


# Export it for docker container to pick up later.
$password = ConvertTo-SecureString -String $pass -Force -AsPlainText


[System.IO.Directory]::CreateDirectory($rootCertPathPfx) | Out-Null
[System.IO.Directory]::CreateDirectory($identityServerCertPath) | Out-Null
[System.IO.Directory]::CreateDirectory($webApiCertPath) | Out-Null

Export-PfxCertificate -Cert $testRootCA -FilePath "$rootCertPathPfx/aspnetapp-root-cert.pfx" -Password $password | Out-Null
Export-PfxCertificate -Cert $identityServerCert -FilePath "$identityServerCertPath/aspnetapp-identity.pfx" -Password $password | Out-Null
Export-PfxCertificate -Cert $webApiCert -FilePath "$webApiCertPath/aspnetapp-webapi.pfx" -Password $password | Out-Null

# Export .cer to be converted to .crt to be trusted within the Docker container.
$rootCertPathCer = "$rootCertPathPfx/aspnetapp-root-cert.cer"
Export-Certificate -Cert $testRootCA -FilePath $rootCertPathCer -Type CERT | Out-Null

Write-Output "Trusting..."
# Trust it on your host machine.
$store = New-Object System.Security.Cryptography.X509Certificates.X509Store "Root","LocalMachine"
$store.Open("ReadWrite")

$rootCertAlreadyTrusted = ($store.Certificates | Where-Object {$_.Subject -eq "CN=$rootCN"} | Measure-Object).Count -eq 1

if ($rootCertAlreadyTrusted -eq $false) {
    Write-Output "Adding the root CA certificate to the trust store."
    $store.Add($testRootCA)
}

$store.Close()