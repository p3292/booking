/* departments */

delete from departments where 1=1;

insert into	departments (id,name)
values ('96001C3D-940B-4174-8C2B-0222BB0C46FC','Department 1');

insert into	departments (id,name)
values ('C2B0507D-F239-4AA9-9911-702F56315789','Department 2');

/* resource_types */

delete from resource_types where 1=1;

insert into resource_types (id,name,is_selectable)values('03418B69-7C59-46AE-B1A2-0B8007CFDADE','res 1', true);
insert into resource_types (id,name,is_selectable)values('916030B9-675F-46C4-A421-0EBAA61D2C80','res 2', false);

/* roles */

delete from roles where 1=1;

insert into roles (id,name,description)values('CFD7508F-745D-4119-9D51-10EF8D262660', 'role 1', 'role 1');
insert into roles (id,name,description)values('5A576CF0-C709-4E9C-9D26-1FBDCECA91FA', 'role 2', 'role 2');

/* employees */

delete from employees where 1=1;

INSERT into employees (id,department_id,role_id,first_name,last_name,middle_name,login,is_admin,can_use_system,login,email )
values('A0CFB134-79C4-4A6F-AB0D-2EFDB0D70EFC','96001C3D-940B-4174-8C2B-0222BB0C46FC','CFD7508F-745D-4119-9D51-10EF8D262660','First name','Last name','M','last',true,TRUE,'last','FIRST.LAST@domain.local');
INSERT into employees (id,department_id,role_id,first_name,last_name,middle_name,login,is_admin,can_use_system,login,email)
values('EA4695CD-EB4B-499B-B793-4FEABAAF46D0','96001C3D-940B-4174-8C2B-0222BB0C46FC','5A576CF0-C709-4E9C-9D26-1FBDCECA91FA','Ivanov','Ivan','I','Ivanov',false,TRUE,'test@domain.local','test@domain.local');

/* parkings */

delete from parkings where 1=1;

insert into parkings (id,name)values('88991A86-4D5D-4445-A6F8-79897A8D93C2', 'parking A');
insert into parkings (id,name)values('72E5114C-E8A8-46AA-B579-4FD3C0D8C7A7', 'parking B');



/*
delete from work_places where 1=1;
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
VALUES('E2799FE9-DC26-4A90-984E-5E8D3B4AB203', null, '01', null, null, '96001C3D-940B-4174-8C2B-0222BB0C46FC', 'build 01',1);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('2E402E64-C156-435E-8241-65610CE97252',null,'02',null,null,'96001C3D-940B-4174-8C2B-0222BB0C46FC','build 01',1);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('43BD2502-7168-425D-B82A-6F97CF51C234',null,'03',null,null,'96001C3D-940B-4174-8C2B-0222BB0C46FC','build 01',1);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('2F570E8B-B5AC-4523-9B72-AA464486D1A8',null,'04',null,null,'C2B0507D-F239-4AA9-9911-702F56315789','build 01',2);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('ABCB17E7-9F1B-4BEE-BE4B-718D3327C547',null,'05',null,null,'C2B0507D-F239-4AA9-9911-702F56315789','build 01',2);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('87CF0554-C0BB-46DB-8954-82439997EDF7',null,'06',null,null,'C2B0507D-F239-4AA9-9911-702F56315789','build 01',2);
INSERT INTO work_places(id, owner_id, "number", lock_time_start, lock_time_end, department_id, building, floor)
values('CA0E9ADA-03D3-4E6D-B93B-A7469E3FAD01',null,'07',null,null,'C2B0507D-F239-4AA9-9911-702F56315789','build 01',2);

delete from parking_places where 1=1;

INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('BFAC8485-26F5-4E3D-B7E9-AE293E40EFAB', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('5EEAC61B-1390-49AB-AC64-AEF8DB70ED20', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('E4F1E758-BCBE-4290-9B79-B8F7E4852633', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('E7656A79-28D3-47E0-935B-D22F6E133036', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('D665A264-C543-4AC6-9BDB-E85B38E6ADBC', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('AA08D5DB-D249-4A57-B546-EE7222A3BC79', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('B707B247-D474-43B5-8808-F1787A4239B3', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('13EA2542-2AB6-40C3-8876-F3FC747C674D', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('A4586EEA-8FDC-4704-BD2C-FA5A12B469F8', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('744B6D61-B8CC-4A0A-82A7-05789941D3D9', null, '01', null, null, 1);
INSERT INTO parking_places(id, owner_id, "number", lock_time_start, lock_time_end, place_type)
VALUES('5106C004-1EC2-4B74-820F-1FC9286E137A', null, '01', null, null, 1);

delete from book_place where 1=1;
INSERT INTO book_place
(id, owner_id, "number", lock_time_start, lock_time_end, discriminator, place_type, department_id, building, floor)
VALUES('2B7F4CC0-0103-47E2-A5F3-0133C0F06261', null, null, '', '', '', 0, '', '', 0);
*/