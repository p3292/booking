import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { getEmployeeInfo } from '../../../api/utilites';
import Nav from '../../Nav';
import './Home.css';
const Home: React.FC = () => {
  const [nameNav, setNameNav] = useState('');
  let navigate = useNavigate();

  useEffect(() => {
    if(getEmployeeInfo().isAuthorized)
      navigate("/reservations");
  },[]);

  return (
    <div className='home-content-container'>
      <div className='home-invite-label'>
        You need to log in to continue working. <br/>Click the LogIn button and enter your login and password.
      </div>
      <div className='home-buttons-panel'>
        <Nav name={nameNav} setName={setNameNav} />
      </div>
    </div>
  );
};

export default Home;