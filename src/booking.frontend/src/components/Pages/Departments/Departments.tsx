import React, { useEffect, useState } from 'react';
import { DataGrid } from 'devextreme-react/data-grid';
import { Client, DepartmentShortResponse } from '../../../api/api';
import './Departments.css';
import { Button, Form, Popup } from 'devextreme-react';
import CommandPanel from '../../CommandPanel';
import { Mod } from '../../CommandPanel/CommandPanel';
import { GroupItem, SimpleItem } from 'devextreme-react/form';
import { FieldDataChangedEvent } from 'devextreme/ui/form';
import { confirm } from 'devextreme/ui/dialog';
import { useNavigate } from 'react-router-dom';
import { getEmployeeInfo } from '../../../api/utilites';
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";

const apiClient = new Client();

const Departments: React.FC = () => {
  const [departments, setDepartments] = useState<DepartmentShortResponse[]>();
  const [isVisibleEditDialog, setVisibleEditDialog] = useState<boolean>(false);
  const [isReadOnlyEditForm, setReadOnlyEditForm] = useState<boolean>(false);
  const [formName, setFormName] = useState<string>('');
  const [mod, setMod] = useState<Mod>(Mod.VIEW);
  const [currentDepartment, setCurrentDepartment] =
    useState<DepartmentShortResponse>();

  useEffect(() => {
    loadDepartments();
  }, []);

  const loadDepartments = async () => {
    const items =
      (await apiClient.getDepartments()) as DepartmentShortResponse[];
    setDepartments(items);
  };

  const rowClick = async (recordId: string) => {
    setMod(Mod.VIEW);
    setCurrentDepartment({ name: '' });
    setReadOnlyEditForm(true);
    const department = await apiClient.getDepartment(recordId);
    if (department) setCurrentDepartment(department);
    setVisibleEditDialog(true);
  };

  const onCreateDepartment = () => {
    setMod(Mod.INSERT);
    setCurrentDepartment({ name: '' });
    setReadOnlyEditForm(false);
    setVisibleEditDialog(true);
  };

  const onEdit = () => {
    setMod(Mod.UPDATE);
    setReadOnlyEditForm(false);
  };

  const onClose = () => {
    setVisibleEditDialog(false);
  };

  const onCancel = () => {
    setVisibleEditDialog(false);
  };

  const onSave = async () => {
    if (mod === Mod.INSERT) {
      await apiClient.createDepartment({ name: formName });
    } else {
      if (currentDepartment) {
        const id = currentDepartment.id ?? '';
        const name = currentDepartment.name ?? '';
        await apiClient.editDepartment(id, { name });
      }
    }
    setVisibleEditDialog(false);
    if (hubConnection) {
      hubConnection.invoke("Send", "The department is updated");
    }
  };

  const onDelete = () => {
    let result = confirm('<h4>Are you sure?</h4>', 'Deleting the record');
    result.then((dialogResult) => {
      if (dialogResult) {
        deleteDepartment();
        setVisibleEditDialog(false);
        if (hubConnection) {
          hubConnection.invoke("Send", "The department is deleted");
        }
      }
    });
  };

  const onEditFormDataModified = (e: FieldDataChangedEvent) => {
    if (e.dataField && e.dataField === 'name') setFormName(e.value);
  };

  const deleteDepartment = async () => {
    await apiClient.deleteDepartment(currentDepartment?.id ?? '');
  }

  const [hubConnection, setHubConnection] = useState<any | null>(null);
  const webApiDepartmentsHubEnv: string = (process.env.REACT_APP_WEB_API_DEPARTMENTS_HUB as string);

  useEffect(() => {
    createHubConnection();
  }, []);

  const createHubConnection = async () => {
    const hubConnection = new HubConnectionBuilder()
      .withUrl(webApiDepartmentsHubEnv)
      .withAutomaticReconnect()
      .configureLogging(LogLevel.Information)
      .build();
    try {
      await hubConnection.start();
    } catch (e) {
      console.log("data", e);
    }
    setHubConnection(hubConnection);
  }


  useEffect(() => {
    if (hubConnection) {
      hubConnection.on("ReceiveMessage", (message: string) => {
        loadDepartments();
      });
    }
  }, [hubConnection]);

  let navigate = useNavigate();
  useEffect(() => {
    if (!getEmployeeInfo().isAuthorized)
      navigate("/");
  }, []);

  return (
    <div>
      <Popup
        visible={isVisibleEditDialog}
        dragEnabled={false}
        closeOnOutsideClick={true}
        showCloseButton={false}
        showTitle={true}
        title='Department'
        container='.dx-viewport'
        width={500}
        height={200}
      >
        <div className='department-modal-contrainer'>
          <Form
            formData={currentDepartment}
            onFieldDataChanged={onEditFormDataModified}
            readOnly={isReadOnlyEditForm}
          >
            <GroupItem colSpan={3}>
              <SimpleItem dataField='name' />
            </GroupItem>
          </Form>

          <CommandPanel
            onEdit={onEdit}
            onClose={onClose}
            onCancel={onCancel}
            onSave={onSave}
            onDelete={onDelete}
            mod={mod}
          //message={error ? { isVisible: true, text: error } : undefined}
          />
        </div>
      </Popup>

      <DataGrid
        dataSource={departments}
        keyExpr='id'
        showColumnLines={true}
        showRowLines={true}
        rowAlternationEnabled={true}
        showBorders={true}
        id='dgDepartment'
        columns={[
          {
            caption: 'Name',
            minWidth: 150,
            cellTemplate: function (container, options) {
              let divContainer = document.createElement('div');
              divContainer.className = 'departments-iconed-link-container';
              let img = document.createElement('img');
              img.width = 16;
              img.height = 16;
              img.className = 'departments-iconed-link-icon';
              img.title = options?.data?.name ?? '';
              img.src = 'images/statuses/default/active.png';
              let divName = document.createElement('div');
              divName.className = 'departments-iconed-link-cell';
              divName.textContent = options?.data?.name ?? '';
              divName.onclick = function () {
                rowClick(options?.data?.id ?? '');
              };
              divContainer.append(img);
              divContainer.append(divName);
              container.append(divContainer);
            },
          },
        ]}
      ></DataGrid>

      <Button
        className='cp-btn'
        key='btn-Close'
        text='Add new'
        type='default'
        stylingMode='contained'
        onClick={onCreateDepartment}
        width={100}
      />
    </div>
  );
};

export default Departments;