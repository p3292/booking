import {
  Client,
  DepartmentResponse,
  EmployeeResponse,
  EmployeeShortResponse,
  ParkingPlaceResponse,
  ParkingResponse,
  ReservationResponse,
  ResourceType,
  WorkPlaceResponse,
} from '../../../api/api';

export const apiClient = new Client();
export const EMPTY_ID = '00000000-0000-0000-0000-000000000000';

export interface IEmployee {
  id?: string;
  name?: string | undefined;
}

export interface ICurrentRecord {
  id?: string;
  endDateTime?: Date;
  startDateTime?: Date;
  bookingPlaceId?: string;
  resourceTypeId?: string;
  employeeId?: string;
  isCancelled?: boolean;
}

export let defaultRecord: ICurrentRecord = {
  id: EMPTY_ID,
  endDateTime: new Date(),
  startDateTime: new Date(),
  bookingPlaceId: '',
  resourceTypeId: '',
  employeeId: EMPTY_ID,
  isCancelled: false,
};

export interface IBookingPlace {
  id?: string;
  owner?: EmployeeResponse;
  number?: string | undefined;
  lockTimeStart?: Date;
  duration?: number;
  resourceTypeId?: string;
  resourceType?: ResourceType;

  department?: DepartmentResponse;
  building?: string | undefined;
  floor?: number;

  parking?: ParkingResponse;
  name?: string;
}

export const getUseName = (options: any) =>
  `${options?.data?.employee?.firstName ?? ''}, ${
    options?.data?.employee?.lastName ?? ''
  }`;

export const loadBookPlacees = async (
  resourceTypeId: string,
  setBookingPlacees: any
) => {
  const wpGeneral: IBookingPlace[] = [...(await apiClient.getWorkPlaces()) as WorkPlaceResponse[]];
  const ppGeneral: IBookingPlace[] = [...(await apiClient.getParkingPlaces()) as ParkingPlaceResponse[]];

  const wp = (wpGeneral).map(
    (item) => {
      return {...item, name: `${item.building}: ${item.floor}: ${item.number} (${item.department?.name})`};
    }
  );
 
  const pp = (ppGeneral).map(
    (item) => { 
      return {...item, name: `${item.parking?.name}: ${item.number}`} 
    }
  );

  const result: IBookingPlace[] = [...wp, ...pp];

  
  const resultFilter = result.filter(
    (item) => item.resourceTypeId === resourceTypeId
  );
  setBookingPlacees(resultFilter);
};

export const loadBindingData = async (
  setEmployeesFunc: any,
  setResourceTypesFunc: any
) => {
  const items = (await apiClient.getEmployees()) as EmployeeShortResponse[];
  const employees: IEmployee[] = items.map(({ id, firstName, lastName }) => {
    return { id, name: `${firstName}, ${lastName}` };
  });
  setEmployeesFunc(employees);

  const resourceTypes = (await apiClient.getResourceTypes()) as any;
  setResourceTypesFunc(resourceTypes);
};

export const loadReservationItems = async (setReservationsFunc: any) => {
  const items = (await apiClient.getBookingList()) as ReservationResponse[];
  setReservationsFunc(items);
};

export const deleteReservation = async (id: string) => {
  await apiClient.deleteBooking(id);
};

export const validateForm = (
  currentRecord: ICurrentRecord | undefined
): string => {
  if (currentRecord) {
    if (currentRecord.employeeId === undefined) return 'Please select employee';
    if (currentRecord.resourceTypeId === undefined)
      return 'Please select resource type';
    if (currentRecord.bookingPlaceId === undefined)
      return 'Please select booking place';
    if (currentRecord.startDateTime === undefined)
      return 'Please select start date';
    if (currentRecord.endDateTime === undefined)
      return 'Please select end date';
  }
  return '';
};
