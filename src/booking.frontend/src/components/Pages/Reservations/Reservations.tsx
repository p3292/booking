import React, { useContext, useEffect, useState } from "react";
import { DataGrid } from "devextreme-react/data-grid";
import { Button, Form, Popup } from "devextreme-react";
import { GroupItem, SimpleItem, Item } from "devextreme-react/form";
import CommandPanel from "../../CommandPanel";
import { Mod } from "../../CommandPanel/CommandPanel";
import { FieldDataChangedEvent } from "devextreme/ui/form";
import { confirm } from "devextreme/ui/dialog";
import {
  apiClient,
  defaultRecord,
  deleteReservation,
  EMPTY_ID,
  getUseName,
  IBookingPlace,
  ICurrentRecord,
  IEmployee,
  loadBindingData,
  loadBookPlacees,
  loadReservationItems,
  validateForm,
} from "./General";
import {
  ReservationRequest,
  ReservationResponse,
  ResourceType,
} from "../../../api/api";
import "./Reservations.css";
import no_photo from "../../../images/employees/no_photo.jpg";
import Rychkov_Andrey from "../../../images/employees/Rychkov_Andrey.jpg";
import Alexeev_Alexey from "../../../images/employees/Alexeev_Alexey.jpg";
import Banenko_Sergey from "../../../images/employees/Banenko_Sergey.jpg";
import Molodtsov_Denis from "../../../images/employees/Molodtsov_Denis.jpg";
import Zhalnin_Aleksey from "../../../images/employees/Zhalnin_Aleksey.jpg";
import Alvarez_Geovanny from "../../../images/employees/Alvarez_Geovanny.jpg";
import Guiet_Nathan from "../../../images/employees/Guiet_Nathan.jpg";
import Jegal_Sooyoun from "../../../images/employees/Jegal_Sooyoun.jpg";
import Rue_Adam from "../../../images/employees/Rue_Adam.jpg";
import Uchuno_James from "../../../images/employees/Uchuno_James.jpg";
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { useNavigate } from 'react-router-dom';
import { getEmployeeInfo } from '../../../api/utilites';

const Reservations: React.FC = () => {
  const [reservations, setReservations] = useState<ReservationResponse[]>();
  const [employees, setEmployees] = useState<IEmployee[]>();
  const [resourceTypes, setResourceTypes] = useState<ResourceType[]>();
  const [bookingPlacees, setBookingPlacees] = useState<IBookingPlace[]>();
  const [isVisibleEditDialog, setVisibleEditDialog] = useState<boolean>(false);
  const [isReadOnlyEditForm, setReadOnlyEditForm] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>();
  const [mod, setMod] = useState<Mod>(Mod.VIEW);
  const [currentRecord, setCurrentRecord] = useState<ICurrentRecord>();
  const [employeeId, setEmployeeId] = useState<string>();

  useEffect(() => {
    loadReservationItems(setReservations);
    loadBindingData(setEmployees, setResourceTypes);
    onLoadKeyHandle();
  }, []);

  const onLoadKeyHandle = () => {
    const handleEsc = (event: { keyCode: number }) => {
      if (event.keyCode === 27) {
          if(mod == Mod.VIEW)
          setVisibleEditDialog(false);
      }
    };
    window.addEventListener("keydown", handleEsc);
    return () => {
      window.removeEventListener("keydown", handleEsc);
    };
  };
  const rowClick = async (recordId: string) => {
    setMod(Mod.VIEW);
    setCurrentRecord(defaultRecord);
    setReadOnlyEditForm(true);
    setErrorMessage("");
    setEmployeeId("");
    const booking = await apiClient.getBookingById(recordId);

    if (booking) {
      let curr: ICurrentRecord = {
        id: booking.id,
        endDateTime: booking.endDateTime,
        startDateTime: booking.startDateTime,
        bookingPlaceId: booking.bookPlaceId,
        resourceTypeId: booking.bookPlace?.resourceTypeId,
        employeeId: booking.employeeId,
        isCancelled: booking.isCancelled,
      };
      loadBookPlacees(booking.bookPlace?.resourceTypeId ?? "",setBookingPlacees);
      setCurrentRecord(curr);
      setEmployeeId(curr.employeeId);
    }
    setVisibleEditDialog(true);
  };

  const onCreateReservation = () => {
    setMod(Mod.INSERT);
    setCurrentRecord({});
    setReadOnlyEditForm(false);
    setVisibleEditDialog(true);
    setErrorMessage("");
    setEmployeeId("");
  };

  const onEdit = () => {
    setMod(Mod.UPDATE);
    setReadOnlyEditForm(false);
  };

  const onClose = () => {
    setVisibleEditDialog(false);
  };

  const onCancel = () => {
    setReadOnlyEditForm(true);
    setMod(Mod.VIEW);
    if(mod==Mod.INSERT)
      setVisibleEditDialog(false);
  };

  const onSave = async () => {
    const validatinResult = validateForm(currentRecord);
    if (validatinResult) {
      setErrorMessage(validatinResult);
      return;
    }

    let recordId = mod === Mod.INSERT ? EMPTY_ID : currentRecord?.id;
    const data: ReservationRequest = {
      id: recordId,
      bookPlaceId: currentRecord?.bookingPlaceId,
      resourceTypeId: currentRecord?.resourceTypeId,
      employeeId: currentRecord?.employeeId,
      startDateTime: currentRecord?.startDateTime,
      endDateTime: currentRecord?.endDateTime,
      //endDateTimeFact?: Date;
      bookingType: true,
      isCancelled: currentRecord?.isCancelled,
    };

    if (mod === Mod.INSERT) {
      //const aaa = await apiClient.createBooking(data);
      try {
        await await apiClient.createBooking(data);
      } catch (err) {
        setErrorMessage(
          "You cannot create an reservation: duplicate or ban exists"
        );
        return;
      }
    } else {
      if (currentRecord) {
        try {
          // data.employeeId replace on current user
          await apiClient.updateBooking(data.employeeId, data);
        } catch (err) {
          setErrorMessage("You cannot change an reservation.");
          return;
        }
      }
    }
    setVisibleEditDialog(false);
    if (hubConnection) {
      hubConnection.invoke("Send", "The reservation is updated");
    }
  };

  const onDelete = () => {
    let result = confirm("<h4>Are you sure?</h4>", "Deleting the record");
    result.then((dialogResult) => {
      if (dialogResult) {
        deleteReservation(currentRecord?.id ?? "");
        setVisibleEditDialog(false);
        if (hubConnection) {
          hubConnection.invoke("Send", "The reservation is deleted");
        }
      }
    });
  };

  const onEditFormDataModified = (e: FieldDataChangedEvent) => {
    if (e.dataField && currentRecord) {
      if (e.dataField === "resourceTypeId") {
        loadBookPlacees(e.value,setBookingPlacees);
        currentRecord.resourceTypeId = e.value;
      }
      if (e.dataField === "employeeId") {
        currentRecord.employeeId = e.value;
        setEmployeeId(e.value);
      }
      if (e.dataField === "bookingPlaceId") {
        currentRecord.bookingPlaceId = e.value;
      }
      if (e.dataField === "startDateTime") {
        currentRecord.startDateTime = e.value;
      }
      if (e.dataField === "endDateTime") {
        currentRecord.endDateTime = e.value;
      }
      if (e.dataField === "isCancelled") {
        currentRecord.isCancelled = e.value;
      }
    }
  };

  const renderPicture = (data: any) => {
    const result = employees?.find((obj) => {
      return obj.id === currentRecord?.employeeId;
    });
    const lastName = result?.name?.split(", ")[1];
    const images = [];
    images[0] = <img src={no_photo} />;
    images[1] = <img src={Rychkov_Andrey} />;
    images[2] = <img src={Alexeev_Alexey} />;
    images[3] = <img src={Molodtsov_Denis} />;
    images[4] = <img src={Banenko_Sergey} />;
    images[5] = <img src={Zhalnin_Aleksey} />;
    images[6] = <img src={Alvarez_Geovanny} />;
    images[7] = <img src={Guiet_Nathan} />;
    images[8] = <img src={Jegal_Sooyoun} />;
    images[9] = <img src={Rue_Adam} />;
    images[10] = <img src={Uchuno_James} />;
    let index = 0;

    if (lastName?.startsWith("R", 0)) {
      index = 1;
    } else if (lastName?.startsWith("A", 0)) {
      index = 2;
    } else if (lastName?.startsWith("M", 0)) {
      index = 3;
    } else if (lastName?.startsWith("B", 0)) {
      index = 4;
    } else if (lastName?.startsWith("Z", 0)) {
      index = 5;
    } else if (lastName?.startsWith("G", 0)) {
      index = 6;
    } else if (lastName?.startsWith("N", 0)) {
      index = 7;
    } else if (lastName?.startsWith("J", 0)) {
      index = 8;
    } else if (lastName?.startsWith("R", 0)) {
      index = 9;
    } else if (lastName?.startsWith("U", 0)) {
      index = 10;
    } else index = 5;

    if (mod === Mod.INSERT) index = 0;

    return <div className="form-avatar">{images[index]}</div>;
  };

  const [hubConnection, setHubConnection] = useState<any | null>(null);
  const webApiReservationsHubEnv: string = process.env
    .REACT_APP_WEB_API_RESERVATIONS_HUB as string;

  useEffect(() => {
    createHubConnection();
  }, []);

  const createHubConnection = async () => {
    const hubConnection = new HubConnectionBuilder()
      .withUrl(webApiReservationsHubEnv)
      .withAutomaticReconnect()
      .configureLogging(LogLevel.Information)
      .build();
    try {
      await hubConnection.start();
    } catch (e) {
      console.log("data", e);
    }
    setHubConnection(hubConnection);
  };

  useEffect(() => {
    if (hubConnection) {
      hubConnection.on("ReceiveMessage", (message: string) => {
        loadReservationItems(setReservations);
      });
    }
  }, [hubConnection]);

  let navigate = useNavigate();
  useEffect(() => {
    if (!getEmployeeInfo().isAuthorized)
      navigate("/");
  }, []);

  const renderPictureNew = (data: any) => {
    if( employeeId != "")
       return ( <div className='form-avatar'><img src={"https://robohash.org/"+employeeId } /></div>)
     else 
     return ( <div className='form-avatar'><img src={no_photo}/></div>)
 }
 
  return (
    <div>
      <Popup
        visible={isVisibleEditDialog}
        dragEnabled={false}
        closeOnOutsideClick={true}
        showCloseButton={false}
        showTitle={true}
        title="Reservation"
        container=".dx-viewport"
        width={950}
        height={500}
      >
        <div className="department-modal-contrainer">
          <Form
            formData={currentRecord}
            onFieldDataChanged={onEditFormDataModified}
            readOnly={isReadOnlyEditForm}
          >
            <GroupItem cssClass="first-group" colCount={4}>
              <SimpleItem render={renderPictureNew}></SimpleItem>
              <GroupItem colSpan={3}>
                <Item
                  label={{ text: "Employee" }}
                  dataField="employeeId"
                  editorType="dxSelectBox"
                  editorOptions={{
                    items: employees,
                    displayExpr: "name",
                    valueExpr: "id",
                    searchEnabled: true,
                    // value: {frmEmployee},
                  }}
                />
                <Item
                  label={{ text: "Resource type" }}
                  dataField="resourceTypeId"
                  editorType="dxSelectBox"
                  editorOptions={{
                    items: resourceTypes,
                    displayExpr: "name",
                    valueExpr: "id",
                    searchEnabled: true,
                  }}
                />
                <Item
                  label={{ text: "Booking place" }}
                  dataField="bookingPlaceId"
                  editorType="dxSelectBox"
                  editorOptions={{
                    items: bookingPlacees,
                    displayExpr: "name",
                    valueExpr: "id",
                    searchEnabled: true,
                  }}
                />
              </GroupItem>
            </GroupItem>
            <GroupItem cssClass="second-group" colCount={2}>
              <SimpleItem
                label={{ text: "Start date time" }}
                dataField="startDateTime"
                editorType="dxDateBox"
                editorOptions={{
                  width: "150px",
                  displayFormat: "dd.MM.yyyy HH:mm",
                  type: "datetime",
                }}
              />
              <SimpleItem
                label={{ text: "End date time" }}
                dataField="endDateTime"
                editorType="dxDateBox"
                editorOptions={{
                  width: "150px",
                  displayFormat: "dd.MM.yyyy HH:mm",
                  type: "datetime",
                }}
              />
              <Item
                label={{ text: "Is cancelled" }}
                dataField="isCancelled"
                editorType="dxCheckBox"
                visible={mod != Mod.INSERT}
              />
            </GroupItem>
          </Form>

          <CommandPanel
            onEdit={onEdit}
            onClose={onClose}
            onCancel={onCancel}
            onSave={onSave}
            onDelete={onDelete}
            mod={mod}
            message={
              errorMessage ? { isVisible: true, text: errorMessage } : undefined
            }
          />
        </div>
      </Popup>

      <DataGrid
        dataSource={reservations}
        keyExpr="id"
        showColumnLines={false}
        showRowLines={true}
        rowAlternationEnabled={true}
        showBorders={true}
        id="dgReservation"
        columns={[
          {
            caption: "Employee",
            minWidth: 150,
            cellTemplate: function (container, options) {
              let divContainer = document.createElement("div");
              divContainer.className = "reservations-iconed-link-container";
              let img = document.createElement("img");
              img.width = 16;
              img.height = 16;
              img.className = "reservations-iconed-link-icon";
              img.title = getUseName(options);
              img.src = "images/statuses/default/active.png";
              let divName = document.createElement("div");
              divName.className = "reservations-iconed-link-cell";
              divName.textContent = getUseName(options);
              divName.onclick = function () {
                rowClick(options?.data?.id ?? "");
              };
              divContainer.append(img);
              divContainer.append(divName);
              container.append(divContainer);
            },
          },
          {
            dataField: "bookPlace.resourceType.name",
            caption: "Resource type",
            alignment: "left",
            minWidth: 150,
          },

          {
            dataField: "bookPlace.number",
            caption: "Number",
            alignment: "left",
            minWidth: 150,
          },

          {
            dataField: "startDateTime",
            caption: "Start date",
            dataType: "datetime",
            format: "dd.MM.yyyy HH:mm",
            alignment: "left",
            width: 150,
          },
          {
            dataField: "endDateTime",
            caption: "End date",
            dataType: "datetime",
            format: "dd.MM.yyyy HH:mm",
            alignment: "left",
            width: 150,
          },
        ]}
      ></DataGrid>

      <Button
        className="cp-btn"
        key="btn-Close"
        text="Add new"
        type="default"
        stylingMode="contained"
        onClick={onCreateReservation}
        width={100}
      />
    </div>
  );
};

export default Reservations;
