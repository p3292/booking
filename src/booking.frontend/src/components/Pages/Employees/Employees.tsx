import {
  EmployeeShortResponse,
  CreateOrEditEmployeeRequest,
  DepartmentShortResponse,
  RoleShortResponse,
  EmployeeResponse,
} from '../../../api/api';
import React, { useEffect, useLayoutEffect, useState } from 'react';
import { DataGrid } from 'devextreme-react/data-grid';
import { Button, Form, Popup } from 'devextreme-react';
import CommandPanel from '../../CommandPanel';
import { Mod } from '../../CommandPanel/CommandPanel';
import { GroupItem, SimpleItem, Item } from 'devextreme-react/form';
import { FieldDataChangedEvent } from 'devextreme/ui/form';
import { confirm } from 'devextreme/ui/dialog';
import './Employees.css';
import { apiClient, loadBindingData, loadEmployees, validateForm } from './General';
import no_photo from '../../../images/employees/no_photo.jpg';
import Rychkov_Andrey from '../../../images/employees/Rychkov_Andrey.jpg';
import Alexeev_Alexey from '../../../images/employees/Alexeev_Alexey.jpg';
import Banenko_Sergey from '../../../images/employees/Banenko_Sergey.jpg';
import Molodtsov_Denis from '../../../images/employees/Molodtsov_Denis.jpg';
import Zhalnin_Aleksey from '../../../images/employees/Zhalnin_Aleksey.jpg';
import Alvarez_Geovanny from '../../../images/employees/Alvarez_Geovanny.jpg';
import Guiet_Nathan from '../../../images/employees/Guiet_Nathan.jpg';
import Jegal_Sooyoun from '../../../images/employees/Jegal_Sooyoun.jpg';
import Rue_Adam from '../../../images/employees/Rue_Adam.jpg';
import Uchuno_James from '../../../images/employees/Uchuno_James.jpg';
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { getEmployeeInfo } from '../../../api/utilites';
import { useNavigate } from 'react-router-dom';

const defaultEmployee: EmployeeShortResponse = {
  id: '-1',
  firstName: '',
  lastName: '',
  email: '',
};

const Employees: React.FC = () => {
  const [employees, setEmployees] = useState<EmployeeShortResponse[]>();
  const [departments, setDepartments] = useState<DepartmentShortResponse[]>();
  const [roles, setRoles] = useState<RoleShortResponse[]>();
  const [errorMessage, setErrorMessage] = useState<string>();
  const [isVisibleEditDialog, setVisibleEditDialog] = useState<boolean>(false);
  const [isReadOnlyEditForm, setReadOnlyEditForm] = useState<boolean>(false);
  const [mod, setMod] = useState<Mod>(Mod.VIEW);
  const [currentEmployee, setCurrentEmployee] = useState<EmployeeResponse>();

  useEffect(() => {
    loadEmployees(setEmployees);
    loadBindingData(setDepartments, setRoles);
  }, []);

  const getFullName = (options: any) =>
    `${options?.data?.firstName ?? ''}, ${options?.data?.lastName ?? ''}`;

  const rowClick = async (recordId: string) => {
    setErrorMessage('');
    setMod(Mod.VIEW);
    setCurrentEmployee(defaultEmployee);
    setReadOnlyEditForm(true);
    const employee = await apiClient.getEmployee(recordId);
    if (employee) setCurrentEmployee(employee);
    setVisibleEditDialog(true);
  };

  const onCreateEmployee = () => {
    setErrorMessage('');
    setMod(Mod.INSERT);
    setCurrentEmployee(defaultEmployee);
    setReadOnlyEditForm(false);
    setVisibleEditDialog(true);
  };

  const onEdit = () => {
    setMod(Mod.UPDATE);
    setReadOnlyEditForm(false);
  };

  const onClose = () => {
    setVisibleEditDialog(false);
  };

  const onCancel = () => {
    setVisibleEditDialog(false);
  };

  const onSave = async () => {
    const validatinResult = validateForm(currentEmployee);
    if (validatinResult) {
      setErrorMessage(validatinResult);
      return;
    }

    let data: CreateOrEditEmployeeRequest = {
      firstName: currentEmployee?.firstName,
      lastName: currentEmployee?.lastName,
      middleName: currentEmployee?.middleName,
      isAdmin: currentEmployee?.isAdmin,
      canUseSystem: currentEmployee?.canUseSystem,
      roleId: currentEmployee?.role?.id,
      departmentId: currentEmployee?.department?.id,
      login: currentEmployee?.login,
      email: currentEmployee?.email,
    };

    try {
      if (mod === Mod.INSERT) {
        await apiClient.createEmployee(data);
      } else {
        if (currentEmployee) {
          const id = currentEmployee.id ?? '';
          await apiClient.editEmployees(id, data);
        }
      }
    } catch (err: any) {
      let errMessage = '';
      const errors = JSON.parse(err.response).errors;
      for (let key in errors) {
        errMessage += errors[key][0] + " ";
        break;
      }

      setErrorMessage(errMessage);
      return;
    }

    setVisibleEditDialog(false);

    if (hubConnection) {
      hubConnection.invoke("Send", "The employee is updated");
    }
    //loadEmployees(setEmployees);
  };

  const onDelete = () => {
    let result = confirm('<h4>Are you sure?</h4>', 'Deleting the record');
    result.then((dialogResult) => {
      if (dialogResult) {
        deleteEmployee();
        setVisibleEditDialog(false);

        if (hubConnection) {
          hubConnection.invoke("Send", "The employee is deleted");
        }
        //loadEmployees(setEmployees);
      }
    });
  };

  const onEditFormDataModified = (e: FieldDataChangedEvent) => {
    if (e.dataField && currentEmployee) {
      if (e.dataField === 'firstName') {
        currentEmployee.firstName = e.value;
      }
      if (e.dataField === 'lastName') {
        currentEmployee.lastName = e.value;
      }
      if (e.dataField === 'middleName') {
        currentEmployee.middleName = e.value;
      }
      if (e.dataField === 'login') {
        currentEmployee.login = e.value;
      }
      if (e.dataField === 'email') {
        currentEmployee.email = e.value;
      }
      if (e.dataField === 'department.id') {
        if (currentEmployee?.department?.id)
          currentEmployee.department.id = e.value;
      }
      if (e.dataField === 'role.id') {
        if (currentEmployee?.role?.id) currentEmployee.role.id = e.value;
      }
      if (e.dataField === 'canUseSystem') {
        currentEmployee.canUseSystem = e.value;
      }
      if (e.dataField === 'isAdmin') {
        currentEmployee.isAdmin = e.value;
      }
    }
  };

  const deleteEmployee = async () => {
    await apiClient.deleteEmployee(currentEmployee?.id ?? '');
  };


  const renderPicture = (data: any) => {
    const images = [];
    images[0] = <img src={no_photo} />;
    images[1] = <img src={Rychkov_Andrey} />;
    images[2] = <img src={Alexeev_Alexey} />;
    images[3] = <img src={Molodtsov_Denis} />;
    images[4] = <img src={Banenko_Sergey} />;
    images[5] = <img src={Zhalnin_Aleksey} />;
    images[6] = <img src={Alvarez_Geovanny} />;
    images[7] = <img src={Guiet_Nathan} />;
    images[8] = <img src={Jegal_Sooyoun} />;
    images[9] = <img src={Rue_Adam} />;
    images[10] = <img src={Uchuno_James} />;

    let index = 0;

    if (currentEmployee?.lastName?.startsWith('R', 0)) { index = 1; }
    else if (currentEmployee?.lastName?.startsWith('A', 0)) { index = 2; }
    else if (currentEmployee?.lastName?.startsWith('M', 0)) { index = 3; }
    else if (currentEmployee?.lastName?.startsWith('B', 0)) { index = 4; }
    else if (currentEmployee?.lastName?.startsWith('Z', 0)) { index = 5; }
    else if (currentEmployee?.lastName?.startsWith('G', 0)) { index = 6; }
    else if (currentEmployee?.lastName?.startsWith('N', 0)) { index = 7; }
    else if (currentEmployee?.lastName?.startsWith('J', 0)) { index = 8; }
    else if (currentEmployee?.lastName?.startsWith('R', 0)) { index = 9; }
    else if (currentEmployee?.lastName?.startsWith('U', 0)) { index = 10; }
    else index = 9;

    if (mod === Mod.INSERT)
      index = 0;

    return <div className="form-avatar">{images[index]}</div>;
  }

  const [hubConnection, setHubConnection] = useState<any | null>(null);
  const webApiEmployeesHubEnv: string = (process.env.REACT_APP_WEB_API_EMPLOYEES_HUB as string);

  useEffect(() => {
    createHubConnection();
  }, []);

  const createHubConnection = async () => {
    const hubConnection = new HubConnectionBuilder()
      .withUrl(webApiEmployeesHubEnv)
      .withAutomaticReconnect()
      .configureLogging(LogLevel.Information)
      .build();
    try {
      await hubConnection.start();
    } catch (e) {
      console.log("data", e);
    }
    setHubConnection(hubConnection);
  }

  useEffect(() => {
    if (hubConnection) {
      hubConnection.on("ReceiveMessage", (message: string) => {
        loadEmployees(setEmployees);
      });
    }
  }, [hubConnection]);

  let navigate = useNavigate();
  useEffect(() => {
    if (!getEmployeeInfo().isAuthorized)
      navigate("/");
  }, []);



  return (
    <div>
      <Popup
        visible={isVisibleEditDialog}
        dragEnabled={false}
        closeOnOutsideClick={true}
        showCloseButton={false}
        showTitle={true}
        title='Employee'
        container='.dx-viewport'
        width={950}
        height={500}
      >
        <div className='department-modal-contrainer'>
          <Form
            formData={currentEmployee}
            onFieldDataChanged={onEditFormDataModified}
            readOnly={isReadOnlyEditForm}
          >
            <GroupItem cssClass="first-group" colCount={4}>
              <SimpleItem render={renderPicture}></SimpleItem>
              <GroupItem colSpan={3}>
                <SimpleItem dataField='firstName' label={{ text: 'First name' }} />
                <SimpleItem dataField='lastName' label={{ text: 'Last name' }} />
                <SimpleItem dataField='middleName' label={{ text: 'Middle name' }} />
              </GroupItem>
            </GroupItem>
            <GroupItem cssClass="second-group" colCount={2}>
              <GroupItem>
                <SimpleItem dataField='login' />
                <SimpleItem dataField='email' />
                <Item
                  label={{ text: 'Department' }}
                  dataField='department.id'
                  editorType='dxSelectBox'
                  editorOptions={{
                    items: departments,
                    displayExpr: 'name',
                    valueExpr: 'id',
                    searchEnabled: true,
                  }}
                />
              </GroupItem>
              <GroupItem>
                <Item
                  label={{ text: 'Role' }}
                  dataField='role.id'
                  editorType='dxSelectBox'
                  editorOptions={{
                    items: roles,
                    displayExpr: 'name',
                    valueExpr: 'id',
                    searchEnabled: true,
                  }}
                />
                <Item
                  label={{ text: 'Is admin' }}
                  dataField='isAdmin'
                  editorType='dxCheckBox'
                />
                <Item
                  label={{ text: 'Can use system' }}
                  dataField='canUseSystem'
                  editorType='dxCheckBox'
                />
              </GroupItem>
            </GroupItem>
          </Form>

          <CommandPanel
            onEdit={onEdit}
            onClose={onClose}
            onCancel={onCancel}
            onSave={onSave}
            onDelete={onDelete}
            mod={mod}
            message={
              errorMessage ? { isVisible: true, text: errorMessage } : undefined
            }
          />
        </div>
      </Popup>

      <DataGrid
        dataSource={employees}
        keyExpr='id'
        showColumnLines={true}
        showRowLines={true}
        rowAlternationEnabled={true}
        showBorders={true}
        id='dgEmployee'

        columns={[
          {
            caption: 'Name',
            minWidth: 150,
            cellTemplate: function (container, options) {
              let divContainer = document.createElement('div');
              divContainer.className = 'employees-iconed-link-container';
              let img = document.createElement('img');
              img.width = 16;
              img.height = 16;
              img.className = 'employees-iconed-link-icon';
              img.title = getFullName(options);
              img.src = 'images/statuses/default/active.png';
              let divName = document.createElement('div');
              divName.className = 'employees-iconed-link-cell';
              divName.textContent = getFullName(options);
              divName.onclick = function () {
                rowClick(options?.data?.id ?? '');
              };
              divContainer.append(img);
              divContainer.append(divName);
              container.append(divContainer);
            },
          },
          {
            dataField: 'email',
            caption: 'Email',
            alignment: 'left',
            minWidth: 150,
          },
        ]}
      ></DataGrid>

      <Button
        className='cp-btn'
        key='btn-Close'
        text='Add new'
        type='default'
        stylingMode='contained'
        onClick={onCreateEmployee}
        width={100}
      />
    </div>
  );
};

export default Employees;