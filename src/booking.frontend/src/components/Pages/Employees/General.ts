import {
  Client,
  DepartmentShortResponse,
  EmployeeResponse,
  EmployeeShortResponse,
  RoleShortResponse,
} from '../../../api/api';

export const apiClient = new Client();

export const loadBindingData = async (
  setDepartmentsFunc: any,
  setRolesFunc: any
) => {
  const departmentList =
    (await apiClient.getDepartments()) as DepartmentShortResponse[];
  setDepartmentsFunc(departmentList);

  const roleList = (await apiClient.getRoles()) as RoleShortResponse[];
  setRolesFunc(roleList);
};

export const loadEmployees = async (setEmployeesFunc: any) => {
  const items = (await apiClient.getEmployees()) as EmployeeShortResponse[];
  setEmployeesFunc(items);
};

export const validateForm = (
  currentRecord: EmployeeResponse | undefined
): string => {
  if (currentRecord) {
    if (currentRecord.firstName === undefined || currentRecord.firstName === '')
      return 'Please select first name';

    if (currentRecord.lastName === undefined || currentRecord.lastName === '') return 'Please select last name';

    if (currentRecord.login === undefined || currentRecord.login === '') return 'Please select login';

    if (currentRecord.email === undefined || currentRecord.email === '') return 'Please select email';

    if (currentRecord.department?.id === undefined)
      return 'Please select department';
    if (currentRecord.role?.id === undefined) return 'Please select role';
  }
  return '';
};
