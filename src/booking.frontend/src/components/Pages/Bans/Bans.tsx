import React, { useEffect, useState } from "react";

// devextreme:
import { DataGrid } from "devextreme-react/data-grid";
import { Button, Form, Popup } from "devextreme-react";
import { GroupItem, SimpleItem, Item } from "devextreme-react/form";
import CommandPanel from "../../CommandPanel";
import { Mod } from "../../CommandPanel/CommandPanel";
import { FieldDataChangedEvent } from "devextreme/ui/form";
import { confirm } from "devextreme/ui/dialog";
import no_photo from '../../../images/employees/no_photo.jpg';
import Rychkov_Andrey from '../../../images/employees/Rychkov_Andrey.jpg';
import Alexeev_Alexey from '../../../images/employees/Alexeev_Alexey.jpg';
import Banenko_Sergey from '../../../images/employees/Banenko_Sergey.jpg';
import Molodtsov_Denis from '../../../images/employees/Molodtsov_Denis.jpg';
import Zhalnin_Aleksey from '../../../images/employees/Zhalnin_Aleksey.jpg';
import Alvarez_Geovanny from '../../../images/employees/Alvarez_Geovanny.jpg';
import Guiet_Nathan from '../../../images/employees/Guiet_Nathan.jpg';
import Jegal_Sooyoun from '../../../images/employees/Jegal_Sooyoun.jpg';
import Rue_Adam from '../../../images/employees/Rue_Adam.jpg';
import Uchuno_James from '../../../images/employees/Uchuno_James.jpg';
import {
  BanForUserRequest,
  BanResponse,
  Client,
  EmployeeShortResponse,
  ResourceType,
} from "../../../api/api";

// css:
import "./Bans.css";
import { getEmployeeInfo } from "../../../api/utilites";
import { useNavigate } from "react-router-dom";
import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";

/*export*/ interface IEmployee {
  id?: string;
  name?: string | undefined;
}

/*export*/ interface ICurrentRecord {
  id?: string;
  resourceTypeId?: string;
  employeeId?: string;
  startDateTime?: Date;
  endDateTime?: Date | undefined;
}

/*export*/ const loadBindingData = async (
  setEmployeesFunc: any,
  setResourceTypesFunc: any
) => {
  const items = (await apiClient.getEmployees()) as EmployeeShortResponse[];
  const employees: IEmployee[] = items.map(({ id, firstName, lastName }) => {
    return { id, name: `${firstName}, ${lastName}` };
  });
  setEmployeesFunc(employees);

  const resourceTypes = (await apiClient.getResourceTypes()) as any;
  setResourceTypesFunc(resourceTypes);
};

/*export*/ const EMPTY_ID = "00000000-0000-0000-0000-000000000000";

/*export*/ let defaultRecord: ICurrentRecord = {
  id: EMPTY_ID,
  resourceTypeId: "" /*EMPTY_ID*/,
  employeeId: EMPTY_ID,
  startDateTime: new Date(),
  endDateTime: new Date(),
};

/*export*/ const getUseName = (options: any) =>
  `${options?.data?.employee?.firstName ?? ""}, ${options?.data?.employee?.lastName ?? ""
  }`;

const apiClient = new Client();

const Bans: React.FC = () => {
  const [bans, setBans] = useState<BanResponse[]>();
  const [employees, setEmployees] = useState<IEmployee[]>();
  const [resourceTypes, setResourceTypes] = useState<ResourceType[]>();
  const [isVisibleEditDialog, setVisibleEditDialog] = useState<boolean>(false);
  const [isReadOnlyEditForm, setReadOnlyEditForm] = useState<boolean>(false);

  // const [formName, setFormName] = useState<string>("");

  const [mod, setMod] = useState<Mod>(Mod.VIEW);
  const [errorMessage, setErrorMessage] = useState<string>();
  const [currentRecord, setCurrentRecord] = useState<ICurrentRecord>();

  useEffect(() => {
    loadBans();

    loadBindingData(setEmployees, setResourceTypes);
  }, []);

  const loadBans = async () => {
    const items = (await apiClient.getBanList()) as BanResponse[];
    setBans(items);
  };

  const rowClick = async (recordId: string) => {
    setMod(Mod.VIEW);
    setCurrentRecord(defaultRecord);
    setReadOnlyEditForm(true);
    setErrorMessage("");

    const ban = await apiClient.getBan(recordId);
    if (ban) {
      let curr: ICurrentRecord = {
        id: ban.id,

        //endDateTime: ban.endDateTime,
        endDateTime: ban.endDateTime == null ? undefined : ban.endDateTime,

        startDateTime: ban.startDateTime,
        resourceTypeId: ban.resourceType?.id,
        employeeId: ban.employee?.id,
      };

      setCurrentRecord(curr);
    }
    setVisibleEditDialog(true);
  };

  const onCreateBan = () => {
    setMod(Mod.INSERT);
    setCurrentRecord({});
    setReadOnlyEditForm(false);
    setVisibleEditDialog(true);
    setErrorMessage("");
  };

  const onEdit = () => {
    setMod(Mod.UPDATE);
    setReadOnlyEditForm(false);
  };

  const onClose = () => {
    setVisibleEditDialog(false);
  };

  const onCancel = () => {
    setVisibleEditDialog(false);
  };

  const onSave = async () => {
    const validatinResult = validateForm(currentRecord);
    if (validatinResult) {
      setErrorMessage(validatinResult);
      return;
    }

    let recordId = mod === Mod.INSERT ? EMPTY_ID : currentRecord?.id;

    const data: BanForUserRequest = {
      resourceTypeId: currentRecord?.resourceTypeId,
      employeeId: currentRecord?.employeeId,
      startDateTime: currentRecord?.startDateTime,
      endDateTime: currentRecord?.endDateTime,
    };

    if (mod === Mod.INSERT) {
      await apiClient.addBanForUser(data);
    } else {
      if (currentRecord) {
        try {
          await apiClient.editBan(recordId, data);
        } catch (err) {
          setErrorMessage("You cannot change a ban.");
          return;
        }
      }
    }
    setVisibleEditDialog(false);
    if (hubConnection) {
      hubConnection.invoke("Send", "The ban is updated");
    }
  };

  const onDelete = () => {
    let result = confirm("<h4>Are you sure?</h4>", "Deleting the record");
    result.then((dialogResult) => {
      if (dialogResult) {
        deleteBan();
        setVisibleEditDialog(false);
        if (hubConnection) {
          hubConnection.invoke("Send", "The ban is deleted");
        }
      }
    });
  };

  const onEditFormDataModified = (e: FieldDataChangedEvent) => {
    //if (e.dataField && e.dataField === "name") setFormName(e.value);

    if (e.dataField && currentRecord) {
      if (e.dataField === "resourceTypeId") {
        //loadBookPlacees(e.value, setBookingPlacees);
        currentRecord.resourceTypeId = e.value;
      }
      if (e.dataField === "employeeId") {
        currentRecord.employeeId = e.value;
      }
      if (e.dataField === "startDateTime") {
        currentRecord.startDateTime = e.value;
      }
      if (e.dataField === "endDateTime") {
        currentRecord.endDateTime = e.value;
      }
    }
  };

  const deleteBan = async () => {
    // await apiClient.clearBanForUser(currentBan?.id ?? '');
  };

  /*export*/ const validateForm = (
    currentRecord: ICurrentRecord | undefined
  ): string => {
    if (currentRecord) {
      if (currentRecord.employeeId === undefined)
        return "Please select employee";
      if (currentRecord.resourceTypeId === undefined)
        return "Please select resource type";
      if (currentRecord.startDateTime === undefined)
        return "Please select start date";
      // if (currentRecord.endDateTime === undefined)
      //   return "Please select end date";
    }
    return "";
  };

  const renderPicture = (data: any) => {
    const result = employees?.find(obj => {
      return obj.id === currentRecord?.employeeId;
    });
    const lastName = result?.name?.split(', ')[1];
    const images = [];
    images[0] = <img src={no_photo} />;
    images[1] = <img src={Rychkov_Andrey} />;
    images[2] = <img src={Alexeev_Alexey} />;
    images[3] = <img src={Molodtsov_Denis} />;
    images[4] = <img src={Banenko_Sergey} />;
    images[5] = <img src={Zhalnin_Aleksey} />;
    images[6] = <img src={Alvarez_Geovanny} />;
    images[7] = <img src={Guiet_Nathan} />;
    images[8] = <img src={Jegal_Sooyoun} />;
    images[9] = <img src={Rue_Adam} />;
    images[10] = <img src={Uchuno_James} />;
    let index = 0;

    if (lastName?.startsWith('R', 0)) { index = 1; }
    else if (lastName?.startsWith('A', 0)) { index = 2; }
    else if (lastName?.startsWith('M', 0)) { index = 3; }
    else if (lastName?.startsWith('B', 0)) { index = 4; }
    else if (lastName?.startsWith('Z', 0)) { index = 5; }
    else if (lastName?.startsWith('G', 0)) { index = 6; }
    else if (lastName?.startsWith('N', 0)) { index = 7; }
    else if (lastName?.startsWith('J', 0)) { index = 8; }
    else if (lastName?.startsWith('R', 0)) { index = 9; }
    else if (lastName?.startsWith('U', 0)) { index = 10; }
    else index = 5;

    if (mod === Mod.INSERT)
      index = 0;

    return <div className="form-avatar">{images[index]}</div>;
  }

  const [hubConnection, setHubConnection] = useState<any | null>(null);
  const webApiBansHubEnv: string = (process.env.REACT_APP_WEB_API_BANS_HUB as string);

  useEffect(() => {
    createHubConnection();
  }, []);

  const createHubConnection = async () => {
    const hubConnection = new HubConnectionBuilder()
      .withUrl(webApiBansHubEnv)
      .withAutomaticReconnect()
      .configureLogging(LogLevel.Information)
      .build();
    try {
      await hubConnection.start();
    } catch (e) {
      console.log("data", e);
    }
    setHubConnection(hubConnection);
  }


  useEffect(() => {
    if (hubConnection) {
      hubConnection.on("ReceiveMessage", (message: string) => {
        loadBans();
      });
    }
  }, [hubConnection]);

  let navigate = useNavigate();
  useEffect(() => {
    if (!getEmployeeInfo().isAuthorized)
      navigate("/");
  }, []);

  return (
    <div>
      <Popup
        visible={isVisibleEditDialog}
        dragEnabled={false}
        closeOnOutsideClick={true}
        showCloseButton={false}
        showTitle={true}
        title="Ban"
        container=".dx-viewport"
        width={950}
        height={450}
      >
        <div className="ban-modal-contrainer">
          <Form
            formData={currentRecord}
            onFieldDataChanged={onEditFormDataModified}
            readOnly={isReadOnlyEditForm}
          >
            <GroupItem cssClass="first-group" colCount={4}>
              <SimpleItem render={renderPicture}></SimpleItem>
              <GroupItem colSpan={3}>
                {/* <SimpleItem dataField='name' /> */}
                {/* <SimpleItem dataField='firstName' label={{ text: 'First name' }} /> */}

                <Item
                  label={{ text: "Employee" }}
                  dataField="employeeId"
                  editorType="dxSelectBox"
                  editorOptions={{
                    items: employees,
                    displayExpr: "name",
                    valueExpr: "id",
                    searchEnabled: true,
                    // value: {frmEmployee},
                  }}
                />
                <Item
                  label={{ text: "Resource type" }}
                  dataField="resourceTypeId"
                  editorType="dxSelectBox"
                  editorOptions={{
                    items: resourceTypes,
                    displayExpr: "name",
                    valueExpr: "id",
                    searchEnabled: true,
                  }}
                />
              </GroupItem>
            </GroupItem>
            <GroupItem cssClass="second-group" colCount={2}>
              <SimpleItem
                label={{ text: "Start date time" }}
                dataField="startDateTime"
                editorType="dxDateBox"
                editorOptions={{
                  width: "150px",
                  displayFormat: "dd.MM.yyyy HH:mm",
                  type: "datetime",
                }}
              />
              <SimpleItem
                label={{ text: "End date time" }}
                dataField="endDateTime"
                editorType="dxDateBox"
                editorOptions={{
                  width: "150px",
                  displayFormat: "dd.MM.yyyy HH:mm",
                  type: "datetime",
                }}
              />
            </GroupItem>
          </Form>

          <CommandPanel
            onEdit={onEdit}
            onClose={onClose}
            onCancel={onCancel}
            onSave={onSave}
            onDelete={onDelete}
            mod={mod}
            message={
              errorMessage ? { isVisible: true, text: errorMessage } : undefined
            }
          />
        </div>
      </Popup>

      <DataGrid
        dataSource={bans}
        keyExpr="id"
        showColumnLines={true}
        showRowLines={true}
        rowAlternationEnabled={true}
        showBorders={true}
        id="dgBans"
        columns={[
          {
            caption: "Employee",
            minWidth: 150,
            cellTemplate: function (container, options) {
              let divContainer = document.createElement("div");
              divContainer.className = "bans-iconed-link-container";
              let img = document.createElement("img");
              img.width = 16;
              img.height = 16;
              img.className = "bans-iconed-link-icon";
              img.title = getUseName(options);
              img.src = "images/statuses/default/active.png";
              let divName = document.createElement("div");
              divName.className = "bans-iconed-link-cell";

              //divName.textContent = options?.data?.employee?.fullName ?? "";
              divName.textContent = getUseName(options);

              divName.onclick = function () {
                rowClick(options?.data?.id ?? "");
              };
              divContainer.append(img);
              divContainer.append(divName);
              container.append(divContainer);
            },
          },
          {
            dataField: "resourceType.name",
            caption: "Resource type",
            alignment: "left",
            minWidth: 150,
          },
          {
            dataField: "startDateTime",
            caption: "Start date",
            dataType: "datetime",
            format: "dd.MM.yyyy HH:mm",
            alignment: "left",
            width: 150,
          },
          {
            dataField: "endDateTime",
            caption: "End date",
            dataType: "datetime",
            format: "dd.MM.yyyy HH:mm",
            alignment: "left",
            width: 150,
          },
        ]}
      ></DataGrid>

      <Button
        className="cp-btn"
        key="btn-Close"
        text="Add new"
        type="default"
        stylingMode="contained"
        onClick={onCreateBan}
        width={100}
      />
    </div>
  );
};

export default Bans;
