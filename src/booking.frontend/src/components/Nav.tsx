import { Button } from 'devextreme-react';
import React from 'react';
import { Link } from 'react-router-dom';
import { getEmployeeInfo } from '../api/utilites';
import { signoutRedirect, signinRedirect } from './Auth/user-service';
import { getUserName } from './Auth/user-service';

const Nav = (props: { name: string; setName: (name: string) => void }) => {

  if (getEmployeeInfo().isAuthorized) {
    return (
      <nav className='navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3'>
        <div className='container'>
          {/* <div className="navbar-brand">Booking react</div> */}
          <div>
            <Button
              onClick={() => signoutRedirect()}
              text='Logout'
              type='default'
              stylingMode='contained'
            />
          </div>
        </div>
      </nav>
    );
  } else {
    return (
      <nav className='navbar navbar-expand-sm navbar-toggleable-sm navbar-light bg-white border-bottom box-shadow mb-3'>
        <div className='container'>
          {/* <div className="navbar-brand">Booking react</div> */}
          <div>
            <Button
              onClick={() => signinRedirect()}
              text='Login'
              type='default'
              stylingMode='contained'
            />
          </div>
        </div>
      </nav>
    );
  }
};

export default Nav;
