import { FC, ReactElement, useEffect, useLayoutEffect, useState } from 'react';
import SigninOidc from '../Auth/SigninOidc';
import SignoutOidc from '../Auth/SignoutOidc';
import { Route, Routes, useLocation } from 'react-router-dom';
import userManager from '../Auth/user-service';
import AuthProvider from '../Auth/auth-provider';
import Toolbar, { Item } from 'devextreme-react/toolbar';
import { Button, Drawer } from 'devextreme-react';
import NavigationList from '../NavigationList/NavigationList';
import Employees from '../Pages/Employees/Employees';
import './App.css';
import Departments from '../Pages/Departments/Departments';
import Reservations from '../Pages/Reservations/Reservations';
import Home from '../Pages/Home/Home';
import Bans from '../Pages/Bans/Bans';

const App: FC<{}> = (): ReactElement => {
  const [opened, setOpened] = useState<boolean>(false);
  const location = useLocation();
  const isVisibleTemplateElements = location.pathname !== '/';

  const onOutsideClick = ({ value }: any) => {
    return false;
  };

  let navigationList: any;
  if (isVisibleTemplateElements)
    navigationList = NavigationList;

    return (
      <>
        <Toolbar className='app-toolbar-container' visible={isVisibleTemplateElements} height={30}>
          <Item location={'before'}>
            <Button
              icon='menu'
              type='normal'
              stylingMode='text'
              onClick={() => setOpened(!opened)}
              className='app-hamburger-menu-button'
            />
          </Item>
        </Toolbar>
        <Drawer
          component={navigationList}
          opened={opened}
          openedStateMode='shrink'
          position='left'
          revealMode='slide'
          closeOnOutsideClick={onOutsideClick}
        >
          <div id='content' className='dx-theme-background-color'>
            <AuthProvider userManager={userManager}>
              <Routes>
                <Route path='*' element={<Employees />} />
                <Route path='depatrments' element={<Departments />} />
                <Route path='employees' element={<Employees />} />
                <Route path='reservations' element={<Reservations />} />
                <Route path='/' element={<Home />} />
                <Route path='bans' element={<Bans />} />
                <Route path='signout-oidc' element={<SignoutOidc />} />
                <Route path='signin-oidc' element={<SigninOidc />} />
              </Routes>
            </AuthProvider>
          </div>
        </Drawer>
        {<div className='app-footer'>© BOOKING 2022</div>}
      </>
    );
};
export default App;