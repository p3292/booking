export interface IMenuItem {
  id: number;
  text: string;
  icon?: string;
  to?: string;
  allowToUser?: boolean;
}

export const getMenuItems = (isAdmin: boolean) => {
  if(isAdmin)
    return menuItems;
  else
    return menuItems.filter((item) => item.allowToUser);
}

export const menuItems: IMenuItem[]  = [
  { id: 1, text: 'Reservations', icon: 'download', to: 'reservations', allowToUser: true },
  { id: 2, text: 'Departments', icon: 'card', to: 'depatrments', allowToUser: false },
  { id: 3, text: 'Employees', icon: 'group', to: 'employees', allowToUser: false },
  { id: 4, text: 'Bans', icon: 'remove', to: 'bans', allowToUser: false },
];