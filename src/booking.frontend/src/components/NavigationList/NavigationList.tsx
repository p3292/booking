import React, { useEffect, useState } from 'react';
import './NavigationList.css';
import { List } from 'devextreme-react';
import { useNavigate } from 'react-router-dom';
import { IMenuItem, getMenuItems } from './data';
import { ItemClickEvent } from 'devextreme/ui/list';
import Nav from '../Nav';
import { loadUser, getUserName } from '../Auth/user-service';
import { getEmployeeInfo } from '../../api/utilites';
loadUser();


const NavigationList: React.FC = () => {
  const [nameNav, setNameNav] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    (async () => {
      const user = await getUserName();
      setNameNav(user ?? '');
    })();
    stylingMenuItems();
  },[]);

  const stylingMenuItems = () => {
    const menuItems = Array.from(
      document.querySelectorAll('.dx-item-content.dx-list-item-content')
    );
    for (let item of menuItems) item.classList.add('menu-item-general');
  };

  const menuItemClick = (e: ItemClickEvent) => {
    document
      .querySelector('.menu-item-selected')
      ?.classList.remove('menu-item-selected');
    e.itemElement.classList.add('menu-item-selected');

    const item = e.itemData as IMenuItem;
    if (item.to && item.to !== '') {
      navigate(item.to);
    }
  };

  return (
    <div className='menu-container'>
      <div>
        <List
          items={getMenuItems(getEmployeeInfo().isAdmin)}
          hoverStateEnabled={false}
          activeStateEnabled={false}
          focusStateEnabled={false}
          onItemClick={menuItemClick}
          className='panel-list dx-theme-accent-as-background-color'
        />
      </div>
      <div className='menu-container-bottom'>
        <div className='menu-nav-container'>
          <Nav name={nameNav} setName={setNameNav} />
        </div>
      </div>
    </div>
  );
};

export default NavigationList;