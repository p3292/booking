import { Button } from 'devextreme-react';
import React, { useEffect, useState, ReactElement } from 'react';
import './CommandPanel.css';

export enum Mod {
  VIEW = 'VIEW',
  UPDATE = 'UPDATE',
  INSERT = 'INSERT',
  CUSTOM = 'CUSTOM',
}

export interface ICommandPanelMessage {
  text: string;
  isVisible: boolean;
}

export interface ICommandPanelShowButtons {
  isShowClose: boolean;
  isShowEdit: boolean;
  isShowSave: boolean;
  isShowDelete: boolean;
  isShowCancel: boolean;
  isShowCustom: boolean;
}

interface CommandPanelProps {
  onClose?: () => void;
  onEdit?: () => void;
  onSave?: () => void;
  onDelete?: () => void;
  onCancel?: () => void;
  mod: Mod;
  message?: ICommandPanelMessage;
  customButtons?: ReactElement | string;
  customShowingButtons?: ICommandPanelShowButtons;
}

const initButtons = (
  mod: Mod,
  customShowingButtons: ICommandPanelShowButtons | null,
  setVisibleClose: (v: boolean) => void,
  setVisibleEdit: (v: boolean) => void,
  setVisibleDelete: (v: boolean) => void,
  setVisibleSave: (v: boolean) => void,
  setVisibleCancel: (v: boolean) => void,
  setVisibleCustom: (v: boolean) => void
): void => {
  switch (mod) {
    case Mod.CUSTOM:
      if (customShowingButtons) {
        setVisibleClose(customShowingButtons.isShowClose);
        setVisibleEdit(customShowingButtons.isShowEdit);
        setVisibleDelete(customShowingButtons.isShowDelete);
        setVisibleSave(customShowingButtons.isShowSave);
        setVisibleCancel(customShowingButtons.isShowCancel);
        setVisibleCustom(customShowingButtons.isShowCustom);
      }
      break;
    case Mod.VIEW:
      setVisibleClose(true);
      setVisibleEdit(true);
      setVisibleDelete(true);
      setVisibleSave(false);
      setVisibleCancel(false);
      setVisibleCustom(false);
      break;
    case Mod.INSERT:
      setVisibleClose(false);
      setVisibleEdit(false);
      setVisibleDelete(false);
      setVisibleSave(true);
      setVisibleCancel(true);
      setVisibleCustom(false);
      break;
    case Mod.UPDATE:
      setVisibleClose(false);
      setVisibleEdit(false);
      setVisibleDelete(false);
      setVisibleSave(true);
      setVisibleCancel(true);
      setVisibleCustom(false);
      break;
    default:
      break;
  }
};

const initMessagePanel = (message: ICommandPanelMessage) => {
  if (message.isVisible) {
    return (
      <div className='cp-error-message'>
        {message.text}
      </div>
    );
  } else return null;
};

const CommandPanel: React.FC<CommandPanelProps> = (props) => {
  const {
    mod = Mod.VIEW,
    message = { isVisible: false, text: '' },
    customButtons = null,
    customShowingButtons = null,
  } = props;
  const [isVisibleClose, setVisibleClose] = useState<boolean>(false);
  const [isVisibleEdit, setVisibleEdit] = useState<boolean>(false);
  const [isVisibleSave, setVisibleSave] = useState<boolean>(false);
  const [isVisibleDelete, setVisibleDelete] = useState<boolean>(false);
  const [isVisibleCancel, setVisibleCancel] = useState<boolean>(false);
  const [isVisibleCustom, setVisibleCustom] = useState<boolean>(false);
  const [lastMod, setLastMod] = useState<Mod>();

  useEffect(() => {
    initButtons(
      mod,
      customShowingButtons,
      setVisibleClose,
      setVisibleEdit,
      setVisibleDelete,
      setVisibleSave,
      setVisibleCancel,
      setVisibleCustom
    );
  }, [customShowingButtons]);

  if (lastMod !== mod) {
    initButtons(
      mod,
      customShowingButtons,
      setVisibleClose,
      setVisibleEdit,
      setVisibleDelete,
      setVisibleSave,
      setVisibleCancel,
      setVisibleCustom
    );
    setLastMod(mod);
  }

  const messagePanel = initMessagePanel(message);

  let buttons: any[] = [];
  if (customButtons && isVisibleCustom)
    buttons.push(<div key='btn-customs'>{customButtons}</div>);
  if (isVisibleDelete)
    buttons.push(
      <Button
        className='cp-btn'
        key='btn-Delete'
        text='Delete'
        type='danger'
        stylingMode='contained'
        onClick={props.onDelete}
      />
    );
  if (isVisibleCancel)
    buttons.push(
      <Button
        className='cp-btn'
        key='btn-Cancel'
        text='Cancel'
        type='default'
        stylingMode='contained'
        onClick={props.onCancel}
      />
    );
  if (isVisibleEdit)
    buttons.push(
      <Button
        className='cp-btn'
        key='btn-Edit'
        text='Edit'
        type='default'
        stylingMode='contained'
        onClick={props.onEdit}
      />
    );
  if (isVisibleSave)
    buttons.push(
      <Button
        className='cp-btn'
        key='btn-Save'
        text='Save'
        type='default'
        stylingMode='contained'
        onClick={props.onSave}
      />
    );
  if (isVisibleClose)
    buttons.push(
      <Button
        className='cp-btn'
        key='btn-Close'
        text='Close'
        type='default'
        stylingMode='contained'
        onClick={props.onClose}
      />
    );

  return (
    <div className='cp-container'>
      {messagePanel}
      <div className='cp-butons-container'>{buttons}</div>
    </div>
  );
};

export default CommandPanel;
