export interface IEmployeeInfo {
    isAdmin: boolean;
    employeeId: number;
    employeeName: string;
    isAuthorized: boolean;
}

export const getEmployeeInfo = (): IEmployeeInfo => {
    const item: string = process.env.REACT_APP_OIDC_SESSION ?? "";
    const claims = JSON.parse(sessionStorage.getItem(item)|| '{}'); 
    return {isAuthorized : (Object.keys(claims).length !== 0), employeeId : 1, isAdmin : (claims?.profile?.is_admin === "True"), employeeName : (claims?.profile?.user_name) };
}

export const isNeedRedirectToHome = (route: string): boolean => {
    return (!getEmployeeInfo().isAuthorized)
        && route.toLowerCase() !== '/home';
}