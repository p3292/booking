﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
	public class Employee : Entity
    {
        //[Required]
        public Guid DepartmentId { get; set; }
        
        public virtual Department Department { get; set; }

        [Required]
        public Guid RoleId { get; set; }
        
        public virtual Role Role { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        
        public string MiddleName { get; set; }

        [Required]
        public string Login { get; set; }

        [Required]
        public string Email { get; set; }
        
        public string FullName => MiddleName is null ? $"{LastName}, {FirstName}" : $"{LastName}, {FirstName} {MiddleName}";
        
        public bool IsAdmin { get; set; }
        
        public bool CanUseSystem { get; set; }

    }
}
