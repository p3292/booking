﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{

    public class Ban : Entity
    {
        [Required]
        public Guid ResourceTypeId { get; set; }
        public virtual ResourceType ResourceType { get; set; }
        [Required]
        public Guid EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }

        public DateTime StartDateTime { get; set; }
        public DateTime ? EndDateTime { get; set; }

    }
}
