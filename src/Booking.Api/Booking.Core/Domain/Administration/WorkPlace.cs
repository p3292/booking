﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Core.Domain.Administration
{
	public class WorkPlace : BookPlace
	{
		[Required]
		public Guid DepartmentId { get; set; }		
		public virtual Department Department { get; set; }

		[Required]
		public string Building { get; set; }

		[Required]
		public int Floor { get; set; }
	}
}
