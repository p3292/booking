﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
	public class Department : Entity
	{
		[Required]
		public string Name { get; set; }
	}
}
