﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
    public class Role: Entity
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
