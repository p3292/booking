﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
	public class ResourceType : Entity
	{
		[Required]
		public string Name { get; set; }
		
		public bool IsSelectable { get; set; }
	}
}
