﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Booking.Core.Domain.Administration
{
	public abstract class BookPlace : Entity
	{
		public Guid? OwnerId { get; set; }
		public virtual Employee? Owner { get; set; }

		[Required]
		public string Number{ get; set; }
		
		public DateTime LockTimeStart { get; set; }
        
		public int Duration { get; set; }                       // Minutes
		
		[Required]
		public Guid ResourceTypeId { get; set; }
		public virtual ResourceType ResourceType { get; set; }
	}
}