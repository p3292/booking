﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
	public class ParkingPlace : BookPlace
	{
        [Required]
        public Guid? ParkingId { get; set; }        
        public virtual Parking Parking { get; set; }
    }
}