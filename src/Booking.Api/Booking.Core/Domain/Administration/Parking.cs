﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.Administration
{
    public class Parking: Entity
    {
        [Required]
        public string Name { get; set; }
    }
}
