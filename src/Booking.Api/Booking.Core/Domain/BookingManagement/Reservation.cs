﻿using Booking.Core.Domain.Administration;
using System;
using System.ComponentModel.DataAnnotations;

namespace Booking.Core.Domain.BookingManagement
{
	public class Reservation : Entity
	{
		[Required]
		public Guid BookPlaceId { get; set; }
		
		public virtual BookPlace BookPlace { get; set; }

		//[Required]
		//public Guid ResourceTypeId { get; set; }
		
		//public virtual ResourceType ResourceType { get; set; }
		
		[Required]
		public Guid EmployeeId { get; set; }
		
		public virtual Employee Employee { get; set; }

		[Required]
		public DateTime StartDateTime { get; set; }
		
		[Required]
		
		public DateTime EndDateTime { get; set; }
		
		public DateTime EndDateTimeFact { get; set; }
		
		public bool BookingType { get; set; }
		
		public bool IsCancelled { get; set; }

	}
}
