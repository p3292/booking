﻿using System;

namespace Booking.Core.Domain
{
	public abstract class Entity
	{
		public Guid Id { get; set; }

	}
}