﻿using System;

namespace Booking.Core
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}
