﻿using System;

namespace Booking.Core
{
    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
