﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Publishers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Booking.UnitTests
{
    public class EmployeesControllerData_InMemory
    {
        public EmployeesController employeesController;
        public IRepository<Employee> employeeRepository;
        public IRepository<Role> roleRepository;
        public IRepository<Department> departmentRepository;
        private DataContext _initContext;
        private readonly IUserPublisher _userPublisher;

        public EmployeesControllerData_InMemory(string mode, ILogger<EmployeesController> logger, IUserPublisher userPublisher)
        {
            _userPublisher = userPublisher;
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();

            if(string.Equals(mode,"createEmployee"))
            {
                roleRepository = GetInMemoryRoleRepository();
                departmentRepository = GetInMemoryDepartmentRepository();
                employeeRepository = new EfRepository<Employee>(_initContext);
                employeesController = new EmployeesController(employeeRepository, roleRepository, departmentRepository, logger, _userPublisher);
            }
            else if(string.Equals(mode,"getEmployee"))
            {
                roleRepository = GetInMemoryRoleRepository();
                departmentRepository = GetInMemoryDepartmentRepository();
                employeeRepository = GetInMemoryRepository();
                employeesController = new EmployeesController(employeeRepository, roleRepository, departmentRepository, logger, _userPublisher);
            }
        }

        private IRepository<Employee> GetInMemoryRepository()
        {
            Seed(_initContext);
            var repository = new EfRepository<Employee>(_initContext);
            return repository;
        }

        private IRepository<Role> GetInMemoryRoleRepository()
        {
            SeedRole(_initContext);
            var repository = new EfRepository<Role>(_initContext);
            return repository;
        }

        private IRepository<Department> GetInMemoryDepartmentRepository()
        {
            SeedDepartment(_initContext);
            var repository = new EfRepository<Department>(_initContext);
            return repository;
        }


        public void Seed(DataContext context)
        {
            Employee employee = new EmployeeBuilder()
                .WithId(Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"))
                .WithFirstName("San_inmemory")
                .WithLastName("Jose_inmemory")
                .WithMiddleName("Texas_inmemory")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRoleId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithDepartmentId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .Build();

            context.Add(employee);
            context.SaveChanges();
        }

        public void SeedRole(DataContext context)
        {
            Role role = new RoleBuilder()
                .WithId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build();

            context.Add(role);
            context.SaveChanges();
        }

        public void SeedDepartment(DataContext context)
        {
            Department department = new DepartmentBuilder()
                .WithId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .WithName("System Development and etc.")
                .Build();

            context.Add(department);
            context.SaveChanges();
        }
    }
}
