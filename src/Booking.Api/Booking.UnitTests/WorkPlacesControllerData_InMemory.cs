﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Booking.UnitTests
{
    public class WorkPlacesControllerData_InMemory
    {
        public WorkPlacesController workPlacesController;
        public IRepository<WorkPlace> workPlacesRepository;
        private DataContext _initContext;

        public WorkPlacesControllerData_InMemory(string mode, ILogger<WorkPlacesController> logger)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();

            if(string.Equals(mode, "createWorkPlace"))
            {
                workPlacesRepository = new EfRepository<WorkPlace>(_initContext);
                workPlacesController = new WorkPlacesController(workPlacesRepository, logger);
            }
            else if (string.Equals(mode, "getWorkPlace"))
            {
                workPlacesRepository = GetInMemoryRepository();
                workPlacesController = new WorkPlacesController(workPlacesRepository, logger);
            }
        }

        private IRepository<WorkPlace> GetInMemoryRepository()
        {
            Seed(_initContext);
            var repository = new EfRepository<WorkPlace>(_initContext);
            return repository;
        }

        public void Seed(DataContext context)
        {
            WorkPlace workPlace = new WorkPlaceBuilder()
                .WithId(Guid.Parse("53C7EFD7-9930-4FBB-B400-67AFD0DAC91D"))
                .WithNumber("12345")
                .WithLockTimeStart(DateTime.Now.AddHours(2))
                .WithDuration(123)
                .WithResourceTypeId(Guid.Parse("124EE48A-52B8-4016-8807-532155C076EE"))
                //.WithParkingId(Guid.Parse("129E25E9-AEA8-4F59-9D0D-58C5AFA10C13"))
                .WithOwnerId(Guid.Parse("B1936881-E9B2-47E8-B2E0-75D355C81E75"))
                .Build();

            context.Add(workPlace);
            context.SaveChanges();
        }
    }
}
