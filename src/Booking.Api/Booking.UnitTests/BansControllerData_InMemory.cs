﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using Booking.WebHost.Publishers;

namespace Booking.UnitTests
{
    public class BansControllerData_InMemory
    {
        public BansController bansController;
        public IRepository<Ban> bansRepository;
        public IRepository<Employee> employeeRepository;
        public IRepository<Role> roleRepository;
        public IRepository<Department> departmentRepository;
        public IRepository<ResourceType> resourceTypeRepository;
        private DataContext _initContext;

        public BansControllerData_InMemory(ILogger<BansController> logger)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockBansDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();
           

            //var fakeFactory = DataAccess.Data.FakeDataFactory.Generate();

            //_initContext.Add(fakeFactory.Departments);
            //_initContext.Add(fakeFactory.Roles);
            //_initContext.Add(fakeFactory.ResourceTypes);
            //_initContext.Add(fakeFactory.Bans);
            //_initContext.Add(fakeFactory.Departments);
            //_initContext.SaveChanges();

            var bansRepository = new EfRepository<Ban>(_initContext);
            var employeeRepository = new EfRepository<Employee>(_initContext);
            var resourceTypeRepository = new EfRepository<ResourceType>(_initContext);
            
            var currentDateTime = new Core.CurrentDateTimeProvider();

            EmailPublisher ep = new EmailPublisher(null);
            Booking.WebHost.Services.EmailGeneratorService gs = new Booking.WebHost.Services.EmailGeneratorService(ep);

            bansController = new BansController(logger, bansRepository, employeeRepository, resourceTypeRepository, currentDateTime, gs);
        }   
    }
}
