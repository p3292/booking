﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Booking.UnitTests
{
    public class RolesControllerData_InMemory
    {
        public RolesController rolesController;
        public IRepository<Role> roleRepository;
        private DataContext _initContext;

        public RolesControllerData_InMemory(ILogger<RolesController> logger)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockRolesDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();

            roleRepository = GetInMemoryRoleRepository();
            rolesController = new RolesController(roleRepository, logger);
        }

        private IRepository<Role> GetInMemoryRoleRepository()
        {
            SeedRole(_initContext);
            var repository = new EfRepository<Role>(_initContext);
            return repository;
        }

        public void SeedRole(DataContext context)
        {
            Role role = new RoleBuilder()
                .WithId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build();

            context.Add(role);
            context.SaveChanges();
        }
    }
}
