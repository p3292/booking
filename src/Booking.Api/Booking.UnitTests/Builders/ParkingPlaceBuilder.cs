﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using Booking.WebHost.Models.Request;
using Booking.WebHost.Models.Response;
using System;

namespace Booking.UnitTests.Builders
{
    public class ParkingPlaceBuilder
    {
        private Guid _id;
        
        private Guid _ownerId { get; set; }
        private Employee _owner;

        private string _number;
        private DateTime _lockTimeStart;
        private int _duration;                       // Minutes

        private Guid _resourceTypeId { get; set; }
        private ResourceType _resourceType;

        private Guid _parkingId { get; set; }
        private Parking _parking;

        public ParkingPlaceBuilder()
        {
            // ...
        }

        public ParkingPlaceBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public ParkingPlaceBuilder WithNumber(string number)
        {
            _number = number;
            return this;
        }

        public ParkingPlaceBuilder WithLockTimeStart(DateTime lockTimeStart)
        {
            _lockTimeStart = lockTimeStart;
            return this;
        }

        public ParkingPlaceBuilder WithDuration(int duration)
        {
            _duration = duration;
            return this;
        }

        public ParkingPlaceBuilder WithResourceTypeId(Guid resourceTypeId)
        {
            _resourceTypeId = resourceTypeId;
            return this;
        }

        public ParkingPlaceBuilder WithResourceType(ResourceType resourceType)
        {
            _resourceType = new ResourceTypeBuilder()
                .WithId(resourceType.Id)
                .WithName(resourceType.Name)
                .WithIsSelectable(resourceType.IsSelectable)
                .Build();

            return this;
        }

        public ParkingPlaceBuilder WithParkingId(Guid parkingId)
        {
            _parkingId = parkingId;
            return this;
        }

        public ParkingPlaceBuilder WithParking(Parking parking)
        {
            _parking = new ParkingBuilder()
                .WithId(parking.Id)
                .WithName(parking.Name)
                .Build();

            return this;
        }

        public ParkingPlaceBuilder WithOwnerId(Guid ownerId)
        {
            _ownerId = ownerId;
            return this;
        }

        public ParkingPlaceBuilder WithOwner(Employee owner)
        {
            _owner = new EmployeeBuilder()
                .WithId(owner.Id)
                .WithFirstName(owner.FirstName)
                .WithLastName(owner.LastName)
                .WithMiddleName(owner.MiddleName)
                .WithIsAdmin(owner.IsAdmin)
                .WithCanUseSystem(owner.CanUseSystem)
                .WithRole(owner.Role)
                .WithDepartment(owner.Department)
                .Build();

            return this;
        }

        public ParkingPlace Build()
        {
            return new ParkingPlace
            {
                Id = _id,
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,
                ResourceType = _resourceType,
                ParkingId = _parkingId,
                Parking = _parking,
                OwnerId = _ownerId,
                Owner = _owner
            };
        }

        public ParkingPlaceResponse BuildParkingPlaceResponse()
        {
            return new ParkingPlaceResponse
            {
                Id = _id,
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,
                ResourceType = _resourceType,
                Parking = new ParkingResponse(_parking),
                Owner = new EmployeeResponse(_owner)
            };
        }
        
        public CreateOrEditParkingPlaceRequest BuildCreateOrEditParkingPlaceRequest()
        {
            return new CreateOrEditParkingPlaceRequest
            {
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,
                ParkingId = _parkingId,
                OwnerId = _ownerId
            };
        }
    }
}
