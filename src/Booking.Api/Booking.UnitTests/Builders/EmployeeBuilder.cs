﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using Booking.WebHost.Models.Response;
using System;

namespace Booking.UnitTests.Builders
{
    public class EmployeeBuilder
    {
        private Guid _id;

        private string _firstName;
        private string _lastName;
        private string _middleName;
        private string _login;

        private bool _isAdmin;
        private bool _canUseSystem;

        private Guid _roleId { get; set; }
        private Role _role;

        public Guid _departmentId { get; set; }
        private Department _department;

        public EmployeeBuilder()
        {
            // some Employee class by default
        }


        public EmployeeBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public EmployeeBuilder WithFirstName(string firstName)
        {
            _firstName = firstName;
            return this;
        }

        public EmployeeBuilder WithLastName(string lastName)
        {
            _lastName = lastName;
            return this;
        }

        public EmployeeBuilder WithMiddleName(string middleName)
        {
            _middleName = middleName;
            return this;
        }

        public EmployeeBuilder WithLogin(string login)
        {
            _login = login;
            return this;
        }

        public EmployeeBuilder WithCanUseSystem(bool canUseSystem)
        {
            _canUseSystem = canUseSystem;
            return this;
        }

        public EmployeeBuilder WithIsAdmin(bool isAdmin)
        {
            _isAdmin = isAdmin;
            return this;
        }

        public EmployeeBuilder WithRoleId(Guid roleId)
        {
            _roleId = roleId;
            return this;
        }

        public EmployeeBuilder WithRole(Role role)
        {
            _role = new RoleBuilder()
                .WithId(role.Id)
                .WithName(role.Name)
                .WithDescription(role.Description)
                .Build();


            return this;
        }

        public EmployeeBuilder WithDepartmentId(Guid departmentId)
        {
            _departmentId = departmentId;
            return this;
        }

        public EmployeeBuilder WithDepartment(Department department)
        {
            _department = new DepartmentBuilder()
                .WithId(department.Id)
                .WithName(department.Name)
                .Build();

            return this;
        }

        public Employee Build()
        {
            return new Employee
            {
                Id = _id,
                FirstName = _firstName,
                LastName = _lastName,
                MiddleName = _middleName,
                IsAdmin = _isAdmin,
                CanUseSystem = _canUseSystem,
                RoleId = _roleId,
                Role = _role,
                DepartmentId = _departmentId,
                Department = _department,
                Login = _login
            };
        }

        public EmployeeResponse BuildEmployeeResponse()
        {
            return new EmployeeResponse
            {
                Id = _id,
                FirstName = _firstName,
                LastName = _lastName,
                IsAdmin = _isAdmin,
                CanUseSystem = _canUseSystem,
                Role = new RoleResponse(_role),
                Department = new DepartmentResponse(_department)
            };
        }
        
        public CreateOrEditEmployeeRequest BuildCreateOrEditEmployeeRequest()
        {
            return new CreateOrEditEmployeeRequest
            {
                FirstName = _firstName,
                LastName = _lastName,
                MiddleName = _middleName,
                IsAdmin = _isAdmin,
                CanUseSystem = _canUseSystem,
                RoleId = _roleId,
                DepartmentId = _departmentId
            };
        }
    }
}
