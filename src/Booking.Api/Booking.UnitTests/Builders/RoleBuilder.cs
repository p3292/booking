﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using System;

namespace Booking.UnitTests.Builders
{
    public class RoleBuilder
    {
        private Guid _id;

        private string _name;
        private string _description;

        public RoleBuilder()
        {
            // some Role class by default
        }

        public RoleBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }
        
        public RoleBuilder WithName(string name)
        {
            _name = name;
            return this;
        }
        
        public RoleBuilder WithDescription(string description)
        {
            _description = description;
            return this;
        }

        public Role Build()
        {
            return new Role
            {
                Id = _id,
                Name = _name,
                Description = _description
            };
        }

        public CreateOrEditRoleRequest BuildCreateOrEditRoleRequest()
        {
            return new CreateOrEditRoleRequest
            {
                Name = _name,
                Description = _description
            };
        }
    }
}
