﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using System;

namespace Booking.UnitTests.Builders
{
    public class ResourceTypeBuilder
    {
        private Guid _id;

        private string _name;
        private bool _isSelectable;

        public ResourceTypeBuilder()
        {
            // some ResourceType class by default
        }

        public ResourceTypeBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public ResourceTypeBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public ResourceTypeBuilder WithIsSelectable(bool isSelectable)
        {
            _isSelectable = isSelectable;
            return this;
        }

        public ResourceType Build()
        {
            return new ResourceType
            {
                Id = _id,
                Name = _name,
                IsSelectable = _isSelectable
            };
        }

        public CreateOrEditResourceTypeRequest BuildCreateOrEditResourceTypeRequest()
        {
            return new CreateOrEditResourceTypeRequest
            {
                Name = _name,
                IsSelectable = _isSelectable
            };
        }
    }
}
