﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using Booking.WebHost.Models.Request;
using System;

namespace Booking.UnitTests.Builders
{
    class BanBuilder
    {
        private Guid _id;
        private Guid _employeeId;
        
        private Guid _resourceTypeId;
        private DateTime ? _endDateTime;
        private DateTime _startDateTime;

        public BanBuilder()
        {
            // some Ban class by default
        }

        public BanBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public BanBuilder WithEmployeeId(Guid employeeId)
        {
            _employeeId = employeeId;
            return this;
        }
        public BanBuilder WithResourceTypeId(Guid resourceTypeId)
        {
            _resourceTypeId = resourceTypeId;
            return this;
        }

        public BanBuilder WithStartDateTime(DateTime startDateTime)
        {
            _startDateTime = startDateTime;
            return this;
        }
        public BanBuilder WithEndDateTime(DateTime ? endDateTime)
        {
            _endDateTime = endDateTime;
            return this;
        }
        public Ban Build()
        {
            return new Ban
            {
                Id = _id,
                EmployeeId = _employeeId,
                ResourceTypeId = _resourceTypeId,
                EndDateTime = _endDateTime,
                StartDateTime = _startDateTime,
                
            };
        }


        public BanForUserRequest BuildCreateBanRequest()
        {
            return new BanForUserRequest
            {
              EmployeeId = _employeeId,
              ResourceTypeId= _resourceTypeId
            };
        }
    }
}
