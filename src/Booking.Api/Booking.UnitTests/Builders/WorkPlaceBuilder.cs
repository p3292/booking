﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using Booking.WebHost.Models.Request;
using Booking.WebHost.Models.Response;
using System;

namespace Booking.UnitTests.Builders
{
    public class WorkPlaceBuilder
    {
        private Guid _id;

        private Guid _ownerId { get; set; }
        private Employee _owner;

        private string _number;
        private DateTime _lockTimeStart;
        private int _duration;                       // Minutes

        private Guid _resourceTypeId { get; set; }
        private ResourceType _resourceType;

        private Guid _departmentId { get; set; }
        private Department _department;

        public string _building;
        private int _floor;

        public WorkPlaceBuilder()
        {
            // ...
        }

        public WorkPlaceBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public WorkPlaceBuilder WithNumber(string number)
        {
            _number = number;
            return this;
        }

        public WorkPlaceBuilder WithLockTimeStart(DateTime lockTimeStart)
        {
            _lockTimeStart = lockTimeStart;
            return this;
        }

        public WorkPlaceBuilder WithDuration(int duration)
        {
            _duration = duration;
            return this;
        }

        public WorkPlaceBuilder WithResourceTypeId(Guid resourceTypeId)
        {
            _resourceTypeId = resourceTypeId;
            return this;
        }

        public WorkPlaceBuilder WithResourceType(ResourceType resourceType)
        {
            _resourceType = new ResourceTypeBuilder()
                .WithId(resourceType.Id)
                .WithName(resourceType.Name)
                .WithIsSelectable(resourceType.IsSelectable)
                .Build();

            return this;
        }

        public WorkPlaceBuilder WithDepartmentId(Guid departmentId)
        {
            _departmentId = departmentId;
            return this;
        }

        public WorkPlaceBuilder WithDepartment(Department department)
        {
            _department = new DepartmentBuilder()
                .WithId(department.Id)
                .WithName("System Development and etc.")
                .Build();

            return this;
        }

        public WorkPlaceBuilder WithOwnerId(Guid ownerId)
        {
            _ownerId = ownerId;
            return this;
        }

        public WorkPlaceBuilder WithOwner(Employee owner)
        {
            _owner = new EmployeeBuilder()
                .WithId(owner.Id)
                .WithFirstName(owner.FirstName)
                .WithLastName(owner.LastName)
                .WithMiddleName(owner.MiddleName)
                .WithIsAdmin(owner.IsAdmin)
                .WithCanUseSystem(owner.CanUseSystem)
                .WithRole(owner.Role)
                .WithDepartment(owner.Department)
                .Build();

            return this;
        }

        public WorkPlace Build()
        {
            return new WorkPlace
            {
                Id = _id,
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,
                ResourceType = _resourceType,                
                OwnerId = _ownerId,
                Owner = _owner,
                DepartmentId= _departmentId,
                Department = _department,
                Building = _building,
                Floor = _floor
            };
        }

        public WorkPlaceResponse BuildWorkPlaceResponse()
        {
            return new WorkPlaceResponse
            {
                Id = _id,
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,
                ResourceType = _resourceType,                
                Owner = new EmployeeResponse(_owner),
                Department = new DepartmentResponse(_department),
                Building = _building,
                Floor = _floor                
            };
        }

        public CreateOrEditWorkPlaceRequest BuildCreateOrEditWorkPlaceRequest()
        {
            return new CreateOrEditWorkPlaceRequest
            {
                Number = _number,
                LockTimeStart = _lockTimeStart,
                Duration = _duration,
                ResourceTypeId = _resourceTypeId,                
                OwnerId = _ownerId,
                DepartmentId= _departmentId,
                Building = _building,
                Floor = _floor
            };
        }
    }
}
