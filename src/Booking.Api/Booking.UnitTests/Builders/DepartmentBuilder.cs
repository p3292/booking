﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models.Request;
using System;

namespace Booking.UnitTests.Builders
{
    class DepartmentBuilder
    {
        private Guid _id;

        private string _name;

        public DepartmentBuilder()
        {
            // some Department class by default
        }

        public DepartmentBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public DepartmentBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public Department Build()
        {
            return new Department
            {
                Id = _id,
                Name = _name
            };
        }

        public CreateOrEditDepartmentRequest BuildCreateOrEditDepartmentRequest()
        {
            return new CreateOrEditDepartmentRequest
            {
                Name = _name
            };
        }
    }
}
