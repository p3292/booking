﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using System;

namespace Booking.UnitTests.Builders
{
    public class ParkingBuilder
    {
        private Guid _id;
        private string _name;

        public ParkingBuilder()
        {
            // ...
        }

        public ParkingBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public ParkingBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public Parking Build()
        {
            return new Parking
            {
                Id = _id,
                Name = _name
            };
        }

        public CreateOrEditParkingRequest BuildCreateOrEditParkingRequest()
        {
            return new CreateOrEditParkingRequest
            {
                Name = _name
            };
        }
    }
}
