﻿using Microsoft.AspNetCore.Hosting.Server;
using Booking.WebHost;
using Microsoft.Extensions.Hosting;
using Shouldly;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Booking.UnitTests
{
    public class ProgramTests
    {
        [Fact]
        public async Task Get_ClientResponse_ShouldBeOk()
        {
            // Arrange Привет
            using var host = Program.CreateHostBuilder()
                .ConfigureWebHost(webBuilder => webBuilder.UseTestServer())
                //.UseUrls("http://+:5000", "https://+:5001"))
                //.UseEnvironment("Test")
                .UseEnvironment("UnitTest")
                .Build();

            await host.StartAsync();

            await host.StartAsync();
            using var client = host.Services
                .GetRequiredService<IServer>()
                .ShouldBeOfType<TestServer>()
                .CreateClient();

            // Act
            var response = await client.GetAsync("index.html");
            await host.StopAsync();

            // Assert
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Get_Client_ShouldResponseWithString()
        {
            // Arrange
            var hostBuilder = new HostBuilder()
            .ConfigureWebHost(webHost =>
            {
                webHost.UseTestServer();
                webHost.Configure(app => app.Run(async ctx =>
                    await ctx.Response.WriteAsync("This is booking!")));
            });

            using var host = await hostBuilder.StartAsync();
            using var client = host.GetTestClient();
            var response = await client.GetAsync("/");

            // Act
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            responseString.ShouldBe("This is booking!");
        }
    }
}
