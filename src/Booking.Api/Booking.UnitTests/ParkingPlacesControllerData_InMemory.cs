﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Booking.UnitTests
{
    public class ParkingPlacesControllerData_InMemory
    {
        public ParkingPlacesController parkingPlacesController;
        public IRepository<ParkingPlace> parkingPlacesRepository;
        //public IRepository<Role> roleRepository;
        //public IRepository<Department> departmentRepository;
        private DataContext _initContext;

        public ParkingPlacesControllerData_InMemory(string mode, ILogger<ParkingPlacesController> logger)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();

            if(string.Equals(mode, "createParkingPlace"))
            {
                //roleRepository = GetInMemoryRoleRepository();
                //departmentRepository = GetInMemoryDepartmentRepository();                
                parkingPlacesRepository = new EfRepository<ParkingPlace>(_initContext);
                parkingPlacesController = new ParkingPlacesController(parkingPlacesRepository /* ,roleRepository, departmentRepository*/ , logger);
            }
            else if (string.Equals(mode, "getParkingPlace"))
            {
                //roleRepository = GetInMemoryRoleRepository();
                //departmentRepository = GetInMemoryDepartmentRepository();
                parkingPlacesRepository = GetInMemoryRepository();
                parkingPlacesController = new ParkingPlacesController(parkingPlacesRepository /*,roleRepository, departmentRepository*/ , logger);
            }
        }

        private IRepository<ParkingPlace> GetInMemoryRepository()
        {
            Seed(_initContext);
            var repository = new EfRepository<ParkingPlace>(_initContext);
            return repository;
        }

        //private IRepository<Role> GetInMemoryRoleRepository()
        //{
        //    SeedRole(_initContext);
        //    var repository = new EfRepository<Role>(_initContext);
        //    return repository;
        //}

        //private IRepository<Department> GetInMemoryDepartmentRepository()
        //{
        //    SeedDepartment(_initContext);
        //    var repository = new EfRepository<Department>(_initContext);
        //    return repository;
        //}

        public void Seed(DataContext context)
        {
            ParkingPlace parkingPlace = new ParkingPlaceBuilder()
                .WithId(Guid.Parse("53C7EFD7-9930-4FBB-B400-67AFD0DAC91D"))
                .WithNumber("12345")
                .WithLockTimeStart(DateTime.Now.AddHours(2))
                .WithDuration(123)
                .WithResourceTypeId(Guid.Parse("124EE48A-52B8-4016-8807-532155C076EE"))
                .WithParkingId(Guid.Parse("129E25E9-AEA8-4F59-9D0D-58C5AFA10C13"))
                .WithOwnerId(Guid.Parse("B1936881-E9B2-47E8-B2E0-75D355C81E75"))
                .Build();

            context.Add(parkingPlace);
            context.SaveChanges();
        }

        //public void SeedRole(DataContext context)
        //{
        //    Role role = new RoleBuilder()
        //        .WithId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
        //        .WithName("Admin")
        //        .WithDescription("The role for admin")
        //        .Build();

        //    context.Add(role);
        //    context.SaveChanges();
        //}

        //public void SeedDepartment(DataContext context)
        //{
        //    Department department = new DepartmentBuilder()
        //        .WithId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
        //        .WithName("System Development and etc.")
        //        .Build();

        //    context.Add(department);
        //    context.SaveChanges();
        //}
    }
}
