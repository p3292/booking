﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using System.Threading.Tasks;
using Booking.WebHost.Controllers;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class PingAsyncTests
    {
        private readonly PingController _pingController;

        public PingAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            
            _pingController = fixture.Build<PingController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async Task PingAsync_ReturnsValue()
        {
            // Arrange
            var value = "12345";
            // Act
            var result = await _pingController.GetPongAsync(value);

            // Assert
            result.Value.Should().Be(value);
        }

    }
}
