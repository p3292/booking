﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models.Request;
using Booking.WebHost.Models.Response;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class WorkPlacesControllerTests
    {
        private readonly Mock<IRepository<WorkPlace>> _workPlacesRepositoryMock;
        private readonly WorkPlacesController _workPlacesController;
        private readonly ILogger<WorkPlacesController> _logger;

        public WorkPlacesControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<WorkPlacesController>>().Object;
            _workPlacesRepositoryMock = fixture.Freeze<Mock<IRepository<WorkPlace>>>();
            _workPlacesController = fixture.Build<WorkPlacesController>().OmitAutoProperties().Create();
        }

        // получение списка: данные есть (200)
        [Fact]
        public async void GetWorkPlacesAsync_WorkingPlacesListIsNotNull_ShouldReturOk()
        {
            // Arrange

            // ...

            // Act
            ActionResult<WorkPlaceResponse> actionResult = await _workPlacesController.GetWorkPlacesAsync();
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<WorkPlaceResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        // получение списка: данных нет (404)
        [Fact]
        public async void GetWorkPlacesAsync_WorkPlacesListIsNull_ShouldReturNotFound()
        {
            // Arrange
            List<WorkPlace> workPlaces = null;

            _workPlacesRepositoryMock.Setup(rep => rep.GetAllAsync())
                .ReturnsAsync(workPlaces);

            // Act
            ActionResult<WorkPlaceResponse> actionResult = await _workPlacesController.GetWorkPlacesAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        // получение парк места: данные есть (200)
        //[Theory]
        //[InlineData("498C338B-068B-4E0D-A522-0B54D5A7DE1B")]
        //public async void GetWorkPlaceByIdAsync_WorkPlaceIsNotNull_ShouldReturOk(string id)
        //{
        //    // Arrange
        //    var workPlaceId = Guid.Parse(id);

        //    ResourceType resourceType = new ResourceTypeBuilder()
        //        .WithId(Guid.Parse("5581F7A2-6831-4EBA-B18C-3E2E99C237F2"))
        //        .WithName("Work place")
        //        .WithIsSelectable(true)
        //        .Build();

        //    //Parking parking = new ParkingBuilder()
        //    //   .WithId(Guid.Parse("E74E214E-2241-4FB8-A6CD-FFA5BB009761"))
        //    //   .WithName("Parking_Borders")
        //    //   .Build();

        //    Role role = new RoleBuilder()
        //        .WithId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
        //        .WithName("Admin")
        //        .WithDescription("The role for admin")
        //        .Build();

        //    Department department = new DepartmentBuilder()
        //        .WithId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
        //        .WithName("System Development")
        //        .Build();

        //    Employee owner = new EmployeeBuilder()
        //        .WithId(Guid.Parse("0EAB40BC-210F-41C9-8BC4-35DB1465EEA0"))
        //        .WithFirstName("")
        //        .WithLastName("")
        //        .WithMiddleName("")
        //        .WithIsAdmin(false)
        //        .WithCanUseSystem(false)
        //        .WithRole(role)
        //        .WithDepartment(department)
        //        .Build();

        //    WorkPlace workPlace = new WorkPlaceBuilder()
        //        .WithId(workPlaceId)
        //        .WithNumber("")
        //        .WithLockTimeStart(DateTime.Now.AddHours(2))
        //        .WithDuration(123)
        //        .WithResourceType(resourceType)
        //        //.WithParking(parking)
        //        .WithOwner(owner)
        //        .Build();

        //    _workPlacesRepositoryMock.Setup(rep => rep.GetByIdAsync(workPlaceId))
        //        .ReturnsAsync(workPlace);

        //    // Act
        //    ActionResult<WorkPlaceResponse> actionResult = await _workPlacesController.GetWorkPlaceAsync(workPlaceId);
        //    OkObjectResult result = actionResult.Result as OkObjectResult;

        //    // Assert
        //    result.Value.Should().BeOfType<WorkPlaceResponse>();
        //    result.As<OkObjectResult>().StatusCode.Should().Be(200);
        //}

        // получение парк места: данных нет (404)
        [Theory]
        [InlineData("2467D566-EF66-4F2E-8F77-C2F05258284D")]
        public async void GetWorkPlaceByIdAsync_WorkPlaceIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            WorkPlace workPlace = null;
            var workPlaceId = Guid.Parse(id);

            _workPlacesRepositoryMock.Setup(rep => rep.GetByIdAsync(workPlaceId))
                .ReturnsAsync(workPlace);

            // Act
            ActionResult<WorkPlaceResponse> actionResult = await _workPlacesController.GetWorkPlaceAsync(workPlaceId);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        // создание места: успешно
        [Fact]
        public async void CreateWorkPlaceAsync_WorkPlaceIsCorrect_ShouldReturnCreatedAtAction()
        {
            // Arrange

            var contextInMemory = new WorkPlacesControllerData_InMemory("createWorkPlace", _logger);
            var workPlacesControllerInMemory = contextInMemory.workPlacesController;

            /// 1.1 This arrange is needed if we use inmemoryDB            
            CreateOrEditWorkPlaceRequest workPlaceRequest = new WorkPlaceBuilder()
                .WithId(Guid.Parse("53C7EFD7-9930-4FBB-B400-67AFD0DAC91D"))
                .WithNumber("12345")
                .WithLockTimeStart(DateTime.Now.AddHours(2))
                .WithDuration(123)
                .WithResourceTypeId(Guid.Parse("124EE48A-52B8-4016-8807-532155C076EE"))
                //.WithParkingId(Guid.Parse("129E25E9-AEA8-4F59-9D0D-58C5AFA10C13"))
                .WithOwnerId(Guid.Parse("B1936881-E9B2-47E8-B2E0-75D355C81E75"))
                .BuildCreateOrEditWorkPlaceRequest();

            /// 2.1 This arrange is needed if we use mock for workPlaceRequest
            //var workPlaceRequest = new Mock<CreateOrEditWorkPlaceRequest>();

            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await workPlacesControllerInMemory.CreateWorkPlaceAsync(workPlaceRequest);

            /// 2.2 This act is nedded if we use mock for workPlaceRequest
            //var actionResult = await _workPlacesController.CreateWorkPlaceAsync(workPlaceRequest);

            // Assert
            CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            okResult.StatusCode.Should().Be(201);
        }

        // удаление места: успешно
        //[Theory]
        //[InlineData("085ECB06-6D88-49E1-90B1-94DE54FBF564")]
        //public async void DeleteWorkPlaceAsync_WorkPlaceIsExists_ShouldReturnNoContent(string id)
        //{
        //    // Arrange
        //    var workPlaceId = Guid.Parse(id);
        //    var contextInMemory = new WorkPlacesControllerData_InMemory("getWorkPlace", _logger);
        //    var workPlacesControllerInMemory = contextInMemory.workPlacesController;

        //    //Act
        //    var actionResult = await workPlacesControllerInMemory.DeleteWorkPlaceAsync(workPlaceId);

        //    // Assert
        //    NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
        //    noContentResult.StatusCode.Should().Be(204);
        //}

        // удаление места: не найдено
        [Theory]
        [InlineData("310F41CE-752D-4AF8-8763-047D8D3E9146")]
        public async void DeleteWorkPlaceAsync_WorkPlaceIsNull_ShouldReturnNoContent(string id)
        {
            // Arrange
            WorkPlace workPlace = null;
            var workPlaceId = Guid.Parse(id);

            _workPlacesRepositoryMock.Setup(rep => rep.GetByIdAsync(workPlaceId))
                .ReturnsAsync(workPlace);

            // Act
            IActionResult actionResult = await _workPlacesController.DeleteWorkPlaceAsync(workPlaceId);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        // редактирование места: успешно
        //[Theory]
        //[InlineData("6C888652-3C66-477D-B9F1-8D738D9E9C38")]
        //public async void EditWorkPlaceAsync_WorkPlaceIsExists_ShouldReturnNoContent(string id)
        //{
        //    // Arrange
        //    var workPlaceId = Guid.Parse(id);
        //    var contextInMemory = new WorkPlacesControllerData_InMemory("getWorkPlace", _logger);
        //    var workPlacesControllerInMemory = contextInMemory.workPlacesController;

        //    /// 1.1 This arrange is needed if we use inmemoryDB
        //    CreateOrEditWorkPlaceRequest workPlaceRequest = new WorkPlaceBuilder()
        //        .WithId(Guid.Parse("53C7EFD7-9930-4FBB-B400-67AFD0DAC91D"))
        //        .WithNumber("12345")
        //        .WithLockTimeStart(DateTime.Now.AddHours(2))
        //        .WithDuration(123)
        //        .WithResourceTypeId(Guid.Parse("124EE48A-52B8-4016-8807-532155C076EE"))
        //        //.WithParkingId(Guid.Parse("129E25E9-AEA8-4F59-9D0D-58C5AFA10C13"))
        //        .WithOwnerId(Guid.Parse("B1936881-E9B2-47E8-B2E0-75D355C81E75"))
        //        .BuildCreateOrEditWorkPlaceRequest();

        //    // 2.1 This arrange is needed if we use mock for workPlaceRequest
        //    //var workPlaceRequest = new Mock<CreateOrEditWorkPlaceRequest>();

        //    //Act
        //    /// 1.2 This act is needed if we use inmemoryDB
        //    var actionResult = await workPlacesControllerInMemory.EditWorkPlaceAsync(workPlaceId, workPlaceRequest);
        //    /// 2.2 This act is nedded if we use mock for workPlaceRequest
        //    //var actionResult = await _workPlacesController.CreateWorkPlaceAsync(workPlaceRequest);

        //    // Assert
        //    NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
        //    // CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
        //    noContentResult.StatusCode.Should().Be(204);
        //}

        // редактирование места: не найдено
        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void EditWorkPlaceAsync_WorkPlaceIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            WorkPlace workPlace = null;
            var workPlaceId = Guid.Parse(id);

            // Assert
            CreateOrEditWorkPlaceRequest workPlaceRequest = new WorkPlaceBuilder()
                            .WithId(Guid.Parse("53C7EFD7-9930-4FBB-B400-67AFD0DAC91D"))
                            .WithNumber("12345")
                            .WithLockTimeStart(DateTime.Now.AddHours(2))
                            .WithDuration(123)
                            .WithResourceTypeId(Guid.Parse("124EE48A-52B8-4016-8807-532155C076EE"))
                            //.WithParkingId(Guid.Parse("129E25E9-AEA8-4F59-9D0D-58C5AFA10C13"))
                            .WithOwnerId(Guid.Parse("B1936881-E9B2-47E8-B2E0-75D355C81E75"))
                            .BuildCreateOrEditWorkPlaceRequest();

            _workPlacesRepositoryMock.Setup(rep => rep.GetByIdAsync(workPlaceId))
                .ReturnsAsync(workPlace);

            // Act
            IActionResult actionResult = await _workPlacesController.EditWorkPlaceAsync(workPlaceId, workPlaceRequest);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }
    }
}