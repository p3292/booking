﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models.Response;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class ParkingsControllerTests
    {
        private readonly Mock<IRepository<Parking>> _parkingsRepositoryMock;
        private readonly ParkingController _parkingsController;
        private readonly ILogger<ParkingController> _logger;

        public ParkingsControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<ParkingController>>().Object;
            _parkingsRepositoryMock = fixture.Freeze<Mock<IRepository<Parking>>>();
            _parkingsController = fixture.Build<ParkingController>().OmitAutoProperties().Create();
        }

        // ...

    }
}