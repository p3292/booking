﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class ResourceTypesControllerTests
    {
        private readonly Mock<IRepository<ResourceType>> _resourceTypesRepositoryMock;
        private readonly ResourceTypesController _resourceTypesController;
        private readonly ILogger<ResourceTypesController> _logger;

        public ResourceTypesControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<ResourceTypesController>>().Object;
            _resourceTypesRepositoryMock = fixture.Freeze<Mock<IRepository<ResourceType>>>();
            _resourceTypesController = fixture.Build<ResourceTypesController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetResourceTypesAsync_ResourceTypes_ShouldReturOk()
        {
            // Arrange

            // Act
            ActionResult<ResourceType> actionResult = await _resourceTypesController.GetResourceTypesAsync();

            // Assert
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            actionResult.Result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Fact]
        public async void GetResourceTypesAsync_ResourceTypeIsNull_ShouldReturNotFound()
        {
            // Arrange
            List<ResourceType> resourceType = null;

            _resourceTypesRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(resourceType);

            // Act
            ActionResult<ResourceType> actionResult = await _resourceTypesController.GetResourceTypesAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void GetResourceTypeAsync_ResourceType_ShouldReturOk(string id)
        {
            // Arrange
            var resourceTypeId = Guid.Parse(id);

            ResourceType resourceType = new ResourceTypeBuilder()
                .WithId(resourceTypeId)
                .WithName("Parking place.")
                .WithIsSelectable(true)
                .Build();

            _resourceTypesRepositoryMock.Setup(repo => repo.GetByIdAsync(resourceTypeId))
                .ReturnsAsync(resourceType);

            // Act
            ActionResult<ResourceType> actionResult = await _resourceTypesController.GetResourceTypeAsync(resourceTypeId);

            // Assert
            actionResult.Result.Should().BeOfType<OkObjectResult>();
            actionResult.Result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void GetResourceTypeAsync_ResourceTypeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            ResourceType resourceType = null;
            var resourceTypeId = Guid.Parse(id);
            _resourceTypesRepositoryMock.Setup(repo => repo.GetByIdAsync(resourceTypeId))
                .ReturnsAsync(resourceType);

            // Act
            ActionResult<ResourceType> actionResult = await _resourceTypesController.GetResourceTypeAsync(resourceTypeId);

            // Assert
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
        }

        [Fact]
        public async void CreateResourceTypeAsync_ResourceTypeResponse_ShouldReturnCreatedAtAction()
        {
            // Arrange
            var resourceTypeRequest = new Mock<CreateOrEditResourceTypeRequest>();

            //Act
            var actionResult = await _resourceTypesController.CreateResourceTypeAsync(resourceTypeRequest.Object);

            // Assert
            actionResult.Should().BeOfType<OkObjectResult>();
            actionResult.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void EditResourceTypeAsync_ResourceTypeEditOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var resourceTypeId = Guid.Parse(id);
            var resourceTypeRequest = new Mock<CreateOrEditResourceTypeRequest>();

            //Act
            var actionResult = await _resourceTypesController.EditResourceTypeAsync(resourceTypeId, resourceTypeRequest.Object);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void EditResourceTypeAsync_ResourceTypeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            ResourceType resourceType = null;
            var resourceTypeId = Guid.Parse(id);
            CreateOrEditResourceTypeRequest resourceTypeRequest = new ResourceTypeBuilder()
                .WithName("Parking place.")
                .WithIsSelectable(true)
                .BuildCreateOrEditResourceTypeRequest();

            _resourceTypesRepositoryMock.Setup(repo => repo.GetByIdAsync(resourceTypeId))
                .ReturnsAsync(resourceType);

            // Act
            IActionResult actionResult = await _resourceTypesController.EditResourceTypeAsync(resourceTypeId, resourceTypeRequest);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void DeleteResourceTypeAsync_ResourceTypeDeleteOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var resourceTypeId = Guid.Parse(id);

            //Act
            var actionResult = await _resourceTypesController.DeleteResourceTypeAsync(resourceTypeId);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("34e2d055-1089-4c2d-a914-854b13d9e38a")]
        public async void DeleteResourceTypeAsync_ResourceTypeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            ResourceType resourceType = null;
            var resourceTypeId = Guid.Parse(id);

            _resourceTypesRepositoryMock.Setup(repo => repo.GetByIdAsync(resourceTypeId))
                .ReturnsAsync(resourceType);

            // Act
            IActionResult actionResult = await _resourceTypesController.DeleteResourceTypeAsync(resourceTypeId);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }
    }
}
