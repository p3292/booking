﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models.Request;
using Booking.WebHost.Models.Response;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class DepartmentsControllerTests
    {
        private readonly Mock<IRepository<Department>> _departmentsRepositoryMock;
        private readonly DepartmentsController _departmentsController;
        private readonly ILogger<DepartmentsController> _logger;

        public DepartmentsControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<DepartmentsController>>().Object;
            _departmentsRepositoryMock = fixture.Freeze<Mock<IRepository<Department>>>();
            _departmentsController = fixture.Build<DepartmentsController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetDepartmentsAsync_DepartmentShortResponseList_ShouldReturOk()
        {
            // Arrange

            // Act
            ActionResult<DepartmentShortResponse> actionResult = await _departmentsController.GetDepartmentsAsync();
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<DepartmentShortResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Fact]
        public async void GetDepartmentsAsync_DepartmentIsNull_ShouldReturNotFound()
        {
            // Arrange
            List<Department> department = null;

            _departmentsRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(department);

            // Act
            ActionResult<DepartmentShortResponse> actionResult = await _departmentsController.GetDepartmentsAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void GetDepartmentAsync_DepartmentResponse_ShouldReturOk(string id)
        {
            // Arrange
            var departmentId = Guid.Parse(id);

            Department department = new DepartmentBuilder()
                .WithId(departmentId)
                .WithName("System Development and etc.")
                .Build();


            _departmentsRepositoryMock.Setup(repo => repo.GetByIdAsync(departmentId))
                .ReturnsAsync(department);

            // Act
            ActionResult<DepartmentShortResponse> actionResult = await _departmentsController.GetDepartmentAsync(departmentId);
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<DepartmentShortResponse>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void GetDepartmentAsync_DepartmentIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Department department = null;
            var departmentId = Guid.Parse(id);
            _departmentsRepositoryMock.Setup(repo => repo.GetByIdAsync(departmentId))
                .ReturnsAsync(department);

            // Act
            ActionResult<DepartmentShortResponse> actionResult = await _departmentsController.GetDepartmentAsync(departmentId);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void CreateDepartmentAsync_DepartmentResponse_ShouldReturnCreatedAtAction()
        {
            // Arrange
            var contextInMemory = new DepartmentsControllerData_InMemory(_logger);
            //var deparmentsRepositoryInMemory = contextInMemory.departmentRepository;
            var departmentsControllerInMemory = contextInMemory.departmentsController;

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditDepartmentRequest departmentRequest = new DepartmentBuilder()
                     .WithName("System Development and etc.")
                     .BuildCreateOrEditDepartmentRequest();
            /// 2.1 This arrange is needed if we use mock for departmentRequest
            //var departmentRequest = new Mock<CreateOrEditDepartmentRequest>();

            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await departmentsControllerInMemory.CreateDepartmentAsync(departmentRequest);
            /// 2.2 This act is nedded if we use mock for departmentRequest
            //var actionResult = await _departmentsController.CreateDepartmentAsync(departmentRequest.Object);

            // Assert
            //CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            //okResult.StatusCode.Should().Be(201);

            OkObjectResult okResult = actionResult.Result.Should().BeOfType<OkObjectResult>().Subject;
            okResult.StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void EditDepartmentAsync_DeparmentEditOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var departmentId = Guid.Parse(id);
            var contextInMemory = new DepartmentsControllerData_InMemory(_logger);
            var departmentsControllerInMemory = contextInMemory.departmentsController;

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditDepartmentRequest departmentRequest = new DepartmentBuilder()
                    .WithName("System Development and etc.")
                    .BuildCreateOrEditDepartmentRequest();
            /// 2.1 This arrange is needed if we use mock for departmentRequest
            //var departmentRequest = new Mock<CreateOrEditDepartmentRequest>();


            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await departmentsControllerInMemory.EditDepartmentAsync(departmentId, departmentRequest);
            /// 2.2 This act is nedded if we use mock for departmentRequest
            //var actionResult = await _departmentsController.EditDepartmentAsync(departmentRequest.Object);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            //CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void EditDepartmentAsync_DepartmentIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Department department = null;
            var departmentId = Guid.Parse(id);
            CreateOrEditDepartmentRequest departmentRequest = new DepartmentBuilder()
                    .WithName("System Development and etc.")
                    .BuildCreateOrEditDepartmentRequest();

            _departmentsRepositoryMock.Setup(repo => repo.GetByIdAsync(departmentId))
                .ReturnsAsync(department);

            // Act
            IActionResult actionResult = await _departmentsController.EditDepartmentAsync(departmentId, departmentRequest);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void DeleteDepartmentAsync_DepartmentDeleteOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var departmentId = Guid.Parse(id);
            var contextInMemory = new DepartmentsControllerData_InMemory(_logger);
            var departmentsControllerInMemory = contextInMemory.departmentsController;

            //Act
            var actionResult = await departmentsControllerInMemory.DeleteDepartmentAsync(departmentId);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void DeleteDepartmentAsync_DepartmentIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Department department = null;
            var departmentId = Guid.Parse(id);

            _departmentsRepositoryMock.Setup(repo => repo.GetByIdAsync(departmentId))
                .ReturnsAsync(department);

            // Act
            IActionResult actionResult = await _departmentsController.DeleteDepartmentAsync(departmentId);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }
    }
}
