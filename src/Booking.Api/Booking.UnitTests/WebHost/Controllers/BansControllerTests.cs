﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models.Response;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class BansControllerTests
    {
        private readonly ILogger<BansController> _logger;

        private readonly Mock<IRepository<Ban>> _bansRepositoryMock;
        private readonly Mock<IRepository<Employee>> _employeeRepositoryMock;
        private readonly Mock<IRepository<ResourceType>> _resourceTypeRepositoryMock;
        private readonly Mock<Core.ICurrentDateTimeProvider> _currentDateTimeProviderMock;

        private readonly BansController _bansController;

        private readonly DateTime NowDate = new DateTime(2022, 01, 01);

        public BansControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<BansController>>().Object;

            _bansRepositoryMock = fixture.Freeze<Mock<IRepository<Ban>>>();
            _employeeRepositoryMock = fixture.Freeze<Mock<IRepository<Employee>>>();
            _resourceTypeRepositoryMock = fixture.Freeze<Mock<IRepository<ResourceType>>>();

            _currentDateTimeProviderMock = fixture.Freeze<Mock<Core.ICurrentDateTimeProvider>>();
            _currentDateTimeProviderMock.Setup(p => p.CurrentDateTime).Returns(NowDate);

            _bansController = fixture.Build<BansController>().OmitAutoProperties().Create();
        }
        [Fact]
        public async void GetBansAsync_BanResponseList_ShouldReturnOk()
        {
            // Arrange

            // Act
            var actionResult = await _bansController.GetBanListAsync();
            var result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<BanResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Fact]
        public async void GetBansAsync_BanIsNull_ShouldReturnNotFound()
        {
            // Arrange
            List<Ban> bans = null;

            _bansRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(bans);

            // Act
            var actionResult = await _bansController.GetBanListAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void GetBansByUserIdAsync_ListBanResponse_ShouldReturnOk()
        {
            // Arrange
            var employeeId = Guid.NewGuid();
            var banId = Guid.NewGuid();
            var bans = new List<Ban>();

            var ban = new BanBuilder()
                .WithId(banId)
                .WithEmployeeId(employeeId)
                .WithResourceTypeId(Guid.NewGuid())
                .WithStartDateTime(DateTime.UtcNow)
                .WithEndDateTime(DateTime.UtcNow.AddDays(10))
                .Build();

            bans.Add(ban);

            _bansRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(bans);

            // Act
            var actionResult = await _bansController.GetBansByUserIdAsync(employeeId);
            var result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<BanResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("dcb54f40-cea4-42c2-b40f-250761783c75")]
        public async void GetBansByUserIdAsync_BanIsNull_ShouldReturnNotFound(string employeeId)
        {
            // Arrange
            var empId = Guid.Parse(employeeId);
            List<Ban> bans = null;

            _bansRepositoryMock.Setup(repo => repo.GetWhereAsync(x =>
                x.EmployeeId == empId))
                .ReturnsAsync(bans);

            // Act
            var actionResult = await _bansController.GetBansByUserIdAsync(empId);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void AddBanForUserAsync_BanResponse_ShouldReturnCreatedAtAction()
        {
            // Arrange
            var banRequest = new BanBuilder()
                .WithEmployeeId(Guid.NewGuid())
                .WithResourceTypeId(Guid.NewGuid())
                .BuildCreateBanRequest();

            IEnumerable<Ban> bans = null;

            _bansRepositoryMock.Setup(repo => repo.GetWhereAsync(x =>
               x.EmployeeId == banRequest.EmployeeId &&
               x.ResourceTypeId == banRequest.ResourceTypeId))
                .ReturnsAsync(bans);

            //Act
            var actionResult = await _bansController.AddBanForUserAsync(banRequest);

            // Assert
            //var okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            //okResult.StatusCode.Should().Be(201);

            OkObjectResult okResult = actionResult.Result.Should().BeOfType<OkObjectResult>().Subject;
            okResult.StatusCode.Should().Be(200);
        }

        [Fact]
        public async void AddBanForUserAsync_EmployeeIsNull_ShouldReturnNotFound()
        {
            // Arrange
            var banRequest = new BanBuilder()
                .WithEmployeeId(Guid.NewGuid())
                .WithResourceTypeId(Guid.NewGuid())
                .BuildCreateBanRequest();

            Employee employee = null;

            _employeeRepositoryMock.Setup(repo => repo.GetByIdAsync(banRequest.EmployeeId))
                .ReturnsAsync(employee);

            //Act
            var actionResult = await _bansController.AddBanForUserAsync(banRequest);

            // Assert
            actionResult.Result.As<NotFoundObjectResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundObjectResult>();


        }
        [Fact]
        public async void AddBanForUserAsync_ResourceTypeIdIsNull_ShouldReturnNotFound()
        {
            // Arrange
            var banRequest = new BanBuilder()
                .WithEmployeeId(Guid.NewGuid())
                .WithResourceTypeId(Guid.NewGuid())
                .BuildCreateBanRequest();

            ResourceType resourceType = null;

            _resourceTypeRepositoryMock.Setup(repo => repo.GetByIdAsync(banRequest.ResourceTypeId))
                .ReturnsAsync(resourceType);

            //Act
            ActionResult<BanResponse> actionResult = await _bansController.AddBanForUserAsync(banRequest);

            // Assert
            actionResult.Result.As<NotFoundObjectResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        [Fact]
        public async void ClearBanListAsync_BanResponse_ShouldReturnNoContent()
        {
            // Arrange

            //Act
            var actionResult = await _bansController.ClearBanListAsync();

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }
        [Fact]
        public async void ClearBanListAsync_EndDateTimeEqDateTimeNow_ShouldReturnNoContent()
        {
            // Arrange
            var bans = new List<Ban>();
            bans.AddRange(
              new[]{  new BanBuilder()
                          .WithEmployeeId(Guid.NewGuid())
                          .WithResourceTypeId(Guid.NewGuid())
                          .WithEndDateTime(null)
                          .Build(),
                      new BanBuilder()
                          .WithEmployeeId(Guid.NewGuid())
                          .WithResourceTypeId(Guid.NewGuid())
                          .WithEndDateTime(null)
                          .Build()
              }
            );

            _bansRepositoryMock.Setup(repo => repo.GetWhereAsync(x =>
                   x.EndDateTime == null))
                .ReturnsAsync(bans);

            //Act
            var actionResult = await _bansController.ClearBanListAsync();

            // Assert
            var noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);

            bans.Should().OnlyContain(x => x.EndDateTime.Equals(NowDate));

        }
        [Fact]
        public async void ClearBanForUserAsync_EndDateTimeEqDateTimeNow_ShouldReturnNoContent()
        {
            // Arrange
            var bans = new List<Ban>();

            var banRequest = new BanBuilder()
             .WithEmployeeId(Guid.NewGuid())
             .WithResourceTypeId(Guid.NewGuid())
             .BuildCreateBanRequest();

            bans.Add(new BanBuilder()
                         .WithEmployeeId(banRequest.EmployeeId)
                         .WithResourceTypeId(banRequest.ResourceTypeId)
                         .WithEndDateTime(null)
                         .Build()
                         );

            _bansRepositoryMock.Setup(repo => repo.GetWhereAsync(x =>
                x.EmployeeId == banRequest.EmployeeId &&
                x.ResourceTypeId == banRequest.ResourceTypeId))
                .ReturnsAsync(bans);

            //Act
            var actionResult = await _bansController.ClearBanForUserAsync(banRequest);

            // Assert
            var noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);

            bans.Should().OnlyContain(x => x.EndDateTime.Equals(NowDate));

        }

    }
}
