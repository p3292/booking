﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class RolesControllerTests
    {
        private readonly Mock<IRepository<Role>> _rolesRepositoryMock;
        private readonly RolesController _rolesController;
        private readonly ILogger<RolesController> _logger;

        public RolesControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });

            _logger = new Mock<ILogger<RolesController>>().Object;
            _rolesRepositoryMock = fixture.Freeze<Mock<IRepository<Role>>>();
            _rolesController = fixture.Build<RolesController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetRolesAsync_RoleShortResponseList_ShouldReturOk()
        {
            // Arrange

            // Act
            ActionResult<RoleShortResponse> actionResult = await _rolesController.GetRolesAsync();
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<RoleShortResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Fact]
        public async void GetRolesAsync_RoleIsNull_ShouldReturNotFound()
        {
            // Arrange
            List<Role> role = null;

            _rolesRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(role);

            // Act
            ActionResult<RoleShortResponse> actionResult = await _rolesController.GetRolesAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void GetRoleAsync_RoleResponse_ShouldReturOk(string id)
        {
            // Arrange
            var roleId = Guid.Parse(id);

            Role role = new RoleBuilder()
                .WithId(roleId)
                .WithName("System Development and etc.")
                .Build();

            _rolesRepositoryMock.Setup(repo => repo.GetByIdAsync(roleId))
                .ReturnsAsync(role);

            // Act
            ActionResult<RoleShortResponse> actionResult = await _rolesController.GetRoleAsync(roleId);
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<RoleShortResponse>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void GetRoleAsync_RoleIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Role role = null;
            var roleId = Guid.Parse(id);
            _rolesRepositoryMock.Setup(repo => repo.GetByIdAsync(roleId))
                .ReturnsAsync(role);

            // Act
            ActionResult<RoleShortResponse> actionResult = await _rolesController.GetRoleAsync(roleId);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void CreateRoleAsync_RoleResponse_ShouldReturnCreatedAtAction()
        {
            // Arrange
            var contextInMemory = new RolesControllerData_InMemory(_logger);
            //var deparmentsRepositoryInMemory = contextInMemory.departmentRepository;
            var rolesControllerInMemory = contextInMemory.rolesController;

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditRoleRequest roleRequest = new RoleBuilder()
                    .WithName("Admin")
                    .WithDescription("The role for admin")
                    .BuildCreateOrEditRoleRequest();
            /// 2.1 This arrange is needed if we use mock for roleRequest
            //var roleRequest = new Mock<CreateOrEditRoleRequest>();

            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await rolesControllerInMemory.CreateRoleAsync(roleRequest);
            /// 2.2 This act is nedded if we use mock for roleRequest
            //var actionResult = await _rolesController.CreateRoleAsync(roleRequest.Object);

            // Assert
            CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            okResult.StatusCode.Should().Be(201);
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void EditRoleAsync_RoleEditOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var roleId = Guid.Parse(id);
            var contextInMemory = new RolesControllerData_InMemory(_logger);
            var rolesControllerInMemory = contextInMemory.rolesController;

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditRoleRequest roleRequest = new RoleBuilder()
                    .WithName("Admin")
                    .WithDescription("The role for admin")
                    .BuildCreateOrEditRoleRequest();
            /// 2.1 This arrange is needed if we use mock for roleRequest
            //var roleRequest = new Mock<CreateOrEditRoleRequest>();

            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await rolesControllerInMemory.EditRoleAsync(roleId, roleRequest);
            /// 2.2 This act is nedded if we use mock for roleRequest
            //var actionResult = await _rolesController.EditRoleAsync(roleRequest.Object);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            //CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void EditRoleAsync_RoleIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Role role = null;
            var roleId = Guid.Parse(id);
            CreateOrEditRoleRequest roleRequest = new RoleBuilder()
                    .WithName("Admin")
                    .WithDescription("The role for admin")
                    .BuildCreateOrEditRoleRequest();

            _rolesRepositoryMock.Setup(repo => repo.GetByIdAsync(roleId))
                .ReturnsAsync(role);

            // Act
            IActionResult actionResult = await _rolesController.EditRoleAsync(roleId, roleRequest);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void DeleteRoleAsync_RoleDeleteOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var roleId = Guid.Parse(id);
            var contextInMemory = new RolesControllerData_InMemory(_logger);
            var rolesControllerInMemory = contextInMemory.rolesController;

            //Act
            var actionResult = await rolesControllerInMemory.DeleteRoleAsync(roleId);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0")]
        public async void DeleteRoleAsync_RoleIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Role role = null;
            var roleId = Guid.Parse(id);

            _rolesRepositoryMock.Setup(repo => repo.GetByIdAsync(roleId))
                .ReturnsAsync(role);

            // Act
            IActionResult actionResult = await _rolesController.DeleteRoleAsync(roleId);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }
    }
}
