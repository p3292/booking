﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers.v1;
using Booking.WebHost.Models;
using Booking.WebHost.Publishers;
using BookingIdentityEmployee;
using FluentAssertions;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Booking.UnitTests.WebHost.Controllers
{
    public class EmployeesControllerTests
    {
        private readonly Mock<IRepository<Employee>> _employeesRepositoryMock;
        private readonly EmployeesController _employeesController;
        private readonly ILogger<EmployeesController> _logger;
        private readonly IBus _bus;

        public EmployeesControllerTests()
        {
           //_bus = bus;
           
            var fixture = new Fixture().Customize(new AutoMoqCustomization() { ConfigureMembers = true });
            _logger = new Mock<ILogger<EmployeesController>>().Object;
            _employeesRepositoryMock = fixture.Freeze<Mock<IRepository<Employee>>>();
            _employeesController = fixture.Build<EmployeesController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetEmployeesAsync_EmployeeShortResponseList_ShouldReturOk()
        {
            // Arrange

            // Act
            ActionResult<EmployeeShortResponse> actionResult = await _employeesController.GetEmployeesAsync();
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<List<EmployeeShortResponse>>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Fact]
        public async void GetEmployeesAsync_EmployeeIsNull_ShouldReturNotFound()
        {
            // Arrange
            List<Employee> employee = null;

            _employeesRepositoryMock.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(employee);

            // Act
            ActionResult<EmployeeShortResponse> actionResult = await _employeesController.GetEmployeesAsync();

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void GetEmployeeAsync_EmployeeResponse_ShouldReturOk(string id)
        {
            // Arrange
            var employeeId = Guid.Parse(id);

            Role role = new RoleBuilder()
                .WithId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build();

            Department department = new DepartmentBuilder()
                .WithId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .WithName("System Development and etc.")
                .Build();

            Employee employee = new EmployeeBuilder()
                .WithId(employeeId)
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRole(role)
                .WithDepartment(department)
                .Build();

            _employeesRepositoryMock.Setup(repo => repo.GetByIdAsync(employeeId))
                .ReturnsAsync(employee);

            // Act
            ActionResult<EmployeeResponse> actionResult = await _employeesController.GetEmployeeAsync(employeeId);
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<EmployeeResponse>();
            result.As<OkObjectResult>().StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void GetEmployeeAsync_EmployeeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Employee employee = null;
            var employeeId = Guid.Parse(id);
            _employeesRepositoryMock.Setup(repo => repo.GetByIdAsync(employeeId))
                .ReturnsAsync(employee);

            // Act
            ActionResult<EmployeeResponse> actionResult = await _employeesController.GetEmployeeAsync(employeeId);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("Ivanov", "Ivanov",200)]
        public async void GetEmployeeByLoginAsync_LoginIsCorrect_ShouldReturnOk(string login, string testLogin, int statusCode)
        {
            // Arrange
            var employeeId = Guid.NewGuid();

            Employee employee = new EmployeeBuilder()
                .WithId(employeeId)
                .WithLogin(login)
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRole(new RoleBuilder()
                .WithId(Guid.NewGuid())
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build())
                .WithDepartment(new DepartmentBuilder()
                .WithId(Guid.NewGuid())
                .WithName("System Development and etc.")
                .Build())
                .Build();

            // Act
            ActionResult<EmployeeResponse> actionResult = await _employeesController.GetEmployeeByLoginAsync(testLogin);
            OkObjectResult result = actionResult.Result as OkObjectResult;

            // Assert
            result.Value.Should().BeOfType<EmployeeResponse>();
            result.As<OkObjectResult>().StatusCode.Should().Be(statusCode);
        }
        
        [Theory]
        [InlineData("Ivanov", "Sidorov",404)]
        public async void GetEmployeeByLoginAsync_LoginIsNull_ShouldReturNotFound(string login, string testLogin, int statusCode)
        {
            // Arrange
            var employeeId = Guid.NewGuid();

            Employee employee = new EmployeeBuilder()
                .WithId(employeeId)
                .WithLogin(login)
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRole(new RoleBuilder()
                .WithId(Guid.NewGuid())
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build())
                .WithDepartment(new DepartmentBuilder()
                .WithId(Guid.NewGuid())
                .WithName("System Development and etc.")
                .Build())
                .Build();

            var employeeMock = new Mock<IRepository<Employee>>();
            employeeMock.Setup(repo => repo.GetByIdAsync(employeeId))
                .ReturnsAsync(employee);
            var roleMock = new Mock<IRepository<Role>>();
            var departmentMock = new Mock<IRepository<Department>>();
            var mockBus = new Mock<IBus>();
            var userPublisherMock = new Mock<UserPublisher>(mockBus.Object);
            var controller = new EmployeesController(employeeMock.Object, roleMock.Object, departmentMock.Object, _logger, userPublisherMock.Object);

            // Act
            ActionResult<EmployeeResponse> actionResult = await controller.GetEmployeeByLoginAsync(testLogin);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(statusCode);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("22780427-16d3-49b1-9320-80a511a648a0", "", "def47943-7aaf-44a1-ae21-05aa4948b165")]
        [InlineData("", "dcb54f40-cea4-42c2-b40f-250761783c75", "def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void CreateEmployeeAsync_RoleOrDepartmentIsNull_ShouldReturnRoleOrDepartmentNotFound(string roleId, string departmentId, string employeeId)
        {
            // Arrange
            string repoEmployeeId = "def47943-7aaf-44a1-ae21-05aa4948b165";
            string repoRoleId = "22780427-16d3-49b1-9320-80a511a648a0";
            string repoDepartmentId = "dcb54f40-cea4-42c2-b40f-250761783c75";

            Role role = new RoleBuilder()
                .WithName("Admin")
                .WithDescription("The role for admin")
                .Build();

            if (!string.IsNullOrEmpty(roleId))
            {
                role = new RoleBuilder()
                    .WithId(Guid.Parse(roleId))
                    .WithName("Admin")
                    .WithDescription("The role for admin")
                    .Build();
            }

            Department department = new DepartmentBuilder()
                .WithName("System Development and etc.")
                .Build();

            if (!string.IsNullOrEmpty(departmentId))
            {
                department = new DepartmentBuilder()
                    .WithId(Guid.Parse(departmentId))
                    .WithName("System Development and etc.")
                    .Build();
            }

            Employee employee = new EmployeeBuilder()
                .WithId(Guid.Parse(employeeId))
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRole(role)
                .WithDepartment(department)
                .Build();

            List<Employee> employeeList = new()
            {
                employee
            };

            CreateOrEditEmployeeRequest employeeRequest = new EmployeeBuilder()
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRole(role)
                .WithDepartment(department)
                .BuildCreateOrEditEmployeeRequest();



            var employeeMock = new Mock<IRepository<Employee>>();
            employeeMock.Setup(repo => repo.GetByIdAsync(Guid.Parse(repoEmployeeId)))
                .ReturnsAsync(employee);

            var roleMock = new Mock<IRepository<Role>>();
            roleMock.Setup(repo => repo.GetByIdAsync(Guid.Parse(repoRoleId)))
                .ReturnsAsync(role);

            var departmentMock = new Mock<IRepository<Department>>();
            departmentMock.Setup(repo => repo.GetByIdAsync(Guid.Parse(repoDepartmentId)))
                .ReturnsAsync(department);
            var userPublisherMock = new Mock<UserPublisher>(null);
            var controller = new EmployeesController(employeeMock.Object, roleMock.Object, departmentMock.Object, _logger, userPublisherMock.Object);

            //Act
            var actionResult = await controller.CreateEmployeeAsync(employeeRequest);

            // Assert
            actionResult.Result.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }
      
        [Fact]
        public async void CreateEmployeeAsync_EmployeeResponse_ShouldReturnCreatedAtAction()
        {
            // Arrange
            // EndpointConvention.Map<BookingIdentityUserDto>(new Uri("http://random"));
            var mockBus = new Mock<IBus>();

            //var snakeCaseEndpointNameFormatter = new SnakeCaseEndpointNameFormatter(false);
            //EndpointConvention.Map<BookingIdentityUserDto>(new Uri($"queue:{ snakeCaseEndpointNameFormatter.Message<BookingIdentityUserDto>()}"));

            

            mockBus.Setup(n => n.GetPublishSendEndpoint<BookingIdentityUserDto>());
            var userPublisherMock = new Mock<UserPublisher>(null);
            var contextInMemory = new EmployeesControllerData_InMemory("createEmployee", _logger, userPublisherMock.Object);
            //var employeesRepositoryInMemory = contextInMemory.employeeRepository;
            var employeesControllerInMemory = contextInMemory.employeesController;


            

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditEmployeeRequest employeeRequest = new EmployeeBuilder()
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRoleId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithDepartmentId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .BuildCreateOrEditEmployeeRequest();
            /// 2.1 This arrange is needed if we use mock for employeeRequest
            //var employeeRequest = new Mock<CreateOrEditEmployeeRequest>();


            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await employeesControllerInMemory.CreateEmployeeAsync(employeeRequest);
            /// 2.2 This act is nedded if we use mock for employeeRequest
            //var actionResult = await _employeesController.CreateEmployeeAsync(employeeRequest.Object);

            // Assert
            OkObjectResult okResult = actionResult.Result.Should().BeOfType<OkObjectResult>().Subject;
            okResult.StatusCode.Should().Be(200);
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void EditEmployeesAsync_EmployeeEditOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var userPublisherMock = new Mock<UserPublisher>(null);
            var employeeId = Guid.Parse(id);
            var contextInMemory = new EmployeesControllerData_InMemory("getEmployee", _logger, userPublisherMock.Object);
            var employeesControllerInMemory = contextInMemory.employeesController;

            /// 1.1 This arrange is needed if we use inmemoryDB
            CreateOrEditEmployeeRequest employeeRequest = new EmployeeBuilder()
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRoleId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithDepartmentId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .BuildCreateOrEditEmployeeRequest();
            /// 2.1 This arrange is needed if we use mock for employeeRequest
            //var employeeRequest = new Mock<CreateOrEditEmployeeRequest>();


            //Act
            /// 1.2 This act is needed if we use inmemoryDB
            var actionResult = await employeesControllerInMemory.EditEmployeesAsync(employeeId, employeeRequest);
            /// 2.2 This act is nedded if we use mock for employeeRequest
            //var actionResult = await _employeesController.CreateEmployeeAsync(employeeRequest.Object);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            //CreatedAtActionResult okResult = actionResult.Result.Should().BeOfType<CreatedAtActionResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void EditEmployeesAsync_EmployeeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Employee employee = null;
            var employeeId = Guid.Parse(id);
            CreateOrEditEmployeeRequest employeeRequest = new EmployeeBuilder()
                .WithFirstName("Capitan")
                .WithLastName("Nemo")
                .WithMiddleName("Nautilus")
                .WithIsAdmin(true)
                .WithCanUseSystem(true)
                .WithRoleId(Guid.Parse("22780427-16d3-49b1-9320-80a511a648a0"))
                .WithDepartmentId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .BuildCreateOrEditEmployeeRequest();

            _employeesRepositoryMock.Setup(repo => repo.GetByIdAsync(employeeId))
                .ReturnsAsync(employee);

            // Act
            IActionResult actionResult = await _employeesController.EditEmployeesAsync(employeeId, employeeRequest);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void DeleteEmployeeAsync_EmployeeDeleteOk_ShouldReturnNoContent(string id)
        {
            // Arrange
            var mockBus = new Mock<IBus>();
            var userPublisherMock = new Mock<UserPublisher>(mockBus.Object);
            var employeeId = Guid.Parse(id);
            var contextInMemory = new EmployeesControllerData_InMemory("getEmployee", _logger, userPublisherMock.Object);
            var employeesControllerInMemory = contextInMemory.employeesController;

            //Act
            var actionResult = await employeesControllerInMemory.DeleteEmployeeAsync(employeeId);

            // Assert
            NoContentResult noContentResult = actionResult.Should().BeOfType<NoContentResult>().Subject;
            noContentResult.StatusCode.Should().Be(204);
        }

        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        public async void DeleteEmployeeAsync_EmployeeIsNull_ShouldReturNotFound(string id)
        {
            // Arrange
            Employee employee = null;
            var employeeId = Guid.Parse(id);

            _employeesRepositoryMock.Setup(repo => repo.GetByIdAsync(employeeId))
                .ReturnsAsync(employee);

            // Act
            IActionResult actionResult = await _employeesController.DeleteEmployeeAsync(employeeId);

            // Assert
            actionResult.As<NotFoundResult>().StatusCode.Should().Be(404);
            actionResult.Should().BeAssignableTo<NotFoundResult>();
        }
    }
}
