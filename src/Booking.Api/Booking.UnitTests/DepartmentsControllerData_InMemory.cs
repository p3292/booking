﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.DataAccess;
using Booking.DataAccess.Repositories;
using Booking.UnitTests.Builders;
using Booking.WebHost.Controllers;
using Booking.WebHost.Controllers.v1;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;

namespace Booking.UnitTests
{
    public class DepartmentsControllerData_InMemory
    {
        public DepartmentsController departmentsController;
        public IRepository<Department> departmentRepository;
        private DataContext _initContext;

        public DepartmentsControllerData_InMemory(ILogger<DepartmentsController> logger)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                             .UseInMemoryDatabase(databaseName: "MockDeparmentsDB")
                             .UseLazyLoadingProxies()
                             .Options;

            _initContext = new DataContext(options);
            _initContext.Database.EnsureDeleted();
            _initContext.Database.EnsureCreated();

            departmentRepository = GetInMemoryDepartmentRepository();
            departmentsController = new DepartmentsController(departmentRepository, logger);
        }

        private IRepository<Department> GetInMemoryDepartmentRepository()
        {
            SeedDepartment(_initContext);
            var repository = new EfRepository<Department>(_initContext);
            return repository;
        }

        public void SeedDepartment(DataContext context)
        {
            Department department = new DepartmentBuilder()
                .WithId(Guid.Parse("dcb54f40-cea4-42c2-b40f-250761783c75"))
                .WithName("System Development and etc.")
                .Build();

            context.Add(department);
            context.SaveChanges();
        }
    }
}
