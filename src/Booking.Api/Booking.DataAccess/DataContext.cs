﻿using Booking.Core.Domain.Administration;
using Booking.Core.Domain.BookingManagement;
using Microsoft.EntityFrameworkCore;

namespace Booking.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Department> Departments { get; set; }
        
        public DbSet<ResourceType> ResourceTypes { get; set; }
                
        public DbSet<ParkingPlace> ParkingPlaces { get; set; }

        public DbSet<WorkPlace> WorkPlaces { get; set; }

        public DbSet<Parking> Parkings { get; set; }

        public DbSet<Reservation> Reservations { get; set; }

        public DbSet<Ban> Bans { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ban>(entity => {
                entity.HasIndex(p => new { p.EmployeeId, p.ResourceTypeId }).IsUnique();
            });

            base.OnModelCreating(modelBuilder);

            var fakeFactory = Data.FakeDataFactory.Generate();
            
            modelBuilder.Entity<Role>().HasData(fakeFactory.Roles);
            modelBuilder.Entity<ResourceType>().HasData(fakeFactory.ResourceTypes);
            modelBuilder.Entity<Department>().HasData(fakeFactory.Departments);
            modelBuilder.Entity<Employee>().HasData(fakeFactory.Employees);
            modelBuilder.Entity<Ban>().HasData(fakeFactory.Bans);
            modelBuilder.Entity<Parking>().HasData(fakeFactory.Parkings);
            modelBuilder.Entity<ParkingPlace>().HasData(fakeFactory.ParkingPlaces);
            modelBuilder.Entity<WorkPlace>().HasData(fakeFactory.WorkPlaces);
            modelBuilder.Entity<Reservation>().HasData(fakeFactory.Reservations);
        }
    }
}
