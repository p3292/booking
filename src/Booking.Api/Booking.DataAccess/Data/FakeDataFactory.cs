﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using Booking.Core.Domain.Administration;
using Booking.Core.Domain.BookingManagement;

namespace Booking.DataAccess.Data
{
    public class FakeDataFactory
    {
        public List<Department> Departments { get; private set; }
        public List<Role> Roles { get; private set; }
        public List<ResourceType> ResourceTypes { get; private set; }
        public List<Employee> Employees { get; private set; }
        public List<Ban> Bans { get; private set; }        
        
        public List<Parking> Parkings { get; private set; }
        public List<ParkingPlace> ParkingPlaces { get; private set; }
        public List<WorkPlace> WorkPlaces { get; private set; }

        public List<Reservation> Reservations { get; private set; }

        public static FakeDataFactory Generate()
        {
            var factory = new FakeDataFactory();
            var testEmails = new List<string>() { "<booking@molodtsov.info>"};

            factory.Roles = new List<Role> { 
                new Role() {Id= Guid.NewGuid(), Name= "Administrator", Description= "Description administrator" },
                new Role() {Id= Guid.NewGuid(), Name= "User", Description= "Description User" },
            };

            factory.ResourceTypes = new List<ResourceType> {
                new ResourceType() {Id= Guid.NewGuid(), Name= "Parking place"},
                new ResourceType() {Id= Guid.NewGuid(), Name= "Work place" },
            };

            factory.Departments = new Faker<Department>()
               .RuleFor(x => x.Name, bogus => bogus.Commerce.Department())
               .RuleFor(x => x.Id, Guid.NewGuid)
               .Generate(5);

            factory.Employees = new Faker<Employee>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.RoleId, bogus => bogus.PickRandom(factory.Roles).Id)
                .RuleFor(x => x.FirstName, bogus => bogus.Person.FirstName)
                .RuleFor(x => x.LastName, bogus => bogus.Person.LastName)
                .RuleFor(x => x.MiddleName, bogus => bogus.Name.FirstName(bogus.Person.Gender).OrNull(bogus, .2f))
                .RuleFor(x => x.DepartmentId, bogus => bogus.PickRandom(factory.Departments).Id)
                .RuleFor(x => x.Login, bogus => bogus.Person.Email)
                .RuleFor(x => x.Email, bogus => String.Concat(bogus.Person.FullName,bogus.PickRandom(testEmails)))
                .Generate(100);

            factory.Bans = new Faker<Ban>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.EmployeeId, bogus => bogus.PickRandom(factory.Employees).Id)
                .RuleFor(x => x.ResourceTypeId, bogus => bogus.PickRandom(factory.ResourceTypes).Id)
                .RuleFor(x => x.StartDateTime,bogus => bogus.Date.Between(DateTime.Now.AddDays(-2), DateTime.Now.AddDays(2)))
                .Generate(2);

            factory.Parkings = new Faker<Parking>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.Name, bogus => "Parking_" + bogus.Random.Word())
                .Generate(4);

            factory.ParkingPlaces = new Faker<ParkingPlace>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.Number, bogus => "ParkPlace_" + bogus.Random.Number(100).ToString())
                .RuleFor(x => x.ParkingId, bogus => bogus.PickRandom(factory.Parkings).Id)
                
                .RuleFor(x => x.ResourceTypeId, bogus => factory.ResourceTypes.Where(x=>x.Name.Equals("Parking place")).FirstOrDefault().Id)

                .Generate(20);

            factory.WorkPlaces = new Faker<WorkPlace>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.Number, bogus => "WorkPlace_" + bogus.Random.Number(50).ToString())
                .RuleFor(x => x.DepartmentId, bogus => bogus.PickRandom(factory.Departments).Id)
                .RuleFor(x => x.Building, bogus => "Building_" + bogus.Random.Word())
                .RuleFor(x => x.Floor, bogus => bogus.Random.Number(6))

                .RuleFor(x => x.ResourceTypeId, bogus => factory.ResourceTypes.Where(x => x.Name.Equals("Work place")).FirstOrDefault().Id)

                .Generate(20);

            factory.Reservations = new Faker<Reservation>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.BookPlaceId, bogus => bogus.PickRandom(factory.WorkPlaces).Id)
                
                //.RuleFor(x => x.ResourceTypeId, bogus => bogus.PickRandom(factory.ResourceTypes).Id)

                .RuleFor(x => x.EmployeeId, bogus => bogus.PickRandom(factory.Employees).Id)
                .RuleFor(x => x.StartDateTime, bogus => bogus.Date.Between(DateTime.Now.AddDays(-5), DateTime.Now.AddDays(5)))
                .RuleFor(x => x.EndDateTime, bogus => bogus.Date.Between(DateTime.Now.AddDays(1), DateTime.Now.AddDays(10)))
                .RuleFor(x => x.BookingType, bogus => bogus.Random.Bool())
                .RuleFor(x => x.IsCancelled, bogus => bogus.Random.Bool())
                .Generate(5);

            factory.Reservations = new Faker<Reservation>()
                .RuleFor(x => x.Id, Guid.NewGuid)
                .RuleFor(x => x.BookPlaceId, bogus => bogus.PickRandom(factory.ParkingPlaces).Id)
                
                //.RuleFor(x => x.ResourceTypeId, bogus => bogus.PickRandom(factory.ResourceTypes).Id)

                .RuleFor(x => x.EmployeeId, bogus => bogus.PickRandom(factory.Employees).Id)
                .RuleFor(x => x.StartDateTime, bogus => bogus.Date.Between(DateTime.Now.AddDays(-5), DateTime.Now.AddDays(5)))
                .RuleFor(x => x.EndDateTime, bogus => bogus.Date.Between(DateTime.Now.AddDays(1), DateTime.Now.AddDays(10)))
                .RuleFor(x => x.BookingType, bogus => bogus.Random.Bool())
                .RuleFor(x => x.IsCancelled, bogus => bogus.Random.Bool())
                .Generate(5);

            return factory;
        }
    }
}