﻿namespace Booking.DataAccess.Data
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}