﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Booking.DataAccess.Data;
using Booking.DataAccess;
using Booking.Core.Abstractions.Repositories;
using Booking.DataAccess.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Serilog;
using FluentValidation.AspNetCore;
using Elastic.Apm.NetCoreAll;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System.Reflection;
using System;
using System.IO;
using MassTransit;
using System.Net.Http;
using Microsoft.AspNetCore.DataProtection;
using System.Security.Cryptography.X509Certificates;
using Booking.Email.Message.Contracts;
using Booking.WebHost.Publishers;
using Booking.WebHost.Services;
using Booking.WebHost.RabbitMQ;
using BookingIdentityEmployee;
using Booking.WebHost.Consumers;
using Booking.WebHost.Hubs;
using BookingWebHostEmployeeEmailDto;
using BookingWebHostEmailRequestDto;

namespace Booking.WebHost
{
    public class Startup
    {
        public readonly IConfiguration _configuration;
        public IWebHostEnvironment _environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment) =>
            (_configuration, _environment) = (configuration, environment);

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration.GetConnectionString("SqliteConnection");
            var persistKeysPath = _configuration.GetValue<string>("PersistKeysPath");
            var certificate = _configuration.GetValue<string>("Certificate");
            var certificatePassword = _configuration.GetValue<string>("CertificatePassword");
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var authority = _configuration.GetValue<string>("DockerAuthority");

            if (environmentName is not null && (environmentName.Equals("DevelopmentLocal") || _environment.EnvironmentName.Equals("UnitTest")))
            {
                authority = _configuration.GetValue<string>("Authority");
            }

            if (connectionString != null) 
            {
              var pathArray = connectionString.Split("=");
                if (pathArray != null && pathArray.Length > 1)
                {
                    var path = Path.GetDirectoryName(pathArray[1]);
                    Directory.CreateDirectory(path);
                }
            }

            if (File.Exists(certificate))
            {
                services.AddDataProtection().PersistKeysToFileSystem(new DirectoryInfo(persistKeysPath))
                .SetApplicationName("WebApi").ProtectKeysWithCertificate(
                    new X509Certificate2(certificate, certificatePassword));
            }
            services.AddScoped(typeof(Core.ICurrentDateTimeProvider), typeof(Core.CurrentDateTimeProvider));
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddFluentValidation(x =>
            {
                x.RegisterValidatorsFromAssemblyContaining<Startup>();
            });
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite(connectionString); 
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            services.AddSignalR();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyMethod();
                    policy.AllowCredentials();
                    policy.WithOrigins(_configuration.GetValue<string>("ClientApp"));
                });
            });
            services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme =
                    JwtBearerDefaults.AuthenticationScheme;
                config.DefaultChallengeScheme =
                    JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer("Bearer", options =>
            {
                options.Authority = authority;
                options.Audience = _configuration.GetValue<string>("Audience");
                options.RequireHttpsMetadata = false;
                options.BackchannelHttpHandler = new HttpClientHandler
                {
                    ServerCertificateCustomValidationCallback = (message, certificate, chain, sslPolicyErrors) => true
                };
            });
            services.AddVersionedApiExplorer(options =>
                options.GroupNameFormat = "'v'VVV");
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>,
                ConfigureSwaggerOptions>();
            services.AddSwaggerGen(config =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                config.IncludeXmlComments(xmlPath);
            });
            services.AddVersionedApiExplorer(options => options.SubstituteApiVersionInUrl = true);
            services.AddApiVersioning();

            #region Rabbit
            services.Configure<RabbitMQOptions>(_configuration.GetSection(nameof(RabbitMQOptions)), o => o.BindNonPublicProperties = true);
            if (!_environment.EnvironmentName.Equals("UnitTest"))
            {
                services.AddMassTransit(mt =>
                {
                    mt.SetSnakeCaseEndpointNameFormatter();
                    mt.AddConsumer<EmployeeConsumer>();
                    mt.UsingRabbitMq((ctx, cfg) =>
                    {
                        EndpointConvention.Map<EmailMessageContract>(new Uri($"queue:{ctx.EndpointNameFormatter.Message<EmailMessageContract>()}"));
                        EndpointConvention.Map<BookingIdentityUserDto>(new Uri($"queue:{ctx.EndpointNameFormatter.Message<BookingIdentityUserDto>()}"));
                        EndpointConvention.Map<EmployeeEmailDto>(new Uri($"queue:{ctx.EndpointNameFormatter.Message<EmployeeEmailDto>()}"));
                        EndpointConvention.Map<EmailRequestDto>(new Uri($"queue:{ctx.EndpointNameFormatter.Message<EmailRequestDto>()}"));

                        cfg.Host(_configuration.GetValue<string>("RabbitMQOptions:Host"), _configuration.GetValue<string>("RabbitMQOptions:VHost"),
                            h =>
                            {
                                h.Username(_configuration.GetValue<string>("RabbitMQOptions:HostSettings:UserName"));
                                h.Password(_configuration.GetValue<string>("RabbitMQOptions:HostSettings:Password"));
                            });
                        cfg.ConfigureEndpoints(ctx);

                        cfg.ReceiveEndpoint(ctx.EndpointNameFormatter.Message<BookingIdentityEmployeeDto>(),
                            e =>
                            {
                                e.ConfigureConsumeTopology = false;
                                e.UseMessageRetry(r =>
                                {
                                    r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                                });
                                e.ConfigureConsumer<EmployeeConsumer>(ctx);
                            });
                    });
                    mt.AddRequestClient<EmailRequestDto>();
                });
            }
            services.AddOptions<MassTransitHostOptions>().Configure(options =>
            {
                options.WaitUntilStarted = true;
                options.StartTimeout = TimeSpan.FromSeconds(10);
                options.StopTimeout = TimeSpan.FromSeconds(30);
            });
            services.AddScoped<IUserPublisher, UserPublisher>();
            services.AddScoped<IEmployeeEmailPublisher, EmployeeEmailPublisher>();
            //services.AddScoped<IEmployeeEmailRequestPublisher, EmployeeEmailRequestPublisher>();
            #endregion

            services.AddScoped<IEmailPublisher, EmailPublisher>();
            services.AddScoped<IEmailGeneratorService, EmailGeneratorService>();
        }

       
        public void Configure(IApplicationBuilder app, 
            IWebHostEnvironment env, 
            IDbInitializer dbInitializer,
            IApiVersionDescriptionProvider provider)
        {
            var isElkApmEnabled = _configuration.GetValue<bool>("isElkApmEnabled");
            
            if(isElkApmEnabled)
                app.UseAllElasticApm(_configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    config.SwaggerEndpoint(
                        $"/swagger/{description.GroupName}/swagger.json",
                        description.GroupName.ToUpperInvariant());

                    config.RoutePrefix = string.Empty;
                }
            });
            app.UseHttpsRedirection();
            app.UseSerilogRequestLogging();
            app.UseRouting();
            app.UseCors("AllowAll");
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseApiVersioning();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<EmployeesHub>("/employeeshub");
                endpoints.MapHub<EmployeesHub>("/departmentshub");
                endpoints.MapHub<EmployeesHub>("/banshub");
                endpoints.MapHub<EmployeesHub>("/reservationshub");
            });

            dbInitializer.InitializeDb();
        }
    }

}
