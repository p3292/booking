﻿using MassTransit;
using System;
using System.Linq;
using System.Threading.Tasks;
using BookingIdentityEmployee;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Microsoft.Extensions.Configuration;

namespace Booking.WebHost.Consumers
{
    public class EmployeeConsumer : IConsumer<BookingIdentityEmployeeDto>
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Department> _departmentRepository;
        private readonly IConfiguration _configuration;

        public EmployeeConsumer(IRepository<Employee> employeeRepository, 
            IRepository<Role> roleRepository, 
            IRepository<Department> departmentRepository,
            IConfiguration configuration) =>
            (_employeeRepository, _roleRepository, _departmentRepository,_configuration) = (employeeRepository, roleRepository, departmentRepository, configuration);

        public async Task Consume(ConsumeContext<BookingIdentityEmployeeDto> context)
        {
            try
            {
                var newRole = new Role { Name = _configuration.GetValue<string>("EmployeeConsumer:role"), Description = $"This role contains {_configuration.GetValue<string>("EmployeeConsumer:role").ToLower()}" };
                var newDepartment = new Department { Name = _configuration.GetValue<string>("EmployeeConsumer:department") };

                var role = await _roleRepository.GetWhereAsync(x => x.Name == _configuration.GetValue<string>("EmployeeConsumer:role"));
                if (role.ToList().Count == 0)
                {
                    await _roleRepository.AddAsync(newRole);
                }
                else
                    newRole = role.ToList()[0];

                var department = await _departmentRepository.GetWhereAsync(x => x.Name == _configuration.GetValue<string>("EmployeeConsumer:department"));
                if (department.ToList().Count == 0)
                {
                    await _departmentRepository.AddAsync(newDepartment);
                }
                else
                    newDepartment = department.ToList()[0];

                var employee = EmployeeMapper.MapFromConsumer(context.Message, newRole, newDepartment);
                var employees = _employeeRepository.GetWhereAsync(x => x.Login == context.Message.Login).Result.ToList();

                if (employees.Count == 0)
                    await _employeeRepository.AddAsync(employee);
            }
            catch(Exception ex)
            { }
        }
    }
}
