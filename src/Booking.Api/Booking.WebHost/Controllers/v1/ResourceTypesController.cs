﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v1
{
    /// <summary>
    /// Типы ресурсов
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ResourceTypesController : ControllerBase
    {
        private readonly IRepository<ResourceType> _resourceTypeRepository;

        private readonly ILogger _logger;

        public ResourceTypesController(IRepository<ResourceType> resourceTypeRepository, ILogger<ResourceTypesController> logger) => 
            (_resourceTypeRepository,_logger) = (resourceTypeRepository, logger);

        /// <summary>
        /// Получить все типы ресурсов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<ResourceType>> GetResourceTypesAsync()
        {
            var resourceTypes = await _resourceTypeRepository.GetAllAsync();
            //var response = roles.Select(n => new RoleShortResponse(n)).ToList();

            if (resourceTypes == null)
                return NotFound();

            return Ok(resourceTypes);
        }

        /// <summary>
        /// Получить тип ресурса по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [Authorize]
        public async Task<ActionResult> GetResourceTypeAsync(Guid id)
        {
            var resourceType = await _resourceTypeRepository.GetByIdAsync(id);

            if (resourceType == null)
                return NotFound();

            return Ok(resourceType);
        }

        /// <summary>
        /// Создать тип ресурса
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> CreateResourceTypeAsync(CreateOrEditResourceTypeRequest request)
        {
            var resourceType = ResourceTypeMapper.MapFromModel(request);

            await _resourceTypeRepository.AddAsync(resourceType);

            _logger
                .LogInformation("The ResourceType {@resourceType} was created in the controller: {controller} with the model: {@request}",
                    resourceType, "ResourceTypeController", request);

            return Ok(new { IsSuccess = true });
        }

        /// <summary>
        /// Отредактировать тип ресурса
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> EditResourceTypeAsync(Guid id, CreateOrEditResourceTypeRequest request)
        {
            var resourceType = await _resourceTypeRepository.GetByIdAsync(id);

            if (resourceType == null)
                return NotFound();

            ResourceTypeMapper.MapFromModel(request, resourceType);

            await _resourceTypeRepository.UpdateAsync(resourceType);

            _logger
                .LogInformation("The ResourceType {@resourceType} was edited in the controller: {controller} with the model: {@request}",
                    resourceType, "ResourceTypeController", request);

            return NoContent();
        }

        /// <summary>
        /// Удалить тип ресурса по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> DeleteResourceTypeAsync(Guid id)
        {
            var resourceType = await _resourceTypeRepository.GetByIdAsync(id);
            if (resourceType == null)
                return NotFound();
            await _resourceTypeRepository.DeleteAsync(resourceType);

            _logger
                .LogInformation("The ResourceType {@resourceType} was deleted in the controller: {controller} with the id: {id}",
                    resourceType, "ResourceTypeController", id);

            return NoContent();
        }
    }
}
