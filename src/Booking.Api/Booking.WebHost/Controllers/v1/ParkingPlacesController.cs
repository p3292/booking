﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Booking.WebHost.Models.Response;
using Booking.WebHost.Models.Request;
using Booking.Core.Domain.Administration;
using Booking.Core.Abstractions.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace Booking.WebHost.Controllers.v1
{
    /// <summary>
    /// Parking place
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class ParkingPlacesController : ControllerBase
    {
        private readonly IRepository<ParkingPlace> _parkPlaceRepository;
        private readonly ILogger _logger;
        private static string contrName = "ParkingPlacesController";

        public ParkingPlacesController(IRepository<ParkingPlace> parkPlaceRepository, ILogger<ParkingPlacesController> logger)
        {
            _parkPlaceRepository = parkPlaceRepository;
            _logger = logger;
        }

        /// <summary>
        /// Get all parking places.
        /// </summary>
        /// <returns>Return all parking places or NotFound.</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<ParkingPlaceResponse>> GetParkingPlacesAsync()
        {
            var parkingPlaces = await _parkPlaceRepository.GetAllAsync();

            if (parkingPlaces == null)
                return NotFound();

            var response = parkingPlaces.Select(n => new ParkingPlaceResponse(n)).ToList();
            return Ok(response);
        }

        /// <summary>
        /// Get parking place by Id.
        /// </summary>
        /// <param name="id">Parking place Id.</param>
        /// <returns>Return parking place or NotFound.</returns>
        [HttpGet("{id:guid}")]
        [Authorize]
        public async Task<ActionResult<ParkingPlaceResponse>> GetParkingPlaceAsync(Guid id)
        {
            var parkingPlace = await _parkPlaceRepository.GetByIdAsync(id);
            
            if (parkingPlace == null)
                return NotFound();

            var response = new ParkingPlaceResponse(parkingPlace);
            return Ok(response);
        }

        /// <summary>
        /// Create parking place.
        /// </summary>        
        /// <param name="request">Parking place request.</param>
        /// <returns>Return parking place entry or NotFound.</returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<ParkingPlaceResponse>> CreateParkingPlaceAsync(CreateOrEditParkingPlaceRequest request)
        {
            var parkingPlace = new ParkingPlace()
            {
                Number = request.Number,
                OwnerId = request.OwnerId == Guid.Empty ? null : request.OwnerId,
                LockTimeStart = request.LockTimeStart,
                Duration = request.Duration,
                ParkingId = request.ParkingId,
                ResourceTypeId = request.ResourceTypeId
            };

            await _parkPlaceRepository.AddAsync(parkingPlace);

            _logger.LogInformation(@"Parking place was created. Controller: {controller}. Model: {@request}", contrName, request);

            return CreatedAtAction(contrName, new { id = parkingPlace.Id }, null);
        }

        /// <summary>
        /// Change parking place.
        /// </summary>
        /// <param name="id">Parking place Id.</param>
        /// <param name="request">Parking place request.</param>
        /// <returns>Return parking place entry or NotFound.</returns>
        [HttpPut("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> EditParkingPlaceAsync(Guid id, CreateOrEditParkingPlaceRequest request)
        {
            var parkingPlace = await _parkPlaceRepository.GetByIdAsync(id);

            if (parkingPlace == null)
                return NotFound();

            parkingPlace.Number = request.Number;
            parkingPlace.OwnerId = request.OwnerId == Guid.Empty ? null : request.OwnerId;
            parkingPlace.LockTimeStart = request.LockTimeStart;
            parkingPlace.Duration = request.Duration;
            parkingPlace.ParkingId = request.ParkingId;
            parkingPlace.ResourceTypeId = request.ResourceTypeId;

            await _parkPlaceRepository.UpdateAsync(parkingPlace);

            _logger.LogInformation(@"Parking place was updated. Controller: {controller}. Model: {@request}", contrName, request);

            return NoContent();
        }

        /// <summary>
        /// Delete parking place.
        /// </summary>
        /// <param name="id">Parking place Id.</param>
        /// <returns>Return parking place entry or NotFound.</returns>
        [HttpDelete("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> DeleteParkingPlaceAsync(Guid id)
        {
            var parkingPlace = await _parkPlaceRepository.GetByIdAsync(id);

            if (parkingPlace == null)
                return NotFound();

            await _parkPlaceRepository.DeleteAsync(parkingPlace);

            _logger.LogInformation(@"Parking place was deleted. Controller: {controller}. Id: {@id}", contrName, id);

            return NoContent();
        }
    }
}
