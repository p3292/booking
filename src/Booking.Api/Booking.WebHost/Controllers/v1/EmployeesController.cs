﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models;
using Booking.WebHost.Publishers;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v1
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Department> _departmentRepository;
        private readonly ILogger _logger;
        private readonly IUserPublisher _userPublisher;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository,
            IRepository<Department> departmentRepository,
            ILogger<EmployeesController> logger,
            IUserPublisher userPublisher) =>
            (_employeeRepository, _roleRepository, _departmentRepository, _logger, _userPublisher) =
            (employeeRepository, roleRepository, departmentRepository, logger, userPublisher);

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            if (employees == null)
                return NotFound();

            var response = employees.Select(n => new EmployeeShortResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [Authorize]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var response = new EmployeeResponse(employee);

            return Ok(response);
        }

        /// <summary>
        /// Получить данные сотрудника по Login
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpGet("{login}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByLoginAsync(string login)
        {
            var employee = await _employeeRepository.GetWhereAsync(x => x.Login == login);
            if (employee.Count() == 0)
                return NotFound();

            var response = new EmployeeResponse(employee.FirstOrDefault());

            return Ok(response);
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(CreateOrEditEmployeeRequest request)
        {
            var role = await _roleRepository.GetByIdAsync(request.RoleId);

            if (role == null)
                return NotFound();

            var department = await _departmentRepository.GetByIdAsync(request.DepartmentId);

            if (department == null)
                return NotFound();

            var employee = EmployeeMapper.MapFromModel(request, role, department);

            var employees = _employeeRepository.GetWhereAsync(x => x.Login == request.Login).Result.ToList();

            if (employees.Count > 0)
                return BadRequest();

            await _employeeRepository.AddAsync(employee);

            #region Rabbit
            await _userPublisher.CreateUser(employee);
            #endregion

            _logger
               .LogInformation("The Employee {@employee} was created in the controller: {controller} with " +
               "the model: {@request}," +
               " the department: {@department}," +
               " the role: {@role} ",
               employee, "EmployeesController", request, department, role);

            return Ok(new { id = employee.Id });
        }


        /// <summary>
        /// Отредактировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> EditEmployeesAsync(Guid id, CreateOrEditEmployeeRequest request)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employees = _employeeRepository.GetWhereAsync(x => x.Login == request.Login).Result.ToList();

            if (employees.Count > 1)
                return BadRequest();

            var role = await _roleRepository.GetByIdAsync(request.RoleId);

            var department = await _departmentRepository.GetByIdAsync(request.DepartmentId);

            EmployeeMapper.MapFromModel(request, role, department, employee);

            await _employeeRepository.UpdateAsync(employee);

            #region Rabbit
            await _userPublisher.CreateUser(employee);
            #endregion

            _logger
               .LogInformation("The Employee {@employee} was edited in the controller: {controller} with " +
               "the model: {@request}," +
               " the department: {@department}," +
               " the role: {@role} ",
               employee, "EmployeesController", request, department, role);

            return NoContent();
        }


        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteAsync(employee);

            _logger
                .LogInformation("The Employee {@employee} was deleted in the controller: {controller} with the id: {id}",
                    employee, "EmployeesController", id);

            return NoContent();
        }


        /// <summary>
        /// Деактивировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/lockEmployee")]
        public async Task<ActionResult<EmployeeResponse>> LockEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Данный сотрудник не найден");

            return CreatedAtAction(nameof(GetEmployeeAsync), new { id = employee.Id }, null);
        }

        /// <summary>
        /// Активировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/unlockEmployee")]
        public async Task<ActionResult<EmployeeResponse>> UnLockEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Данный сотрудник не найден");

            return CreatedAtAction(nameof(GetEmployeeAsync), new { id = employee.Id }, null);
        }

        /// <summary>
        /// Заблокировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/banEmployee")]
        public async Task<ActionResult<EmployeeResponse>> BanEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Данный сотрудник не найден");

            return CreatedAtAction(nameof(GetEmployeeAsync), new { id = employee.Id }, null);
        }

        /// <summary>
        /// Разблокировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}/unBanEmployee")]
        public async Task<ActionResult<EmployeeResponse>> UnBanEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound("Данный сотрудник не найден");

            return CreatedAtAction(nameof(GetEmployeeAsync), new { id = employee.Id }, null);
        }
    }
}
