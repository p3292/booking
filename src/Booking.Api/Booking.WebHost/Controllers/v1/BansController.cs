﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models;
using Booking.WebHost.Models.Response;
using Booking.WebHost.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v1
{
    /// <summary>
    /// Запреты на бронирования (Ban)
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BansController : ControllerBase
    {
        private readonly IRepository<Ban> _bansRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<ResourceType> _resourceTypeRepository;
        private readonly Core.ICurrentDateTimeProvider _currentDateTime;
        private readonly ILogger _logger;
        private static string contrName = "BansController";
        private readonly IEmailGeneratorService _emailGeneratorService;

        public BansController(ILogger<BansController> logger, IRepository<Ban> repository, IRepository<Employee> employeeRepository,
            IRepository<ResourceType> resourceTypeRepository, Core.ICurrentDateTimeProvider currentDateTime, IEmailGeneratorService emailGeneratorService) =>
            (_logger, _bansRepository, _employeeRepository, _resourceTypeRepository, _currentDateTime, _emailGeneratorService) =
            (logger, repository, employeeRepository, resourceTypeRepository, currentDateTime, emailGeneratorService);

        /// <summary>
        /// Получить все доступные баны
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetBanList")]
        [Authorize]
        public async Task<ActionResult<List<BanResponse>>> GetBanListAsync()
        {
            var bans = await _bansRepository.GetAllAsync();

            if (bans == null)
            {
                return NotFound();
            }

            var response = bans.Select(n => new BanResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Get ban by id.
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetBanAsync/{id:guid}")]
        public async Task<ActionResult<BanResponse>> GetBanAsync(Guid id)
        {
            var ban = await _bansRepository.GetByIdAsync(id);

            if (ban == null)
                return NotFound();

            var response = new BanResponse(ban);
            return Ok(response);
        }

        /// <summary>
        /// Получить все баны пользователя 
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetBansByUserIdAsync/{id:guid}")]
        public async Task<ActionResult<List<BanResponse>>> GetBansByUserIdAsync(Guid employeeId)
        {
            var bans = await _bansRepository.GetWhereAsync(x =>
                x.EmployeeId == employeeId);

            if (bans == null)
            {
                return NotFound();
            }

            var response = bans.Select(n => new BanResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        ///  Заполняет все пустые EndDate текущей датой
        /// </summary>
        /// <returns></returns>
        [HttpPost("ClearBanListAsync")]
        public async Task<ActionResult> ClearBanListAsync()
        {
            var bans = await _bansRepository.GetWhereAsync(x =>
                x.EndDateTime == null);

            foreach (Ban ban in bans)
            {
                ban.EndDateTime = _currentDateTime.CurrentDateTime;
            }

            await _bansRepository.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Cоздает бан запись на указанный тип ресурса для указанного пользователя с текущего момента. 
        /// нет пользователя - ошибка. если бан уже есть - ошибка.
        /// </summary>
        /// <returns></returns>
        [HttpPost("AddBanForUserAsync")]
        [Authorize]
        public async Task<ActionResult<BanResponse>> AddBanForUserAsync(BanForUserRequest request)
        {
            var ban = BanMapper.MapFromModel(request);

            var employee = await _employeeRepository.GetByIdAsync(ban.EmployeeId);
            var resource = await _resourceTypeRepository.GetByIdAsync(ban.ResourceTypeId);

            if (employee == null)
            {
                return NotFound("Employee not found");
            }

            if (resource == null)
            {
                return NotFound("Resource type not found");
            }

            var banFound = await _bansRepository.GetWhereAsync(x =>
               x.EmployeeId == request.EmployeeId &&
               x.ResourceTypeId == request.ResourceTypeId);

            if (banFound != null && banFound.Count() > 0)
            {
                return BadRequest("Ban exist");
            }

            await _bansRepository.AddAsync(ban);

            var banInRepository = await _bansRepository.GetByIdAsync(ban.Id);

            await _emailGeneratorService.BanSendAsync(
                to: banInRepository.Employee.Email,
                fullName: banInRepository.Employee.FullName,
                resourceName: resource.Name
                );

            //return CreatedAtAction(nameof(AddBanForUserAsync), new BanResponse(banInRepository), null);
            return Ok(request);
        }

        /// <summary>
        /// Change ban.
        /// </summary>
        /// <param name="id">Ban Id.</param>
        /// <param name="request">Ban request.</param>
        /// <returns>Return ban entry or NotFound.</returns>

        //[HttpPut("{id:guid}")]
        [HttpPut]
        public async Task<IActionResult> EditBanAsync(Guid id, BanForUserRequest request)
        {
            var ban = await _bansRepository.GetByIdAsync(id);

            if (ban == null)
                return NotFound();

            ban.EmployeeId = request.EmployeeId;
            ban.ResourceTypeId = request.ResourceTypeId;
            ban.StartDateTime = request.StartDateTime;

            //ban.EndDateTime = request.EndDateTime;
            ban.EndDateTime = request.EndDateTime == DateTime.MinValue ? null : request.EndDateTime;

            await _bansRepository.UpdateAsync(ban);

            _logger.LogInformation(@"Ban was updated. Controller: {controller}. Model: {@request}", contrName, request);

            return NoContent();
        }

        /// <summary>
        /// Закрывает указанный бан(ы) (по типу ресурса и пользователю). 
        /// если не находит бана/пользователя - ошибка
        /// </summary>
        /// <returns></returns>
        [HttpPost("ClearBanForUserAsync")]
        public async Task<ActionResult> ClearBanForUserAsync(BanForUserRequest banRequest)
        {
            var allBans = await _bansRepository.GetWhereAsync(x =>
                x.EmployeeId == banRequest.EmployeeId &&
                x.ResourceTypeId == banRequest.ResourceTypeId);

            if (allBans == null)
            {
                return NotFound();
            }

            foreach (Ban ban in allBans)
            {
                ban.EndDateTime = _currentDateTime.CurrentDateTime;
            }

            await _bansRepository.SaveAsync();

            return NoContent();
        }
    }
}
