﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Booking.WebHost.Models.Request;
using Booking.Core.Domain.BookingManagement;
using Booking.WebHost.Models;
using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Microsoft.AspNetCore.Authorization;
using Booking.WebHost.Services;
using BookingWebHostEmailRequestDto;
using MassTransit;

namespace Booking.WebHost.Controllers.v1
{
    /// <summary>
    /// Booking
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class ReservationController : ControllerBase
    {
        private readonly IRepository<Reservation> _bookingRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Ban> _banRepository;
        private readonly IRepository<WorkPlace> _workPlaceRepository;
        private readonly IRepository<ParkingPlace> _parkPlaceRepository;
        private readonly IEmailGeneratorService _emailGeneratorService;
        private readonly IRequestClient<EmailRequestDto> _client;

        public ReservationController(IRepository<Reservation> bookingRepository,
            IRepository<Employee> employeeRepository,
            IEmailGeneratorService emailGeneratorService,
            IRepository<WorkPlace> workPlaceRepository,
            IRepository<ParkingPlace> parkPlaceRepository,
            IRepository<Ban> banRepository,
            IRequestClient<EmailRequestDto> client) =>
            (_bookingRepository, _employeeRepository, _emailGeneratorService, _workPlaceRepository, _parkPlaceRepository, _banRepository, _client) =
            (bookingRepository, employeeRepository, emailGeneratorService, workPlaceRepository, parkPlaceRepository, banRepository, client);

        /// <summary>
        /// Get all reservations
        /// </summary>
        /// <returns>Return all reservations or NotFound</returns>
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<ReservationResponse>> GetBookingListAsync()
        {
            var bookingList = await _bookingRepository.GetAllAsync();
            if (bookingList == null)
                return NotFound();
            var response = bookingList.Select(n => new ReservationResponse(n)).ToList();
            return Ok(response);
        }

        /// <summary>
        /// Get reservations by employeeId
        /// </summary>
        /// <param name="employeeId">Employee Id</param>
        /// <returns>Return reservations or NotFound</returns>
        [HttpGet("{employeeId:guid}")]
        [Authorize]
        public async Task<ActionResult<ReservationResponse>> GetBookingListByEmployeeIdAsync(Guid employeeId)
        {
            var bookingList = await _bookingRepository.GetWhereAsync(
                n => n.EmployeeId == employeeId);
            if (bookingList == null)
                return NotFound();
            return Ok(bookingList.Select(n => new ReservationResponse(n)));
        }

        /// <summary>
        /// Get reservation by id
        /// </summary>
        /// <param name="id">Booking Id</param>
        /// <returns>Return reservation or NotFound</returns>
        [HttpGet("GetBookingByIdAsync/{id:guid}")]
        [Authorize]
        public async Task<ActionResult<ReservationResponse>> GetBookingByIdAsync(Guid id)
        {
            var bookingList = await _bookingRepository.GetWhereAsync(
                n => n.Id == id);
            if (bookingList == null)
                return NotFound();
            return Ok(new ReservationResponse(bookingList.First()));
        }

        /// <summary>
        /// Create reservation
        /// </summary>        
        /// <param name="request">Booking request.</param>
        /// <returns>Return Ok or NotFound.</returns>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateBookingAsync(ReservationRequest request)
        {
            var banList = await _banRepository.GetWhereAsync(x => x.EmployeeId == request.EmployeeId && x.ResourceTypeId == request.ResourceTypeId);
            if (banList.Count() > 0)
                return NotFound("User banned");

            // Get resourse type:
            var resourceTypeId = Guid.Empty;

            var parkingPlace = await _parkPlaceRepository.GetByIdAsync(request.BookPlaceId);
            var workplacePlace = await _workPlaceRepository.GetByIdAsync(request.BookPlaceId);

            if (parkingPlace != null)
                resourceTypeId = parkingPlace.ResourceTypeId;
            else
                resourceTypeId = workplacePlace.ResourceTypeId;

            var reserations = await _bookingRepository.GetWhereAsync
            (x =>
                // not cancelled:
                !x.IsCancelled &&

                // same satrt date:
                x.StartDateTime.Date == request.StartDateTime.Date &&

                (
                    // Try to book the same resourse type in one day for same employee (start):
                    (resourceTypeId == request.ResourceTypeId && x.EmployeeId == request.EmployeeId /*&& x.StartDateTime.Date == request.StartDateTime.Date*/) ||

                    // Try to book the same resourse in one period (start):
                    (x.BookPlaceId == request.BookPlaceId /* && x.StartDateTime.Date == request.StartDateTime.Date*/)

                    // ...

                )
            );

            //var reserations = await _bookingRepository.GetWhereAsync(x => x.BookPlaceId == request.BookPlaceId && !x.IsCancelled &&
            //(x.EndDateTime > request.StartDateTime || x.StartDateTime < request.EndDateTime || x.StartDateTime > request.StartDateTime && x.EndDateTime < request.EndDateTime)
            //|| x.BookingType == request.BookingType && x.StartDateTime.Date == request.StartDateTime.Date && x.EmployeeId == request.EmployeeId
            //);
            if (reserations.Count() > 0) return NotFound("Cross reservation error");

            var booking = new Reservation()
            {
                BookPlaceId = request.BookPlaceId,
                EmployeeId = request.EmployeeId,
                Employee = await _employeeRepository.GetByIdAsync(request.EmployeeId),
                StartDateTime = request.StartDateTime,
                EndDateTime = request.EndDateTime,
                EndDateTimeFact = request.EndDateTimeFact,
                BookingType = request.BookingType,
                IsCancelled = request.IsCancelled
            };

            await _bookingRepository.AddAsync(booking);

            /*
            var bookingList = await _bookingRepository.GetWhereAsync(n => n.Id == booking.Id);
            if (bookingList == null)
                return NotFound();

            var bookingFound = bookingList.FirstOrDefault();
            */

            await _emailGeneratorService.ReservationSendAsync(
                to: booking.Employee.Email,
                fullName: booking.Employee.FullName,
                startDate: booking.StartDateTime.ToShortDateString(),
                endDate: booking.EndDateTime.ToShortDateString()
            );

            return NoContent();
        }

        /// <summary>
        /// Update reservation
        /// </summary>
        /// <param name="request">Booking record request</param>
        /// <param name="employeeId"></param>
        /// <returns>Return result object</returns>
        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateBookingAsync(ReservationRequest request, Guid employeeId)
        {
            var result = new QueryResult();
            if (request == null)
                return NotFound();

            var booking = await _bookingRepository.GetByIdAsync(request.Id);
            if (booking == null)
                return NotFound();

            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee == null)
                return NotFound();

            if (!(employee.IsAdmin || employeeId == booking.EmployeeId))
                return NotFound();

            booking.BookPlaceId = request.BookPlaceId;
            booking.EmployeeId = request.EmployeeId;
            booking.StartDateTime = request.StartDateTime;
            booking.EndDateTime = request.EndDateTime;
            booking.EndDateTimeFact = request.EndDateTimeFact;
            booking.BookingType = request.BookingType;
            booking.IsCancelled = request.IsCancelled;

            await _bookingRepository.UpdateAsync(booking);

            return NoContent();
        }

        /// <summary>
        /// Cancell reservation
        /// </summary>
        /// <param name="id">Booking Id</param>
        /// <param name="employeeId"></param>
        /// <returns>Return result object</returns>
        [HttpPut("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> CancelBooking(Guid id, Guid employeeId)
        {
            var booking = await _bookingRepository.GetByIdAsync(id);
            if (booking == null)
                return NotFound();

            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee == null)
                return NotFound();

            if (!(employee.IsAdmin || employeeId == booking.EmployeeId))
                return NotFound();

            booking.IsCancelled = true;

            await _bookingRepository.UpdateAsync(booking);

            return NoContent();
        }

        /// <summary>
        /// Delete reservation
        /// </summary>
        /// <param name="id">Booking Id</param>
        /// <returns>Return result object</returns>
        [HttpDelete("{id:guid}")]
        [Authorize]
        public async Task<IActionResult> DeleteBooking(Guid id)
        {
            var booking = await _bookingRepository.GetByIdAsync(id);
            if (booking == null)
                return NotFound();
            await _bookingRepository.DeleteAsync(booking);

            return NoContent();
        }
    }
}
