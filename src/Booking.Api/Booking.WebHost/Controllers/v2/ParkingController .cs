﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v2
{
    /// <summary>
    /// Пркинги
    /// </summary>
    [ApiVersion("2.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ParkingController: ControllerBase
    {
        private readonly IRepository<Parking> _parkingRepository;
        
        private readonly ILogger _logger;

        public ParkingController(IRepository<Parking> parkingRepository, ILogger<ParkingController> logger) =>
            (_parkingRepository, _logger) = (parkingRepository, logger);

        /// <summary>
        /// Получить все доступные паркинги
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Get /parkings
        /// </remarks>
        /// <returns>Returns ParkingResponse</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        public async Task<ActionResult<ParkingResponse>> GetParkingAsync()
        {
            var parking = await _parkingRepository.GetAllAsync();

            if (parking == null)
                return NotFound();

            var response = parking.Select(n => new ParkingResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить паркинг по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ParkingResponse>> GetParkingAsync(Guid id)
        {
            var parking = await _parkingRepository.GetByIdAsync(id);

            if (parking == null)
                return NotFound();

            var response = new ParkingResponse(parking);

            return Ok(response);
        }

        /// <summary>
        /// Создать паркинг
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<ParkingResponse>> CreateParkingAsync(CreateOrEditParkingRequest request)
        {
            var parking = ParkingMapper.MapFromModel(request);

            await _parkingRepository.AddAsync(parking);

            _logger
                .LogInformation("The Parking {@parking} was created in the controller: {controller} with the model: {@request}",
                    parking, "ParkingController", request);

            return CreatedAtAction(nameof(GetParkingAsync), new { id = parking.Id }, null);
        }

        /// <summary>
        /// Отредактировать паркинг
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditParkingAsync(Guid id, CreateOrEditParkingRequest request)
        {
            var parking = await _parkingRepository.GetByIdAsync(id);

            if (parking == null)
                return NotFound();

            ParkingMapper.MapFromModel(request, parking);

            await _parkingRepository.UpdateAsync(parking);

            _logger
               .LogInformation("The Parking {@parking} was edited in the controller: {controller} with the model: {@request}",
                   parking, "ParkingController", request);

            return NoContent();
        }


        /// <summary>
        /// Удалить паркинг по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteParkingAsync(Guid id)
        {
            var parking = await _parkingRepository.GetByIdAsync(id);

            if (parking == null)
                return NotFound();

            await _parkingRepository.DeleteAsync(parking);

            _logger
              .LogInformation("The Parking {@parking} was deleted in the controller: {controller} with id: {id}",
                  parking, "ParkingController", id);

            return NoContent();
        }
    }
}
