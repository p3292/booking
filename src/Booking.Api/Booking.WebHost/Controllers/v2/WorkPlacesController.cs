﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Booking.WebHost.Models.Response;
using Booking.WebHost.Models.Request;
using Booking.Core.Domain.Administration;
using Booking.Core.Abstractions.Repositories;
using Microsoft.Extensions.Logging;

namespace Booking.WebHost.Controllers.v2
{
    /// <summary>
    /// Book place
    /// </summary>
    [ApiVersion("2.0")]
    [ApiController]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/[controller]")]

    public class WorkPlacesController : ControllerBase
    {
        private readonly IRepository<WorkPlace> _workPlaceRepository;
        private readonly ILogger _logger;
        private static string contrName = "WorkPlacesController";

        public WorkPlacesController(IRepository<WorkPlace> workPlaceRepository, ILogger<WorkPlacesController> logger)
        {
            _workPlaceRepository = workPlaceRepository;
            _logger = logger;
        }

        /// <summary>
        /// Get all work places.
        /// </summary>
        /// <returns>Return all work places or NotFound.</returns>
        [HttpGet]
        public async Task<ActionResult<WorkPlaceResponse>> GetWorkPlacesAsync()
        {
            var workPlaces = await _workPlaceRepository.GetAllAsync();

            if (workPlaces == null)
                return NotFound();

            var response = workPlaces.Select(n => new WorkPlaceResponse(n)).ToList();
            return Ok(response);
        }

        /// <summary>
        /// Get work place by Id.
        /// </summary>
        /// <param name="id">Work place Id.</param>
        /// <returns>Return work place or NotFound.</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<WorkPlaceResponse>> GetWorkPlaceAsync(Guid id)
        {
            var workPlace = await _workPlaceRepository.GetByIdAsync(id);
            if (workPlace == null)
                return NotFound();

            var response = new WorkPlaceResponse(workPlace);
            return Ok(response);
        }

        /// <summary>
        /// Create work place.
        /// </summary>        
        /// <param name="request">work place request.</param>
        /// <returns>Return work place entry or NotFound.</returns>
        [HttpPost]
        public async Task<ActionResult<WorkPlaceResponse>> CreateWorkPlaceAsync(CreateOrEditWorkPlaceRequest request)
        {
            var workPlace = new WorkPlace()
            {
                Number = request.Number,

                //OwnerId = request.OwnerId,
                OwnerId = request.OwnerId == Guid.Empty ? null : request.OwnerId,

                LockTimeStart = request.LockTimeStart,
                Duration = request.Duration,

                ResourceTypeId = request.ResourceTypeId

            };

            await _workPlaceRepository.AddAsync(workPlace);

            _logger.LogInformation(@"Work place was created. Controller: {controller}. Model: {@request}", contrName, request);

            return CreatedAtAction(nameof(GetWorkPlaceAsync), new { id = workPlace.Id }, null);
        }

        /// <summary>
        /// Change work place.
        /// </summary>
        /// <param name="id">Work place Id.</param>
        /// <param name="request">Work place request.</param>
        /// <returns>Return work place entry or NotFound.</returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditWorkPlaceAsync(Guid id, CreateOrEditWorkPlaceRequest request)
        {
            var workPlace = await _workPlaceRepository.GetByIdAsync(id);

            if (workPlace == null)
                return NotFound();

            workPlace.Number = request.Number;

            //workPlace.OwnerId = request.OwnerId;
            workPlace.OwnerId = request.OwnerId == Guid.Empty ? null : request.OwnerId;

            workPlace.LockTimeStart = request.LockTimeStart;
            workPlace.Duration = request.Duration;
            workPlace.DepartmentId = request.DepartmentId;
            workPlace.Building = request.Building;
            workPlace.Floor = request.Floor;

            workPlace.ResourceTypeId = request.ResourceTypeId;

            await _workPlaceRepository.UpdateAsync(workPlace);

            _logger.LogInformation(@"Work place was updated. Controller: {controller}. Model: {@request}", contrName, request);

            return NoContent();
        }

        /// <summary>
        /// Delete work place.
        /// </summary>
        /// <param name="id">Work place Id.</param>
        /// <returns>Return work place entry or NotFound.</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteWorkPlaceAsync(Guid id)
        {
            var workPlace = await _workPlaceRepository.GetByIdAsync(id);

            if (workPlace == null)
                return NotFound();

            await _workPlaceRepository.DeleteAsync(workPlace);

            _logger.LogInformation(@"Work place was deleted. Controller: {controller}. Id: {@id}", contrName, id);

            return NoContent();
        }
    }
}
