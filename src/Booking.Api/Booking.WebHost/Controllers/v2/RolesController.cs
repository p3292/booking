﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v2
{
    /// <summary>
    /// Роли
    /// </summary>
    [ApiVersion("2.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class RolesController: ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;
        
        private readonly ILogger _logger;

        public RolesController(IRepository<Role> rolesRepository, ILogger<RolesController> logger) =>
            (_rolesRepository,_logger) = (rolesRepository,logger);

        /// <summary>
        /// Получить все доступные роли
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Get /roles
        /// </remarks>
        /// <returns>Returns RoleShortResponse</returns>
        /// <response code="200">Success</response>
        /// <response code="401">If the user is unauthorized</response>
        [HttpGet]
        public async Task<ActionResult<RoleShortResponse>> GetRolesAsync()//RoleItemResponse
        {
            var roles = await _rolesRepository.GetAllAsync();

            if (roles == null)
                return NotFound();

            var response = roles.Select(n => new RoleShortResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить роль по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleShortResponse>> GetRoleAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            var response = new RoleShortResponse(role);

            return Ok(response);
        }

        /// <summary>
        /// Создать роль
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<RoleShortResponse>> CreateRoleAsync(CreateOrEditRoleRequest request)
        {
            var role = RoleMapper.MapFromModel(request);

            await _rolesRepository.AddAsync(role);

            _logger
                .LogInformation("The Role {@role} was created in the controller: {controller} with the model: {@request}",
                    role, "RolesController", request);

            return CreatedAtAction(nameof(GetRolesAsync), new { id = role.Id }, null);
        }


        /// <summary>
        /// Отредактировать роль
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditRoleAsync(Guid id, CreateOrEditRoleRequest request)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            RoleMapper.MapFromModel(request, role);

            await _rolesRepository.UpdateAsync(role);

            _logger
               .LogInformation("The Role {@role} was edited in the controller: {controller} with the model: {@request}",
                   role, "RolesController", request);

            return NoContent();
        }


        /// <summary>
        /// Удалить роль по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteRoleAsync(Guid id)
        {
            var role = await _rolesRepository.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            await _rolesRepository.DeleteAsync(role);

            _logger
              .LogInformation("The Role {@role} was deleted in the controller: {controller} with id: {id}",
                  role, "RolesController", id);

            return NoContent();
        }
    }
}
