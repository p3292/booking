﻿using Booking.Core.Abstractions.Repositories;
using Booking.Core.Domain.Administration;
using Booking.WebHost.Mappers;
using Booking.WebHost.Models.Request;
using Booking.WebHost.Models.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers.v2
{
    /// <summary>
    /// Отделы
    /// </summary>
    [ApiVersion("2.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class DepartmentsController: ControllerBase
    {
        private readonly IRepository<Department> _departmentRepository;

        private readonly ILogger _logger;

        public DepartmentsController(IRepository<Department> repository, ILogger<DepartmentsController> logger) =>
            (_departmentRepository, _logger) = (repository, logger);

        /// <summary>
        /// Получить все доступные отделы
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<DepartmentShortResponse>> GetDepartmentsAsync()
        {
            var departments = await _departmentRepository.GetAllAsync();

            if (departments == null)
                return NotFound();

            var response = departments.Select(n => new DepartmentShortResponse(n)).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить отдел по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<DepartmentShortResponse>> GetDepartmentAsync(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            var response = new DepartmentShortResponse(department);

            return Ok(response);
        }

        /// <summary>
        /// Создать отдел
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<DepartmentShortResponse>> CreateDepartmentAsync(CreateOrEditDepartmentRequest request)
        {
            var department = DepartmentMapper.MapFromModel(request);

            await _departmentRepository.AddAsync(department);

            _logger
                .LogInformation("The Department {@department} was created in the controller: {controller} with the model: {@request}",
                    department, "DepartmentsController", request);

            return Ok(request);
        }


        /// <summary>
        /// Отредактировать отдел
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditDepartmentAsync(Guid id, CreateOrEditDepartmentRequest request)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            DepartmentMapper.MapFromModel(request, department);

            await _departmentRepository.UpdateAsync(department);

            _logger
                .LogInformation("The Department {@department} was edited in the controller: {controller} with the model: {@request}",
                    department, "DepartmentsController", request);

            return NoContent();
        }


        /// <summary>
        /// Удалить отдел по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteDepartmentAsync(Guid id)
        {
            var department = await _departmentRepository.GetByIdAsync(id);

            if (department == null)
                return NotFound();

            await _departmentRepository.DeleteAsync(department);

            _logger
                .LogInformation("The Department {@department} was deleted in the controller: {controller} with the id: {id}",
                    department, "DepartmentsController", id);

            return NoContent();
        }
    }
}
