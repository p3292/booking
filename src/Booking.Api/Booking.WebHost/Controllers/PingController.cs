﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [ApiController]
    [Produces("application/json ")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class PingController : ControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<string>> GetPongAsync(string value)
        {
            return await Task.FromResult(value);

        }
    }
}
