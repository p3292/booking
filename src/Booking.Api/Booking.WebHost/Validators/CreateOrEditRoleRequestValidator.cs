﻿using Booking.WebHost.Models;
using FluentValidation;

namespace Booking.WebHost.Validators
{
    public class CreateOrEditRoleRequestValidator : AbstractValidator<CreateOrEditRoleRequest>
    {
        public CreateOrEditRoleRequestValidator()
        {
            RuleFor(n => n.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.Name)
                .Length(1, 50)
                .When(n => !string.IsNullOrEmpty(n.Name))
                .WithMessage("Length({TotalLength}) of {PropertyName} exceeds allowed Length 50.");
        }
    }
}
