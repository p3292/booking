﻿using Booking.WebHost.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.WebHost.Validators
{
    public class CreateOrEditResourceTypeRequestValidator : AbstractValidator<CreateOrEditResourceTypeRequest>
    {
        public CreateOrEditResourceTypeRequestValidator()
        {
            RuleFor(n => n.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.Name)
                .Length(1, 50)
                .When(n => !string.IsNullOrEmpty(n.Name))
                .WithMessage("Length({TotalLength}) of {PropertyName} exceeds allowed Length 50.");
        }
    }
}
