﻿using Booking.WebHost.Models.Request;
using FluentValidation;

namespace Booking.WebHost.Validators
{
    public class CreateOrEditDepartmentRequestValidator : AbstractValidator<CreateOrEditDepartmentRequest>
    {
        public CreateOrEditDepartmentRequestValidator()
        {
            RuleFor(n => n.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.Name)
                .Length(1, 50)
                .When(n => !string.IsNullOrEmpty(n.Name))
                .WithMessage("Length({TotalLength}) of {PropertyName} exceeds allowed Length 50.");
        }
    }
}
