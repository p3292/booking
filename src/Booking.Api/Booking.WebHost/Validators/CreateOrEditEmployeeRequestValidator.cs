﻿using Booking.WebHost.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.WebHost.Validators
{
    public class CreateOrEditEmployeeRequestValidator : AbstractValidator<CreateOrEditEmployeeRequest>
    {
        public CreateOrEditEmployeeRequestValidator()
        {
            RuleFor(n => n.FirstName)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.FirstName)
                .Length(1, 50)
                .When(n => !string.IsNullOrEmpty(n.FirstName))
                .WithMessage("Length({TotalLength}) of {PropertyName} exceeds allowed Length 50.");
            RuleFor(n => n.LastName)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.LastName)
                .Length(1, 50)
                .When(n => !string.IsNullOrEmpty(n.LastName))
                .WithMessage("Length({TotalLength}) of {PropertyName} exceeds allowed Length 50.");
            RuleFor(n => n.Login)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.Email)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(n => n.Email)
                .NotEmpty()
                .WithMessage("{PropertyName} cannot be null");
            RuleFor(x => x.Email).NotEmpty()
                .EmailAddress();
        }
    }
}
