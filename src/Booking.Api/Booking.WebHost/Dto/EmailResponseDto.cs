﻿using System;

namespace BookingWebHostEmailResponseDto
{
    public class EmailResponseDto
    {
        public Guid Id { get; set; }

        public string Email { get; set; }
    }
}
