﻿using System;

namespace BookingIdentityEmployee
{
    public class BookingIdentityEmployeeDto
        {
            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string MiddleName { get; set; }

            public string Login { get; set; }

            public string EmployeeEmail { get; set; }

            public bool IsAdmin { get; set; }

            public bool CanUseSystem { get; set; }
        }
}
