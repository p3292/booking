﻿using System;

namespace BookingWebHostEmailRequestDto
{
    public class EmailRequestDto
    {
        public Guid Id { get; set; }
    }
}
