﻿namespace Booking.WebHost.Models.Request
{
    public class CreateOrEditDepartmentRequest
    {
        public string Name { get; set; }
    }
}
