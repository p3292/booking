﻿namespace Booking.WebHost.Models
{
    public class CreateOrEditRoleRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
