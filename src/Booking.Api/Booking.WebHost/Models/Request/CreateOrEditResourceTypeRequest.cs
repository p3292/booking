﻿namespace Booking.WebHost.Models
{
    public class CreateOrEditResourceTypeRequest
    {
        public string Name { get; set; }
        public bool IsSelectable { get; set; }
    }
}