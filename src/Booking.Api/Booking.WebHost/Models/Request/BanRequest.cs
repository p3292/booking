﻿
using System;

namespace Booking.WebHost.Models
{
    public class BanForUserRequest
    {
        public Guid EmployeeId { get; set; }
        public Guid ResourceTypeId { get; set; }

        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}
