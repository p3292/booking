﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models.Request
{
    public class CreateOrEditParkingPlaceRequest
    {
		public string Number { get; set; }

		public Guid OwnerId { get; set; }
                
        public DateTime LockTimeStart { get; set; }

        public int Duration { get; set; }

        public Guid ParkingId { get; set; }

        public Guid ResourceTypeId { get; set; }
    }
}