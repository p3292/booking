﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models.Request
{
    public class ReservationRequest
    {
        public Guid Id { get; set; }

        public Guid BookPlaceId { get; set; }

        public Guid ResourceTypeId { get; set; }

        public Guid EmployeeId { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public DateTime EndDateTimeFact { get; set; }

        public bool BookingType { get; set; }

        public bool IsCancelled { get; set; }
    }
}