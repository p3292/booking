﻿using System;

namespace Booking.WebHost.Models
{
    public class CreateOrEditEmployeeRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public bool IsAdmin { get; set; }
        
        public bool CanUseSystem { get; set; }

        public Guid RoleId { get; set; }
        
        public Guid DepartmentId { get; set; }

        public string Login { get; set; }
        public string Email { get; set; }
    }
}
