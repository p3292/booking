﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models.Request
{
    public class CreateOrEditWorkPlaceRequest
    {
		public string Number { get; set; }
		
        public Guid OwnerId { get; set; }        
        
        public DateTime LockTimeStart { get; set; }
     
        public int Duration { get; set; }

        public Guid DepartmentId { get; set; }

        public string Building { get; set; }

        public int Floor { get; set; }

        public Guid ResourceTypeId { get; set; }
    }
}