﻿namespace Booking.WebHost.Models
{
    public class QueryResult
    {
        public bool IsSuccess { get; set; } = true;

        public string Message { get; set; } = "";

        public object Data { get; set; }
    }
}
