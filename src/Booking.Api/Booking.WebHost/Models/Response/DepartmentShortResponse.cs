﻿
using Booking.Core.Domain.Administration;
using System;

namespace Booking.WebHost.Models.Response
{
    public class DepartmentShortResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DepartmentShortResponse()
        {

        }

        public DepartmentShortResponse(Department depratment)
        {
            Id = depratment.Id;
            Name = depratment.Name;
        }
    }
}
