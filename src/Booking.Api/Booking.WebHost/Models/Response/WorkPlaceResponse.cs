﻿using Booking.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models.Response
{
    public class WorkPlaceResponse : BookPlaceResponse
    {
        //public Guid DepartmentId { get; set; }
        
        public DepartmentResponse Department { get; set; }
        
        public string Building { get; set; }
        
        public int Floor { get; set; }        

        public WorkPlaceResponse()
        {
            // ...
        }

        public WorkPlaceResponse(WorkPlace workPlace) : base(workPlace)
        {
            //DepartmentId = workPlace.DepartmentId;
            Department = new DepartmentResponse(workPlace.Department);
            Building = workPlace.Building;
            Floor = workPlace.Floor;
        }

        // ...

    }
}
