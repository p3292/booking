﻿using Booking.Core.Domain.Administration;
using System;

namespace Booking.WebHost.Models.Response
{
    public class DepartmentResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DepartmentResponse()
        {

        }

        public DepartmentResponse(Department role)
        {
            Id = role.Id;
            Name = role.Name;
        }
    }
}
