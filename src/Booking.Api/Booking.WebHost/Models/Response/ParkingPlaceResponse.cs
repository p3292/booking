﻿using System;
using Booking.Core.Domain.Administration;

namespace Booking.WebHost.Models.Response
{
    public class ParkingPlaceResponse:BookPlaceResponse
    {
        public ParkingResponse Parking { get; set; }

        public ParkingPlaceResponse()
        {
            // ...
        }

        public ParkingPlaceResponse(ParkingPlace parkingPlace) : base(parkingPlace)
        {
            Parking = new ParkingResponse(parkingPlace.Parking);
        }
    }
}