﻿using Booking.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models
{
    public class RoleResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        
        public string Description { get; set; }

        public RoleResponse()
        {

        }

        public RoleResponse(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            Description = role.Description;
        }
    }
}
