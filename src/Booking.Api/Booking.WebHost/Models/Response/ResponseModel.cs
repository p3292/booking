﻿using System.Collections.Generic;

namespace Booking.WebHost.Models.Response
{
    public class ResponseModel
    {
        public ResponseModel() => 
            (IsValid, ValidationMessages) = (true, new List<string>());

        public bool IsValid { get; set; }
        
        public List<string> ValidationMessages { get; set; }
    }
}
