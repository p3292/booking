﻿using Booking.Core.Domain.Administration;
using System;

namespace Booking.WebHost.Models
{
	public class EmployeeShortResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Email { get; set; }

        public EmployeeShortResponse()
        {

        }

        public EmployeeShortResponse(Employee employee)
        {
            Id = employee.Id;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            Email = employee.Email;
        }
    }
}
