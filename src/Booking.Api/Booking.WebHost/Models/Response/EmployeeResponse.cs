﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models.Response;
using System;

namespace Booking.WebHost.Models
{
	public class EmployeeResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
        
        public bool CanUseSystem { get; set; }

        public RoleResponse Role { get; set; }

        public DepartmentResponse Department { get; set; }


        public EmployeeResponse()
        {

        }

        public EmployeeResponse(Employee employee)
        {
            Id = employee.Id;
            Login = employee.Login; 
            MiddleName = employee.MiddleName;   
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            IsAdmin = employee.IsAdmin;
            CanUseSystem = employee.CanUseSystem;
            Email = employee.Email;
            Role = new RoleResponse(employee.Role);
            Department = new DepartmentResponse(employee.Department);
        }
    }
}
