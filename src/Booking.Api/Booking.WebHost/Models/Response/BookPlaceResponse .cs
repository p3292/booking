﻿using System;
using Booking.Core.Domain.Administration;

namespace Booking.WebHost.Models.Response
{
    public abstract class BookPlaceResponse
    {
        public Guid Id { get; set; }
        
        public EmployeeResponse Owner { get; set; }
        
        public string Number { get; set; }        

        public DateTime LockTimeStart { get; set; }
        
        public int Duration { get; set; }					// Minutes
        
        public Guid ResourceTypeId { get; set; }
        public ResourceType ResourceType { get; set; }

        public BookPlaceResponse()
        {
            // ..
        }

        public BookPlaceResponse(BookPlace bookPlace)
        {
            Id = bookPlace.Id;

            if (bookPlace.Owner != null)
                Owner = new EmployeeResponse(bookPlace.Owner);

            Number = bookPlace.Number;
            LockTimeStart = bookPlace.LockTimeStart;
            Duration = bookPlace.Duration;

            ResourceTypeId = bookPlace.ResourceTypeId;
            ResourceType = bookPlace.ResourceType;
        }
    }
}