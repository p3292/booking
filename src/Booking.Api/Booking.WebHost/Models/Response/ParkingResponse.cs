﻿using Booking.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models
{
    public class ParkingResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }        

        public ParkingResponse()
        {
            // ...
        }

        public ParkingResponse(Parking parking)
        {
            Id = parking.Id;
            Name = parking.Name;
        }
    }
}
