﻿using Booking.Core.Domain.Administration;
using System;

namespace Booking.WebHost.Models
{
    public class RoleShortResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public RoleShortResponse()
        {

        }

        public RoleShortResponse(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            Description = role.Description;
        }
    }
}
