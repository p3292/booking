﻿using Booking.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Booking.WebHost.Models.Response
{
    public class BanResponse
    {
        public Guid Id { get; set; }
        public ResourceType ResourceType { get; set; }
        public Employee Employee { get; set; }
        public DateTime StartDateTime {get;set;}
        public DateTime ? EndDateTime { get; set; }

        public BanResponse()
        {

        }

        public BanResponse(Ban ban)
        {
            Id = ban.Id;
            ResourceType = ban.ResourceType;
            Employee = ban.Employee;
            StartDateTime = ban.StartDateTime;
            EndDateTime = ban.EndDateTime;
        }
    }
}
