﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models.Response;
using System;

namespace Booking.WebHost.Models
{
	public class ReservationResponse
    {
        public Guid Id { get; set; }

        public Guid BookPlaceId { get; set; }
        
        public BookPlace BookPlace { get; set; }
        
        public Guid EmployeeId { get; set; }
        
        public Employee Employee { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public DateTime EndDateTimeFact { get; set; }

        public bool BookingType { get; set; }

        public bool IsCancelled { get; set; }

        public ReservationResponse() { }

        public ReservationResponse(Booking.Core.Domain.BookingManagement.Reservation item)
        {
            Id = item.Id;
            BookPlaceId = item.BookPlaceId;
            BookPlace = item.BookPlace;
            EmployeeId = item.EmployeeId;
            Employee = item.Employee;
            StartDateTime = item.StartDateTime;
            EndDateTime = item.EndDateTime;
            EndDateTimeFact = item.EndDateTimeFact;
            BookingType = item.BookingType;
            IsCancelled = item.IsCancelled;
        }
    }
}
