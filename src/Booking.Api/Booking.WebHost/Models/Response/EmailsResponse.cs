﻿using System;

namespace Booking.WebHost.Models.Response
{
    public class EmailsResponse
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

    }
}
