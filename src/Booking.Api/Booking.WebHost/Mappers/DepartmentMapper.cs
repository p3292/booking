﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models.Request;

namespace Booking.WebHost.Mappers
{
    public static class DepartmentMapper
    {
        public static Department MapFromModel(CreateOrEditDepartmentRequest request, Department department = null)
        {
            if (department == null)
            {
                department = new Department();
            }

            department.Name = request.Name;

            return department;
        }
    }
}
