﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;

namespace Booking.WebHost.Mappers
{
    public static class ResourceTypeMapper
    {
        public static ResourceType MapFromModel(CreateOrEditResourceTypeRequest request, ResourceType resourceType = null)
        {
            if (resourceType == null)
            {
                resourceType = new ResourceType();
            }

            resourceType.Name = request.Name;
            resourceType.IsSelectable = request.IsSelectable;

            return resourceType;
        }
    }
}
