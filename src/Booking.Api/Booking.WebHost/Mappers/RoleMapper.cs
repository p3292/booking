﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;

namespace Booking.WebHost.Mappers
{
    public static class RoleMapper
    {
        public static Role MapFromModel(CreateOrEditRoleRequest request, Role role = null)
        {
            if (role == null)
            {
                role = new Role();
            }

            role.Name = request.Name;
            role.Description = request.Description;

            return role;
        }
    }
}
