﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using System;

namespace Booking.WebHost.Mappers
{
    public static class BanMapper
    {
        public static Ban MapFromModel(BanForUserRequest request, Ban ban = null)
        {
            if (ban == null)
            {
                ban = new Ban
                {
                    Id = Guid.NewGuid()
                };
            }

            ban.EmployeeId = request.EmployeeId;
            ban.ResourceTypeId = request.ResourceTypeId;

            //ban.StartDateTime = DateTime.Now;
            ban.StartDateTime = request.StartDateTime == DateTime.MinValue ? DateTime.Now : request.StartDateTime;
            ban.EndDateTime = request.EndDateTime == DateTime.MinValue ? null : request.EndDateTime;

            return ban;
        }
    }
}
