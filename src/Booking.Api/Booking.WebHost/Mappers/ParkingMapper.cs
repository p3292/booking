﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;

namespace Booking.WebHost.Mappers
{
    public static class ParkingMapper
    {
        public static Parking MapFromModel(CreateOrEditParkingRequest request, Parking parking = null)
        {
            if (parking == null)
            {
                parking = new Parking();
            }

            parking.Name = request.Name;
            
            return parking;
        }
    }
}
