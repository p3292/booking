﻿using Booking.Core.Domain.Administration;
using Booking.WebHost.Models;
using BookingIdentityEmployee;

namespace Booking.WebHost.Mappers
{
    public static class EmployeeMapper
    {
        public static Employee MapFromModel(CreateOrEditEmployeeRequest request,
            Role role, Department department, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    IsAdmin = request.IsAdmin,
                    CanUseSystem = request.CanUseSystem
                };
            }
            else
            {
                employee.IsAdmin = request.IsAdmin;
                employee.CanUseSystem = request.CanUseSystem;
            }

            employee.FirstName = request.FirstName;
            employee.LastName = request.LastName;
            employee.Login = request.Login;
            employee.Email = request.Email;

            if (role != null)
            {
                employee.RoleId = role.Id;
                employee.Role = role;
            }

            if (department != null)
            {
                employee.DepartmentId = department.Id;
                employee.Department = department;
            }

            return employee;
        }


        public static Employee MapFromConsumer(BookingIdentityEmployeeDto dto,
            Role role, Department department, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    IsAdmin = false,
                    CanUseSystem = true,
                    FirstName = dto.Login,
                    LastName = dto.Login,
                    Login = dto.Login,
                    Email = dto.EmployeeEmail
                };
            }
            else
            {
                employee.IsAdmin = dto.IsAdmin;
                employee.CanUseSystem = dto.CanUseSystem;
                employee.FirstName = dto.FirstName;
                employee.LastName = dto.LastName;
                employee.Login = dto.Login;
                employee.Email = dto.EmployeeEmail;
            }

            if (role != null)
            {
                employee.RoleId = role.Id;
                employee.Role = role;
            }

            if (department != null)
            {
                employee.DepartmentId = department.Id;
                employee.Department = department;
            }

            return employee;
        }
    }
}
