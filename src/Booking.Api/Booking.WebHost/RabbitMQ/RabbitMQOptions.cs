﻿using System.Collections.Generic;

namespace Booking.WebHost.RabbitMQ
{
    public class RabbitMQOptions
    {
        public string Host { get; set; }

        public string VHost { get; set; }

        public Dictionary<string, string> HostSettings { get; set; }

        public Dictionary<string, string> Queues { get; set; }
    }
}
