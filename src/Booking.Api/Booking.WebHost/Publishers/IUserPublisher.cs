﻿using Booking.Core.Domain.Administration;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public interface IUserPublisher
    {
        Task CreateUser(Employee employee);
    }
}
