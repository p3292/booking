﻿using Booking.WebHost.Models.Response;
using BookingWebHostEmployeeEmailDto;
using MassTransit;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public class EmployeeEmailPublisher : IEmployeeEmailPublisher
    {
        private readonly IBus _bus;

        public EmployeeEmailPublisher(IBus bus) => _bus = bus;

        /// <summary>
        /// Get email from Redis cache
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        async public Task SetEmail(EmailsResponse email)
        {
            var dto = new EmployeeEmailDto
            {
                Id = email.Id,
                Email = email.Email
            };

            await(_bus?.Send(dto) ?? Task.CompletedTask);
        }
    }
}
