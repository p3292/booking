﻿using Booking.WebHost.Models.Response;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public interface IEmployeeEmailPublisher
    {
        Task SetEmail(EmailsResponse email);
    }
}
