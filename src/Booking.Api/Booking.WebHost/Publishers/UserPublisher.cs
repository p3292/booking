﻿using Booking.Core.Domain.Administration;
using BookingIdentityEmployee;
using MassTransit;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public class UserPublisher : IUserPublisher
    {
        private readonly IBus   _bus;

        public UserPublisher(IBus bus) => _bus = bus;

        async public Task CreateUser(Employee employee)
        {
            var dto = new BookingIdentityUserDto
            {
                Login = employee.Login,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                MiddleName = employee.MiddleName,
                EmployeeEmail = employee.Email,
                CanUseSystem = employee.CanUseSystem,
                IsAdmin = employee.IsAdmin
            };
            
            await (_bus?.Send(dto) ?? Task.CompletedTask);
        }
    }
}
