﻿using Booking.Core;
using Booking.Email.Message.Contracts;
using MassTransit;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public class EmailPublisher: IEmailPublisher
    {
        private readonly IBus _bus;

        public EmailPublisher(IBus bus)
        {
            _bus = bus;
        }

        public async Task SendEmail(EmailMessage emailMessage)
        {
            var msg = new EmailMessageContract()
            {
                To = emailMessage.To,
                Subject = emailMessage.Subject,
                Body = emailMessage.Body,
                IsBodyHtml = emailMessage.IsBodyHtml
            };

            await _bus?.Send(msg);
        }

    }
}