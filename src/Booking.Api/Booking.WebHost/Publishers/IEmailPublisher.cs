﻿using Booking.Core;
using System.Threading.Tasks;

namespace Booking.WebHost.Publishers
{
    public interface IEmailPublisher
    {
        Task SendEmail(EmailMessage emailMessage);  
    }
}