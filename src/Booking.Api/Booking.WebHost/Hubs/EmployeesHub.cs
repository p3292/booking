﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Booking.WebHost.Hubs
{
    public class EmployeesHub : Hub
    {
        public async Task Send(string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", message);
        }
    }
}
