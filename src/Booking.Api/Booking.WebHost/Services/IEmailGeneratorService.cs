﻿using Booking.Core;
using System.Threading.Tasks;

namespace Booking.WebHost.Services
{
    public interface IEmailGeneratorService
    {
        
        Task BanSendAsync(string to, string fullName, string resourceName);
        Task ReservationSendAsync(string to, string fullName,  string startDate,string endDate);
    }
}