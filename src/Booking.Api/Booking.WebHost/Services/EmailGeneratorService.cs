﻿using Booking.Core;
using Booking.WebHost.Publishers;
using System.Text;
using System.Threading.Tasks;

namespace Booking.WebHost.Services
{
    public class EmailGeneratorService: IEmailGeneratorService
    {
        private readonly IEmailPublisher _emailPublisher;

        public EmailGeneratorService(IEmailPublisher emailPublisher)
        {
            _emailPublisher = emailPublisher;
        }

        public async Task BanSendAsync(string to, string fullName,string resourceName)
        {
            var sb = new StringBuilder();

            sb.Append($"Dear {fullName},");
            sb.Append("<br>");
            sb.Append($"you resource \"{resourceName}\" was blocked");

            var emailMessage = new Core.EmailMessage
            {
                To = to,
                Subject = $"For {fullName} resource is blocked",
                Body = sb.ToString(),
                IsBodyHtml = true
            };

            await _emailPublisher.SendEmail(emailMessage);
        }

        public async Task ReservationSendAsync(string to, string fullName, string startDate,string endDate)
        {
            var sb = new StringBuilder();

            sb.Append($"Dear {fullName},");
            sb.Append("<br>");
            sb.Append($"you have booked a resource starting at {startDate} ending at {endDate}");

            var emailMessage = new Core.EmailMessage
            {
                To = to,
                Subject = $"For {fullName} resource is booked",
                Body = sb.ToString(),
                IsBodyHtml = true
            };

            await _emailPublisher.SendEmail(emailMessage);
        }
    }
}
