﻿using BookingWebHostEmailResponseDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Email.Service.Publishers
{
    public interface IEmployeeEmailResponsePublisher
    {
        Task SetEmail(EmailResponseDto response);
    }
}
