﻿using BookingWebHostEmailResponseDto;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Email.Service.Publishers
{
    public class EmployeeEmailResponsePublisher : IEmployeeEmailResponsePublisher
    {
        private readonly IBus _bus;

        public EmployeeEmailResponsePublisher(IBus bus) => _bus = bus;

        async public Task SetEmail(EmailResponseDto response)
        {
            await(_bus?.Send(response) ?? Task.CompletedTask);
        }
    }
}
