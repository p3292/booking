﻿using Booking.Email.Service.Entities;
using Booking.Email.Service.Publishers;
using Booking.Email.Service.Repository;
using BookingWebHostEmailRequestDto;
using BookingWebHostEmailResponseDto;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace Booking.Email.Service.Consumers
{
    public class EmployeeEmailRequestConsumer : IConsumer<EmailRequestDto>
    {
        private readonly IEmailsRepository<Emails> _repository;
        private readonly IEmployeeEmailResponsePublisher _employeeEmailResponsePublisher;

        public EmployeeEmailRequestConsumer(IEmailsRepository<Emails> emailsRepository,
            IEmployeeEmailResponsePublisher employeeEmailResponsePublisher) =>
            (_repository, _employeeEmailResponsePublisher) = 
            (emailsRepository ?? throw new ArgumentNullException(nameof(emailsRepository)), employeeEmailResponsePublisher);

        /// <summary>
        /// Get the value from Redis cache and send it back to the endpoint (controller)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<EmailRequestDto> context)
        {
            try
            {
                var email = await _repository.GetByIdAsync(context.Message.Id);

                var response = new EmailResponseDto();
                if (email != null)
                {
                    response.Email = email.Email;
                }
                
                await context.RespondAsync<EmailResponseDto>(response);
            }
            catch (Exception ex)
            { }
        }
    }
}
