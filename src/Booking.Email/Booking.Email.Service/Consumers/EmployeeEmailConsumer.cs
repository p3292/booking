﻿using Booking.Email.Service.Entities;
using Booking.Email.Service.Repository;
using BookingWebHostEmployeeEmailDto;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace Booking.Email.Service.Consumers
{
    public class EmployeeEmailConsumer : IConsumer<EmployeeEmailDto>
    {
        private readonly IEmailsRepository<Emails> _repository;

        public EmployeeEmailConsumer(IEmailsRepository<Emails> emailsRepository) =>
            (_repository) =
            (emailsRepository ?? throw new ArgumentNullException(nameof(emailsRepository)));

        /// <summary>
        /// Update Redis cache with new value from endpoint (controller)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<EmployeeEmailDto> context)
        {
            try
            {
                var email = await _repository.GetByIdAsync(context.Message.Id);

                if(email == null)
                {
                    var entity = new Emails
                    {
                        Id = context.Message.Id,
                        Email = context.Message.Email
                    };
                    await _repository.UpdateAsync(entity);
                }
            }
            catch (Exception ex)
            { }
        }
    }
}
