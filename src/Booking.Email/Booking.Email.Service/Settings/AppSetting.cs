﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Email.Service.Settings
{
    public class AppSetting
    {
        public MessageBrokerSetting MessageBroker { get; set; }  = new MessageBrokerSetting();
        public SmtpSetting Smtp { get; set; } = new SmtpSetting();
        
    }
}
