﻿using Booking.Email.Message.Contracts;
using MassTransit;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Booking.Email.Service
{

    public class EmailConsumer : IConsumer<EmailMessageContract>
    {
        private readonly ILogger<EmailConsumer> _logger;
        private readonly IEmailService _emailService;

        public EmailConsumer(ILogger<EmailConsumer> logger, IEmailService emailService)
        {
            _logger = logger;
            _emailService = emailService;
        }

        public async Task Consume(ConsumeContext<EmailMessageContract> context)
        {
            var message = context.Message;

            _logger.LogDebug($"messageId:{context.MessageId}");

            var emailMessage = new EmailMessage()
            {
                To = message.To,
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = message.IsBodyHtml
            };

            await _emailService.SendMailAsync(emailMessage);
        }
    }

}