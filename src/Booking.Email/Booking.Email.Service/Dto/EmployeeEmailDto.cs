﻿using System;

namespace BookingWebHostEmployeeEmailDto
{
    public class EmployeeEmailDto
    {
        public Guid Id { get; set; }

        public string Email { get; set; }
    }
}