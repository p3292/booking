﻿using System.Threading.Tasks;

namespace Booking.Email.Service
{
    public interface IEmailService
    {
        Task SendMailAsync(EmailMessage emailMessage);

    }
}