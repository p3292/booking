﻿using Booking.Email.Service.Settings;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Booking.Email.Service
{
    public class EmailService : IEmailService
    {
        private readonly ILogger<EmailService> _logger;
        private readonly SmtpSetting _smtpSetting;

        public EmailService(ILogger<EmailService> logger, AppSetting appSetting)
        {
            _logger = logger;
            _smtpSetting = appSetting.Smtp;
        }

        public async Task SendMailAsync(EmailMessage emailMessage)
        {

            using (var smtpClient = new SmtpClient()
            {
                Host = _smtpSetting.Host,
                Port = _smtpSetting.Port,
                Credentials = new NetworkCredential(_smtpSetting.UserName, _smtpSetting.Password),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
            })
            {
                var encoding = System.Text.Encoding.UTF8;

                var mailMessage = new MailMessage(from: _smtpSetting.From, to: emailMessage.To)
                {
                    BodyEncoding = encoding,
                    SubjectEncoding = encoding,
                    Subject = emailMessage.Subject,
                    Body = emailMessage.Body,
                    IsBodyHtml = emailMessage.IsBodyHtml
                };

                await smtpClient.SendMailAsync(mailMessage);
            };


        }
    }
}