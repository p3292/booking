﻿using Booking.Email.Service.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Email.Service.Repository
{
    public interface IEmailsRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);

        Task<T> UpdateAsync(T entity);

        Task DeleteAsync(Guid id);

        Task DeleteAllkeys();
    }
}
