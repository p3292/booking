﻿using Booking.Email.Service.Entities;
using Booking.Email.Service.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Booking.Email.Service.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmailsController : ControllerBase
    {
        private readonly IEmailsRepository<Emails> _repository;

        public EmailsController(IEmailsRepository<Emails> emailsRepository) =>
            _repository = emailsRepository ?? throw new ArgumentNullException(nameof(emailsRepository));

        [HttpGet]
        public async Task<IEnumerable<Emails>> GetAsync()
        {
            return await _repository.GetAllAsync();
        }

        [HttpGet("GetRangeByIds")]
        public async Task<IEnumerable<Emails>> GetRangeByIdsAsync([FromQuery] List<Guid> ids)
        {
            return await _repository.GetRangeByIdsAsync(ids);
        }

        [HttpGet("GetById")]
        public async Task<Emails> GetByIdAsync([FromQuery] Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        [HttpPost("UpdateKey")]
        public async Task<ActionResult<Emails>> UpdateAsync(Emails entity)
        {
            return Ok(await _repository.UpdateAsync(entity));
        }

        [HttpDelete("DeleteKey")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
            return Ok();
        }

        [HttpDelete("FlushAllKeys")]
        public async Task<IActionResult> DeleteAllkeys()
        {
            await _repository.DeleteAllkeys();
            return Ok();
        }
    }
}
