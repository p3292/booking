﻿using Booking.Email.Message.Contracts;
using Booking.Email.Service.Consumers;
using Booking.Email.Service.Publishers;
using Booking.Email.Service.Repository;
using Booking.Email.Service.Settings;
using BookingWebHostEmailRequestDto;
using BookingWebHostEmailResponseDto;
using BookingWebHostEmployeeEmailDto;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;

namespace Booking.Email.Service
{
    public class Startup
    {
        public readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) =>
            (_configuration) = (configuration);

        public void ConfigureServices(IServiceCollection services)
        {
            var appSetting = _configuration.GetSection("App").Get<AppSetting>();

            services.AddSingleton(appSetting);

            services.AddTransient<IEmailService, EmailService>();

            services.AddMassTransit(x =>
            {
                x.SetSnakeCaseEndpointNameFormatter();

                x.AddConsumer<EmailConsumer>();
                x.AddConsumer<EmployeeEmailRequestConsumer>();
                x.AddConsumer<EmployeeEmailConsumer>();
                x.UsingRabbitMq((context, cfg) =>
                {
                    EndpointConvention.Map<EmailResponseDto>(new Uri($"queue:{context.EndpointNameFormatter.Message<EmailResponseDto>()}"));
                    EndpointConvention.Map<EmployeeEmailDto>(new Uri($"queue:{context.EndpointNameFormatter.Message<EmployeeEmailDto>()}"));
                    cfg.Host(host: appSetting.MessageBroker.RabbitMq.Host,
                        virtualHost: appSetting.MessageBroker.RabbitMq.VirtualHost,
                        configure: h =>
                        {
                            h.Username(appSetting.MessageBroker.RabbitMq.UserName);
                            h.Password(appSetting.MessageBroker.RabbitMq.Password);
                        });

                    cfg.ReceiveEndpoint(context.EndpointNameFormatter.Message<EmailMessageContract>(),
                        e =>
                        {
                            e.ConfigureConsumeTopology = false;
                            e.UseMessageRetry(r =>
                            {
                                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                            });
                            e.ConfigureConsumer<EmailConsumer>(context);

                        });
                    cfg.ReceiveEndpoint(context.EndpointNameFormatter.Message<EmailRequestDto>(),
                        e =>
                        {
                            e.ConfigureConsumeTopology = false;
                            e.UseMessageRetry(r =>
                            {
                                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                            });
                            e.ConfigureConsumer<EmployeeEmailRequestConsumer>(context);

                        });
                    cfg.ReceiveEndpoint(context.EndpointNameFormatter.Message<EmployeeEmailDto>(),
                        e =>
                        {
                            e.ConfigureConsumeTopology = false;
                            e.UseMessageRetry(r =>
                            {
                                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                            });
                            e.ConfigureConsumer<EmployeeEmailConsumer>(context);

                        });
                });
            });

            services.AddOptions<MassTransitHostOptions>().Configure(options =>
            {
                options.WaitUntilStarted = true;
                options.StartTimeout = TimeSpan.FromSeconds(10);
                options.StopTimeout = TimeSpan.FromSeconds(30);
            });
            services.AddScoped<IEmployeeEmailResponsePublisher, EmployeeEmailResponsePublisher>();

            services.AddStackExchangeRedisCache(opt =>
            {
                opt.Configuration = _configuration.GetConnectionString("Redis");
            });

            services.AddScoped(typeof(IEmailsRepository<>), typeof(EmailsRepository<>));
            services.AddControllers();
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo { Title = "Booking Emails 1.0", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List);
                s.RoutePrefix = string.Empty;
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                s.InjectStylesheet("/swagger-ui/custom.css");
            });
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
