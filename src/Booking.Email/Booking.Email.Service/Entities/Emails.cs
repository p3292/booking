﻿namespace Booking.Email.Service.Entities
{
    public class Emails : BaseEntity
    {
        public string Email { get; set; }
    }
}
