﻿using System;

namespace Booking.Email.Service.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
