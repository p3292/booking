﻿using System;

namespace Booking.Email.Message.Contracts
{
    public class EmailMessageContract
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
    }
}
