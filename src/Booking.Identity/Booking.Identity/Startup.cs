using System;
using Booking.Identity.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using Microsoft.AspNetCore.DataProtection;
using System.Security.Cryptography.X509Certificates;
using Microsoft.OpenApi.Models;
using Booking.Identity.Mongo;
using Booking.Identity.RabbitMQ;
using MassTransit;
using Booking.Identity.Consumers;
using Booking.Identity.Infrastructure;
using BookingIdentityEmployee;
using Booking.Identity.Publishers;
using Serilog;
using Elastic.Apm.NetCoreAll;

namespace Booking.Identity
{
    public class Startup
    {
        public IConfiguration _configuration { get; }

        public IWebHostEnvironment _environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment) =>
            (_configuration, _environment) = (configuration, environment);

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _configuration.GetConnectionString("SqliteConnection");
            var persistKeysPath = _configuration.GetValue<string>("PersistKeysPath");
            var certificate = _configuration.GetValue<string>("Certificate");
            var certificatePassword = _configuration.GetValue<string>("CertificatePassword");
            var mongoDbSettings = _configuration.GetSection(nameof(MongoDbOptions)).Get<MongoDbOptions>();
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Booking Identity 1.0", Version = "v1" });
            });

            if (File.Exists(certificate))
            {
                services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(persistKeysPath))
                .SetApplicationName("Identity").ProtectKeysWithCertificate(
                    new X509Certificate2(certificate, certificatePassword));
            }

            services.AddIdentity<ApplicationUser, ApplicationRole>(config =>
            {
                config.Password.RequiredLength = 4;
                config.Password.RequireDigit = false;
                config.Password.RequireUppercase = false;
                config.Password.RequireNonAlphanumeric = false;
            })
                .AddMongoDbStores<ApplicationUser, ApplicationRole, Guid>(
                    mongoDbSettings.ConnectionString, mongoDbSettings.DbName
                )
                .AddDefaultTokenProviders();

            services.AddIdentityServer(opt => opt.IssuerUri = _configuration.GetValue<string>("IssuerUri"))
                .AddAspNetIdentity<ApplicationUser>()
                .AddInMemoryApiResources(Configuration.ApiResources)
                .AddInMemoryIdentityResources(Configuration.IdentityResources)
                .AddInMemoryApiScopes(Configuration.ApiScopes)
                .AddInMemoryClients(Configuration.Clients)
                .AddDeveloperSigningCredential()
                .AddProfileService<ProfileService>();

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = _configuration.GetValue<string>("CookieName");
                config.LoginPath = _configuration.GetValue<string>("LoginPath");
                config.LogoutPath = _configuration.GetValue<string>("LogoutPath");
            });

            #region Rabbit
            services.Configure<RabbitMQOptions>(_configuration.GetSection(nameof(RabbitMQOptions)), o => o.BindNonPublicProperties = true);
            services.AddMassTransit(mt =>
            {
                mt.SetSnakeCaseEndpointNameFormatter();
                mt.AddConsumer<UserConsumer>();
                mt.UsingRabbitMq((ctx, cfg) =>
                {
                    EndpointConvention.Map<BookingIdentityEmployeeDto>(new Uri($"queue:{ctx.EndpointNameFormatter.Message<BookingIdentityEmployeeDto>()}"));
                    cfg.Host(_configuration.GetValue<string>("RabbitMQOptions:Host"), _configuration.GetValue<string>("RabbitMQOptions:VHost"),
                        h =>
                        {
                            h.Username(_configuration.GetValue<string>("RabbitMQOptions:HostSettings:UserName"));
                            h.Password(_configuration.GetValue<string>("RabbitMQOptions:HostSettings:Password"));
                        }
                    );
                    MessageDataDefaults.ExtraTimeToLive = TimeSpan.FromDays(1);
                    MessageDataDefaults.Threshold = 2000;
                    MessageDataDefaults.AlwaysWriteToRepository = false;
                    cfg.ConfigureEndpoints(ctx);

                    cfg.ReceiveEndpoint(ctx.EndpointNameFormatter.Message<BookingIdentityUserDto>(),
                        e =>
                        {
                            e.ConfigureConsumeTopology = false;
                            e.UseMessageRetry(r =>
                            {
                                r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                            });
                            e.ConfigureConsumer<UserConsumer>(ctx);
                        });
                });

                services.AddOptions<MassTransitHostOptions>().Configure(options =>
                {
                    options.WaitUntilStarted = true;
                    options.StartTimeout = TimeSpan.FromSeconds(10);
                    options.StopTimeout = TimeSpan.FromSeconds(30);
                });
            });
            services.AddScoped<IUserPublisher, UserPublisher>();
            #endregion

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var isElkApmEnabled = _configuration.GetValue<bool>("isElkApmEnabled");

            if (isElkApmEnabled)
                app.UseAllElasticApm(_configuration);

            if (env.IsDevelopment() || env.EnvironmentName.Equals("DevelopmentLocal"))
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(config =>
                {
                    config.SwaggerEndpoint(
                        $"/swagger/v1/swagger.json",
                        "v1");

                    config.RoutePrefix = string.Empty;
                });
            }

            app.UseHttpsRedirection();
            app.UseSerilogRequestLogging();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
