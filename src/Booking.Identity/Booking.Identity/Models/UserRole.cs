﻿using AspNetCore.Identity.MongoDbCore.Models;
using MongoDbGenericRepository.Attributes;
using System;

namespace Booking.Identity.Models
{
    public class UserRole
    {
        public string RoleName { get; set; }
    }
}
