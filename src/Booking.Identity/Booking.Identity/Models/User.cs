﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Identity.Models
{
    public class User
    {
        [Required]
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public bool IsAdmin { get; set; } = false;

        public bool CanUseSystem { get; set; } = true;

        public bool IsActive { get; set; } = true;
    }
}
