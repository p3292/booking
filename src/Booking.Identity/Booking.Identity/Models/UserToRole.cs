﻿using System.ComponentModel.DataAnnotations;

namespace Booking.Identity.Models
{
    public class UserToRole
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string RoleName { get; set; }
    }
}
