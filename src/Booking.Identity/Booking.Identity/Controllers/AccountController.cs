﻿using Booking.Identity.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Booking.Identity.Publishers;
using Microsoft.Extensions.Logging;

namespace Booking.Identity.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _singInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IIdentityServerInteractionService _interactionService;
        private readonly IUserPublisher _userPublisher;
        private readonly ILogger _logger;

        public AccountController(
            SignInManager<ApplicationUser> singInManager,
            UserManager<ApplicationUser> userManager,
            IIdentityServerInteractionService interactionService,
            IUserPublisher userPublisher,
            ILogger<SecuredController> logger) =>
            (_singInManager, _userManager, _interactionService, _userPublisher, _logger) =
            (singInManager, userManager, interactionService, userPublisher, logger);

        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            var viewModel = new LoginViewModel
            {
                ReturnUrl = returnUrl
            };

            return View(viewModel);
        }
        
        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = await _userManager.FindByNameAsync(viewModel.UserName);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "User not found");
                return View(viewModel);
            }

            var result = await _singInManager.PasswordSignInAsync(viewModel.UserName,
                viewModel.Password, false, false);
            if (result.Succeeded)
            {
                _logger
              .LogInformation("The User {@user} was logged in the controller: {controller} with " +
              "the model: {@viewModel} ",
              user, "AccountController", viewModel);

                return Redirect(viewModel.ReturnUrl);
            }
            ModelState.AddModelError(string.Empty, "Login error");

            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult Register(string returnUrl)
        {
            var viewModel = new RegisterViewModel
            {
                ReturnUrl= returnUrl
            };
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var user = new ApplicationUser
            {
                UserName = viewModel.UserName,
                FirstName = viewModel.UserName,
                LastName = viewModel.UserName
            };

            var result = await _userManager.CreateAsync(user, viewModel.Password);
            if (result.Succeeded)
            {
                _logger
               .LogInformation("The User {@user} was created in the controller: {controller} with " +
               "the model: {@viewModel} ",
               user, "AccountController", viewModel);

                #region Rabbit
                await _userPublisher.CreateEmployee(user);
                #endregion

                await _singInManager.SignInAsync(user, false);
                return Redirect(viewModel.ReturnUrl);
            }
            else
            {
                foreach (IdentityError error in result.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Logout(string logoutId)
        {
            await _singInManager.SignOutAsync();
            var logoutRequest = await _interactionService.GetLogoutContextAsync(logoutId);
            _logger
             .LogInformation("The User was logged out the controller: {controller} with " +
             "the model: {@logoutId} ",
             "AccountController", logoutId);

            return Redirect(logoutRequest.PostLogoutRedirectUri);
        }
    }
}
