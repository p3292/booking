﻿using Booking.Identity.Models;
using Booking.Identity.Publishers;
using Booking.Identity.RabbitMQ;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace Booking.Identity.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserPublisher _userPublisher;
        private readonly ILogger _logger;

        public UserController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IUserPublisher userPublisher,
            ILogger<UserController> logger) =>
            (_userManager, _roleManager, _userPublisher, _logger) =
            (userManager, roleManager, userPublisher, logger);

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(User user)
        {
            if (ModelState.IsValid)
            {
                var appUser = new ApplicationUser
                {
                    UserName = user.UserName,
                    Email = user.Email
                };

                var result = await _userManager.CreateAsync(appUser, user.Password);
                if (result.Succeeded)
                {
                    _logger
                   .LogInformation("The User {@appUser} was created in the controller: {controller} with " +
                   "the model: {@user} ",
                   appUser, "UserController", user);

                    #region Rabbit
                    await _userPublisher.CreateEmployee(user);
                    #endregion
                    ViewBag.Message = "User created successfully";
                }
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(user);
        }
        
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(UserRole userRole)
        {
            if (ModelState.IsValid)
            {
                var result = await _roleManager.CreateAsync(new ApplicationRole { Name = userRole.RoleName });
                if (result.Succeeded)
                {
                    _logger
                  .LogInformation("The Role {@userRole} was created in the controller: {controller} with " +
                  "the model: {@userRole} ",
                  userRole, "UserController", userRole);

                    ViewBag.Message = "Role created successfully";
                }
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(userRole);
        }
        

        [HttpGet]
        public IActionResult AddToRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddToRole(UserToRole userToRole)
        {
            if (ModelState.IsValid)
            {
                var appUser = await _userManager.FindByNameAsync(userToRole.UserName);
                if (appUser == null)
                {
                    ModelState.AddModelError(string.Empty, "Error occured");
                }

                var appRole = await _roleManager.FindByNameAsync(userToRole.RoleName);
                if(appRole == null)
                {
                    ModelState.AddModelError(string.Empty, "Error occured");
                }

                var result = await _userManager.AddToRoleAsync(appUser, userToRole.RoleName);

                if (result.Succeeded)
                {
                    _logger
                      .LogInformation("The User {@appUser} was added to the Role {@appRole} was created in the controller: {controller} with " +
                      "the model: {@userToRole} ",
                      appUser, appRole, "UserController", userToRole);

                    ViewBag.Message = "Role assigned successfully";
                }
                else
                {
                    foreach (IdentityError error in result.Errors)
                        ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            return View(userToRole);
        }
    }
}
