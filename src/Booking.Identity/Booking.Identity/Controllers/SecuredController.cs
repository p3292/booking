﻿using Booking.Identity.Models;
using Booking.Identity.Publishers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Booking.Identity.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SecuredController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserPublisher _userPublisher;
        private readonly ILogger _logger;

        public SecuredController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
             IUserPublisher userPublisher,
            ILogger<SecuredController> logger) =>
            (_userManager, _roleManager, _userPublisher, _logger) =
            (userManager,  roleManager, userPublisher, logger);


        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create(User user)
        {

            var appUser = new ApplicationUser
            {
                UserName = user.UserName,
                Email = user.Email
            };

            var result = await _userManager.CreateAsync(appUser, user.Password);
            if (result.Succeeded)
            {
                _logger
               .LogInformation("The User {@appUser} was created in the controller: {controller} with " +
               "the model: {@user} ",
               appUser, "SecuredController", user);

                #region Rabbit
                await _userPublisher.CreateEmployee(user);
                #endregion
            }
            return Ok(user);
        }

        /// <summary>
        /// Create role
        /// </summary>
        /// <param name="userRole"></param>
        /// <returns></returns>
        [HttpPost("CreateRole")]
        public async Task<IActionResult> CreateRole(UserRole userRole)
        {
            var result = await _roleManager.CreateAsync(new ApplicationRole { Name = userRole.RoleName });
            if (result.Succeeded)
            {
                _logger
              .LogInformation("The Role {@userRole} was created in the controller: {controller} with " +
              "the model: {@userRole} ",
              userRole, "SecuredController", userRole);

                ViewBag.Message = "Role created successfully";
            }
            else
            {
                foreach (IdentityError error in result.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }
            return Ok(userRole);
        }

        /// <summary>
        /// Add user to the role
        /// </summary>
        /// <param name="userToRole"></param>
        /// <returns></returns>
        [HttpPost("AddToRole")]
        public async Task<IActionResult> AddToRole(UserToRole userToRole)
        {
            var appUser = await _userManager.FindByNameAsync(userToRole.UserName);
            if (appUser == null)
            {
                ModelState.AddModelError(string.Empty, "Error occured");
            }

            var appRole = await _roleManager.FindByNameAsync(userToRole.RoleName);
            if (appRole == null)
            {
                ModelState.AddModelError(string.Empty, "Error occured");
            }

            var result = await _userManager.AddToRoleAsync(appUser, userToRole.RoleName);

            if (result.Succeeded)
            {
                _logger
              .LogInformation("The User {@appUser} was added to the Role {@appRole} was created in the controller: {controller} with " +
              "the model: {@userToRole} ",
              appUser, appRole, "SecuredController", userToRole);

                ViewBag.Message = "Role assigned successfully";
            }
            else
            {
                foreach (IdentityError error in result.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }

            return Ok(userToRole);
        }
    }
}
