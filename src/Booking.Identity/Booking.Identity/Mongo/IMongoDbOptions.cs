﻿using System.Collections.Generic;

namespace Booking.Identity.Mongo
{
    public interface IMongoDbOptions
    {
        string ConnectionString { get; set; }
        string DbName { get; set; }
    }
}
