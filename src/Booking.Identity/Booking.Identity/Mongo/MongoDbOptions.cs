﻿using System.Collections.Generic;

namespace Booking.Identity.Mongo
{
    public class MongoDbOptions : IMongoDbOptions
    {
        public string ConnectionString { get; set; }
        public string DbName { get; set; }
    }
}
