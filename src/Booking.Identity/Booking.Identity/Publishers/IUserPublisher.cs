﻿using Booking.Identity.Models;
using System.Threading.Tasks;

namespace Booking.Identity.Publishers
{
    public interface IUserPublisher
    {
        Task CreateEmployee(User user);

        Task CreateEmployee(ApplicationUser user);
    }
}
