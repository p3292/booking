﻿using Booking.Identity.Models;
using BookingIdentityEmployee;
using MassTransit;
using System.Threading.Tasks;

namespace Booking.Identity.Publishers
{
    public class UserPublisher : IUserPublisher
    {
        private readonly IBus _bus;

        public UserPublisher(IBus bus) => _bus = bus;

        public async Task CreateEmployee(User user)
        {
            var dto = new BookingIdentityEmployeeDto
            {
                Login = user.UserName,
                FirstName = user.UserName,
                LastName = user.UserName,
                EmployeeEmail = user.Email
            };

            await _bus?.Send<BookingIdentityEmployeeDto>(dto);
        }

        public async Task CreateEmployee(ApplicationUser user)
        {
            var dto = new BookingIdentityEmployeeDto
            {
                Login = user.UserName,
                FirstName = user.UserName,
                LastName = user.UserName,
                EmployeeEmail = user.Email
            };

            await _bus?.Send<BookingIdentityEmployeeDto>(dto);
        }
    }
}
