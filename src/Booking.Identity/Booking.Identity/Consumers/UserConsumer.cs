﻿using System.Threading.Tasks;
using Booking.Identity.Models;
using BookingIdentityEmployee;
using MassTransit;
using Microsoft.AspNetCore.Identity;

namespace Booking.Identity.Consumers
{
    public class UserConsumer : IConsumer<BookingIdentityUserDto>
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserConsumer(UserManager<ApplicationUser> userManager) => 
            _userManager = userManager;

        public async Task Consume(ConsumeContext<BookingIdentityUserDto> context)
        {
            var appUser = await _userManager.FindByNameAsync(context.Message.Login);
            var user = new ApplicationUser
            {
                UserName = context.Message.Login,
                FirstName = context.Message.FirstName,
                LastName = context.Message.LastName,
                MiddleName = context.Message.MiddleName,
                Email = context.Message.EmployeeEmail,
                IsAdmin = context.Message.IsAdmin,
                CanUseSystem = context.Message.CanUseSystem
            };

            if (appUser is null)
                await _userManager.CreateAsync(user, context.Message.Login);
            else
            {
                ApplicationUser model = await _userManager.FindByNameAsync(context.Message.Login);
                if (model is not null)
                {
                    model.UserName = context.Message.Login;
                    model.FirstName = context.Message.FirstName;
                    model.LastName = context.Message.LastName;
                    model.MiddleName = context.Message.MiddleName;
                    model.Email = context.Message.EmployeeEmail;
                    model.IsAdmin = context.Message.IsAdmin;
                    model.CanUseSystem = context.Message.CanUseSystem;

                    await _userManager.UpdateAsync(model);
                }
            }
        }
    }
}
