SELECT * FROM departments d;
SELECT * FROM resource_types;
SELECT * FROM roles;
SELECT * FROM employees;
SELECT * FROM parkings p;

SELECT * FROM book_place bp;

/* Employee */

/*
{
  "firstName": "ban",
  "lastName": "ser",
  "middleName": "",
  "isAdmin": true,
  "canUseSystem": true,
  "roleId": "CFD7508F-745D-4119-9D51-10EF8D262660",
  "departmentId": "96001C3D-940B-4174-8C2B-0222BB0C46FC",
  "login": "ban",
  "email": "ban@mail.ru"
}
*/

/* Work place */

/*
{
  "number": "123",
  "ownerId": "A0CFB134-79C4-4A6F-AB0D-2EFDB0D70EFC",  
  "lockTimeStart": "2017-07-21T17:32:28Z",
  "duration": 125,  
  "departmentId": "96001C3D-940B-4174-8C2B-0222BB0C46FC",
  "building": "Main",
  "floor": 3,
  "resourceTypeId": "1A17CD76-CBAE-4DDA-A77C-B38B31726586"
}
*/


/* Parking place */

/*
{
  "number": "zzzzzzzzzzzzzzzzzzzz",
  "ownerId": "0BA58039-9D02-43DB-B2A3-BEEEC65D654B",
  "lockTimeStart": "2022-04-26T22:56:48.677Z",
  "duration": 0,
  "parkingId": "07D49080-8602-4E4F-AFC2-C90EB4589059",
  "resourceTypeId": "5835AE8E-E09D-4CDE-B8BD-AEB8DA5513DB"
}
*/

/*
 
-
  
{
  "number": "123",    
  "lockTimeStart": "2017-07-21T17:32:28Z",
  "duration": 125,  
  "parkingId": "88991A86-4D5D-4445-A6F8-79897A8D93C2"  
}
*/

/*
 
+
 
{
  "number": "123",
  "ownerId": "A0CFB134-79C4-4A6F-AB0D-2EFDB0D70EFC",  
  "parkingId": "88991A86-4D5D-4445-A6F8-79897A8D93C2"
}
*/