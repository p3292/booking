@echo off
docker-compose --profile all stop
docker-compose --profile all down
powershell -command "&  docker rm -f  $(docker ps -a  --format \"{{.Names}}\" | findstr \"booking.*\")"
powershell -command "&  docker rmi -f $(docker images --format \"{{.Repository}}:{{.Tag}}\" | findstr \"^^booking/*\")"
::docker-compose build


@set profile=%1
if not defined profile (
set profile=all
)

echo Start docker profile:%profile%
docker-compose --profile %profile% --env-file .env  -f "docker-compose.yml" up --build
