@echo off
echo.
echo.
echo. start under admin session
echo.
echo.
echo.
pause

docker-compose --profile all stop
net stop winnat

netsh int ipv4 add excludedportrange protocol=tcp startport=3000 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=5672 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=8081 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=8200 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=5601 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=9200 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=44361 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=44317 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=44362 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=27017 numberofports=1
netsh int ipv4 add excludedportrange protocol=tcp startport=15672 numberofports=1

net start winnat
docker-compose --profile all start
